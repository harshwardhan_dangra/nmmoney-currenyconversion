USE [master]
GO
/****** Object:  Database [NMoneyCurrencyConversion]    Script Date: 21-12-2018 22:39:03 ******/
CREATE DATABASE [NMoneyCurrencyConversion]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'NMoneyCurrencyConversion', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER01\MSSQL\DATA\NMoneyCurrencyConversion.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'NMoneyCurrencyConversion_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER01\MSSQL\DATA\NMoneyCurrencyConversion_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [NMoneyCurrencyConversion].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET ARITHABORT OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET  ENABLE_BROKER 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET RECOVERY FULL 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET  MULTI_USER 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET DB_CHAINING OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'NMoneyCurrencyConversion', N'ON'
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET QUERY_STORE = OFF
GO
USE [NMoneyCurrencyConversion]
GO
/****** Object:  UserDefinedTableType [dbo].[ItemsTable]    Script Date: 21-12-2018 22:39:04 ******/
CREATE TYPE [dbo].[ItemsTable] AS TABLE(
	[Key] [nvarchar](150) NULL,
	[Value] [nvarchar](max) NULL
)
GO
/****** Object:  UserDefinedFunction [dbo].[UrlDecode]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[UrlDecode](@url varchar(3072))
RETURNS varchar(3072)
AS
BEGIN 
    DECLARE @count int, @c char(1), @cenc char(2), @i int, @urlReturn varchar(3072) 
    SET @count = Len(@url) 
    SET @i = 1 
    SET @urlReturn = '' 
    WHILE (@i <= @count) 
     BEGIN 
        SET @c = substring(@url, @i, 1) 
        IF @c LIKE '[!%]' ESCAPE '!' 
         BEGIN 
            SET @cenc = substring(@url, @i + 1, 2) 
            SET @c = CHAR(CASE WHEN SUBSTRING(@cenc, 1, 1) LIKE '[0-9]' 
                                THEN CAST(SUBSTRING(@cenc, 1, 1) as int) 
                                ELSE CAST(ASCII(UPPER(SUBSTRING(@cenc, 1, 1)))-55 as int) 
                            END * 16 + 
                            CASE WHEN SUBSTRING(@cenc, 2, 1) LIKE '[0-9]' 
                                THEN CAST(SUBSTRING(@cenc, 2, 1) as int) 
                                ELSE CAST(ASCII(UPPER(SUBSTRING(@cenc, 2, 1)))-55 as int) 
                            END) 
            SET @urlReturn = @urlReturn + @c 
            SET @i = @i + 2 
         END 
        ELSE 
         BEGIN 
            SET @urlReturn = @urlReturn + @c 
         END 
        SET @i = @i +1 
     END 
    RETURN @urlReturn
END
GO
/****** Object:  Table [dbo].[AccessRight]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccessRight](
	[AccessRightsId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[EntityType] [int] NULL,
	[KorrectAccessRight] [int] NULL,
	[SecurityPrincipalType] [int] NULL,
	[AllowedRights] [int] NULL,
	[DeniedRights] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_AccessRight] PRIMARY KEY CLUSTERED 
(
	[AccessRightsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Locales]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Locales](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](max) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[Culture] [nvarchar](5) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
 CONSTRAINT [PK_Localization] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCurrency]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCurrency](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrencyName] [nvarchar](500) NULL,
	[CurrencyDescription] [nvarchar](500) NULL,
	[CurrencyCode] [nvarchar](500) NULL,
	[CurrencySymbol] [nvarchar](50) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[IsActive] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblScreens]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblScreens](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ScreenName] [nvarchar](500) NULL,
	[ScreenTypeId] [int] NULL,
	[FeatureColour] [nvarchar](100) NULL,
	[Title] [nvarchar](500) NULL,
	[SubTitle] [nvarchar](500) NULL,
	[CurrencySymbol] [int] NULL,
	[BackgroundImage] [nvarchar](max) NULL,
	[ScreenOrder] [int] NULL,
	[HtmlCode] [nvarchar](max) NULL,
	[CurrencyFirst] [int] NULL,
	[CurrencyTwo] [int] NULL,
	[CurrencyThree] [int] NULL,
	[CurrencyFour] [int] NULL,
	[CurrencyFive] [int] NULL,
	[Image] [nvarchar](max) NULL,
	[ScreenFlag] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[IsActive] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblScreenType]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblScreenType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ScreenType] [nvarchar](500) NULL,
	[ScreenDescription] [nvarchar](500) NULL,
	[IsActive] [bit] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](150) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[Status] [int] NULL,
	[RoleId] [bigint] NULL,
 CONSTRAINT [PK_UserID] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserAuthToken]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserAuthToken](
	[UserAuthTokenID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[TokenKey] [uniqueidentifier] NOT NULL,
	[LoginDate] [datetime] NULL,
	[ExpiryDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
 CONSTRAINT [PK_UserAuthTokenID] PRIMARY KEY CLUSTERED 
(
	[UserAuthTokenID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[UserRoleId] [bigint] IDENTITY(1,1) NOT NULL,
	[Role] [nvarchar](500) NULL,
	[Status] [int] NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_ApplicationRole] PRIMARY KEY CLUSTERED 
(
	[UserRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[AccessRight] ON 

INSERT [dbo].[AccessRight] ([AccessRightsId], [UserId], [EntityType], [KorrectAccessRight], [SecurityPrincipalType], [AllowedRights], [DeniedRights], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [status]) VALUES (1, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[AccessRight] OFF
SET IDENTITY_INSERT [dbo].[tblCurrency] ON 

INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (1, N'Leke', NULL, N'ALL', N'Lek', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (2, N'Dollars', NULL, N'USD', N'$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (3, N'Afghanis', NULL, N'AFN', N'?', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (4, N'Pesos', NULL, N'ARS', N'$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (5, N'Guilders', NULL, N'AWG', N'ƒ', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (6, N'Dollars', NULL, N'AUD', N'$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (7, N'New Manats', NULL, N'AZN', N'???', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (8, N'Dollars', NULL, N'BSD', N'$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (9, N'Dollars', NULL, N'BBD', N'$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (10, N'Rubles', NULL, N'BYR', N'p.', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (11, N'Euro', NULL, N'EUR', N'€', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (12, N'Dollars', NULL, N'BZD', N'BZ$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (13, N'Dollars', NULL, N'BMD', N'$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (14, N'Bolivianos', NULL, N'BOB', N'$b', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (15, N'Convertible Marka', NULL, N'BAM', N'KM', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (16, N'Pula', NULL, N'BWP', N'P', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (17, N'Leva', NULL, N'BGN', N'??', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (18, N'Reais', NULL, N'BRL', N'R$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (19, N'Pounds', NULL, N'GBP', N'£', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (20, N'Dollars', NULL, N'BND', N'$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (21, N'Riels', NULL, N'KHR', N'?', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (22, N'Dollars', NULL, N'CAD', N'$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (23, N'Dollars', NULL, N'KYD', N'$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (24, N'Pesos', NULL, N'CLP', N'$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (25, N'Yuan Renminbi', NULL, N'CNY', N'¥', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (26, N'Pesos', NULL, N'COP', N'$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (27, N'Colón', NULL, N'CRC', N'¢', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (28, N'Kuna', NULL, N'HRK', N'kn', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (29, N'Pesos', NULL, N'CUP', N'?', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (30, N'Koruny', NULL, N'CZK', N'Kc', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (31, N'Kroner', NULL, N'DKK', N'kr', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (32, N'Pesos', NULL, N'DOP ', N'RD$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (33, N'Dollars', NULL, N'XCD', N'$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (34, N'Pounds', NULL, N'EGP', N'£', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (35, N'Colones', NULL, N'SVC', N'$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (36, N'Pounds', NULL, N'FKP', N'£', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (37, N'Dollars', NULL, N'FJD', N'$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (38, N'Cedis', NULL, N'GHC', N'¢', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (39, N'Pounds', NULL, N'GIP', N'£', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (40, N'Quetzales', NULL, N'GTQ', N'Q', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (41, N'Pounds', NULL, N'GGP', N'£', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (42, N'Dollars', NULL, N'GYD', N'$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (43, N'Lempiras', NULL, N'HNL', N'L', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (44, N'Dollars', NULL, N'HKD', N'$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (45, N'Forint', NULL, N'HUF', N'Ft', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (46, N'Kronur', NULL, N'ISK', N'kr', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (47, N'Rupees', NULL, N'INR', N'Rp', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (48, N'Rupiahs', NULL, N'IDR', N'Rp', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (49, N'Rials', NULL, N'IRR', N'?', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (50, N'Pounds', NULL, N'IMP', N'£', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (51, N'New Shekels', NULL, N'ILS', N'?', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (52, N'Dollars', NULL, N'JMD', N'J$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (53, N'Yen', NULL, N'JPY', N'¥', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (54, N'Pounds', NULL, N'JEP', N'£', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (55, N'Tenge', NULL, N'KZT', N'??', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (56, N'Won', NULL, N'KPW', N'?', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (57, N'Won', NULL, N'KRW', N'?', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (58, N'Soms', NULL, N'KGS', N'??', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (59, N'Kips', NULL, N'LAK', N'?', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (60, N'Lati', NULL, N'LVL', N'Ls', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (61, N'Pounds', NULL, N'LBP', N'£', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (62, N'Dollars', NULL, N'LRD', N'$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (63, N'Switzerland Francs', NULL, N'CHF', N'CHF', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (64, N'Litai', NULL, N'LTL', N'Lt', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (65, N'Denars', NULL, N'MKD', N'???', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (66, N'Ringgits', NULL, N'MYR', N'RM', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (67, N'Rupees', NULL, N'MUR', N'?', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (68, N'Pesos', NULL, N'MXN', N'$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (69, N'Tugriks', NULL, N'MNT', N'?', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (70, N'Meticais', NULL, N'MZN', N'MT', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (71, N'Dollars', NULL, N'NAD', N'$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (72, N'Rupees', NULL, N'NPR', N'?', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (73, N'Guilders', NULL, N'ANG', N'ƒ', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (74, N'Dollars', NULL, N'NZD', N'$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (75, N'Cordobas', NULL, N'NIO', N'C$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (76, N'Nairas', NULL, N'NGN', N'?', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (77, N'Krone', NULL, N'NOK', N'kr', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (78, N'Rials', NULL, N'OMR', N'?', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (79, N'Rupees', NULL, N'PKR', N'?', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (80, N'Balboa', NULL, N'PAB', N'B/.', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (81, N'Guarani', NULL, N'PYG', N'Gs', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (82, N'Nuevos Soles', NULL, N'PEN', N'S/.', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (83, N'Pesos', NULL, N'PHP', N'Php', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (84, N'Zlotych', NULL, N'PLN', N'zl', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (85, N'Rials', NULL, N'QAR', N'?', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (86, N'New Lei', NULL, N'RON', N'lei', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (87, N'Rubles', NULL, N'RUB', N'???', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (88, N'Pounds', NULL, N'SHP', N'£', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (89, N'Riyals', NULL, N'SAR', N'?', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (90, N'Dinars', NULL, N'RSD', N'???.', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (91, N'Rupees', NULL, N'SCR', N'?', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (92, N'Dollars', NULL, N'SGD', N'$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (93, N'Dollars', NULL, N'SBD', N'$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (94, N'Shillings', NULL, N'SOS', N'S', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (95, N'Rand', NULL, N'ZAR', N'R', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (96, N'Rupees', NULL, N'LKR', N'?', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (97, N'Kronor', NULL, N'SEK', N'kr', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (98, N'Dollars', NULL, N'SRD', N'$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (99, N'Pounds', NULL, N'SYP', N'£', NULL, NULL, NULL, NULL, 1)
GO
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (100, N'New Dollars', NULL, N'TWD', N'NT$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (101, N'Baht', NULL, N'THB', N'?', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (102, N'Dollars', NULL, N'TTD', N'TT$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (103, N'Lira', NULL, N'TRY', N'TL', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (104, N'Liras', NULL, N'TRL', N'£', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (105, N'Dollars', NULL, N'TVD', N'$', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (106, N'Hryvnia', NULL, N'UAH', N'?', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (107, N'Pesos', NULL, N'UYU', N'$U', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (108, N'Sums', NULL, N'UZS', N'??', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (109, N'Bolivares Fuertes', NULL, N'VEF', N'Bs', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (110, N'Dong', NULL, N'VND', N'?', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (111, N'Rials', NULL, N'YER', N'?', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive]) VALUES (112, N'Zimbabwe Dollars', NULL, N'ZWD', N'Z$', NULL, NULL, NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[tblCurrency] OFF
SET IDENTITY_INSERT [dbo].[tblScreens] ON 

INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (2, N'fgh', 1, N'fghfgh', N'fghfhg', N'ghfhg', 36, NULL, 2, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(N'2018-12-16T01:55:43.930' AS DateTime), 0, CAST(N'2018-12-16T01:55:43.930' AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (3, N'Test1', 1, N'Test3', N'Test4', N'Test 5', 36, NULL, 8, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(N'2018-12-16T02:00:08.900' AS DateTime), 0, CAST(N'2018-12-16T02:00:08.900' AS DateTime), NULL, 1)
SET IDENTITY_INSERT [dbo].[tblScreens] OFF
SET IDENTITY_INSERT [dbo].[tblScreenType] ON 

INSERT [dbo].[tblScreenType] ([Id], [ScreenType], [ScreenDescription], [IsActive], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn]) VALUES (1, N'PromoScreen', N'Promo screen', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblScreenType] ([Id], [ScreenType], [ScreenDescription], [IsActive], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn]) VALUES (2, N'HTML', N'html', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblScreenType] ([Id], [ScreenType], [ScreenDescription], [IsActive], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn]) VALUES (3, N'PromoScreen2', N'Promo screen', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblScreenType] ([Id], [ScreenType], [ScreenDescription], [IsActive], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn]) VALUES (4, N'ImageScreen', N'Image screen', 1, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblScreenType] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([UserID], [FirstName], [LastName], [Email], [Password], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Status], [RoleId]) VALUES (1, N'dev', N'dev', N'dev@dev.com', N'QEbdqkVOX0yjRxKbJ4KFYW9MDUhj58JRbhDaF5fcXA171k2GHCvbqAvMKWp0KVCuWwMVK0DCizoHC9mwJ8/5uVhflPdDq+g=', NULL, NULL, CAST(N'2018-09-11T01:54:04.610' AS DateTime), NULL, 1, 1)
SET IDENTITY_INSERT [dbo].[User] OFF
SET IDENTITY_INSERT [dbo].[UserAuthToken] ON 

INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1, 1, N'2116b9b1-065f-4f67-a8c8-33338ecac5c2', CAST(N'2018-12-10T23:44:00.000' AS DateTime), CAST(N'2018-12-11T05:44:57.880' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (2, 1, N'6d40a898-b80f-402b-b9ff-e8033b515ceb', CAST(N'2018-12-14T16:25:00.000' AS DateTime), CAST(N'2018-12-14T22:25:40.457' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (3, 1, N'cf5feea0-3c18-4715-8f06-800afdf073e3', CAST(N'2018-12-14T16:25:00.000' AS DateTime), CAST(N'2018-12-14T22:25:40.457' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (4, 1, N'b77a0d99-ff09-4075-ae22-60c4e44d37ea', CAST(N'2018-12-14T16:25:00.000' AS DateTime), CAST(N'2018-12-14T22:25:40.477' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (5, 1, N'ad458b6f-c2b1-40ea-9b55-c25d1173d888', CAST(N'2018-12-14T16:25:00.000' AS DateTime), CAST(N'2018-12-14T22:25:40.613' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (6, 1, N'6e2bf9d2-eb3a-45dd-8ddd-96aa68150f3a', CAST(N'2018-12-14T16:25:00.000' AS DateTime), CAST(N'2018-12-14T22:25:40.613' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1003, 1, N'67346e5e-88ab-47e8-9782-5c5f01123489', CAST(N'2018-12-16T21:22:00.000' AS DateTime), CAST(N'2018-12-17T03:22:37.260' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1004, 1, N'c7a4057e-bfc4-4c91-897f-a698364bdf56', CAST(N'2018-12-17T22:39:00.000' AS DateTime), CAST(N'2018-12-18T04:39:52.670' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1005, 1, N'5cf84ac5-c7d2-41ab-81bd-aafabba315c7', CAST(N'2018-12-17T22:39:00.000' AS DateTime), CAST(N'2018-12-18T04:39:52.670' AS DateTime), NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[UserAuthToken] OFF
/****** Object:  StoredProcedure [dbo].[AccessRight_Select]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[AccessRight_Select] 
  @userId INT
 
AS

  SELECT [AccessRightsId]
     ,[UserId]
     ,[EntityType]
     ,[KorrectAccessRight]
     ,[SecurityPrincipalType]
     ,[AllowedRights]
     ,[DeniedRights]
    FROM [AccessRight]
    --INNER JOIN [USER] ON [AccessRight].UserId = [USER].UserID
  
  WHERE UserId = @userId


GO
/****** Object:  StoredProcedure [dbo].[PROC_SCREENDATAUPDATION]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*************************************************************************************************************
-------------------------------------------------------------------------------------  
PROCEDURE NAME  :	PROC_SCREENDATAUPDATION 
PURPOSE         :	SCREEN DATA INSERTION AND UPDATION
AUTHOR          :	MANSI PORWAL
DATE WRITTEN	:	13-DEC-2018
TEST SCRIPT		:

REVISION HISTORY:
DATE           DEVELOPER       MODIFICATION
-------------------------------------------------------------------------------------   
EXEC PROC_SCREENDATAUPDATION @P_PLANTCODE=N'N',@P_SYSCODE=N'S',@P_USER=N'MANSIPORWAL',@P_MODULEID=N'',@P_SUBMODULEID=N'',
@P_SAVESTRING=N'',@P_SEARCHSTRING=N'',@P_SORTSTRING=N'',@P_MODE=N'U',@P_DATEFORMAT=120,
@P_USERID=N'MANSIPORWAL',@P_SESSION_ID=N'D5TBXZGSPMTXJ0RRQHOJMUBT',@P_RESOURCESTRING=N'',@P_ERRORMSG=N'',@P_FOCUS=N''
**************************************************************************************************************/

CREATE PROC [dbo].[PROC_SCREENDATAUPDATION]  
( 
	@Id					INT OUTPUT,
    @ScreenName			NVARCHAR(500) = NULL,
    @ScreenType			INT = NULL, 
	@FeatureColour		NVARCHAR(500) = NULL, 
	@Title				NVARCHAR(500) = NULL, 
	@SubTitle			NVARCHAR(500) = NULL,  
	@CurrencyCode		INT = NULL,  
	@ScreenOrder		INT = NULL,  	
	@BackgroundImage	NVARCHAR(500) = NULL,  
	@HtmlCode			NVARCHAR(MAX) = NULL,
	@CurrencyFirst		INT = NULL,  
	@CurrencyTwo		INT = NULL,
	@CurrencyThree		INT = NULL,
	@CurrencyFour		INT = NULL,
	@CurrencyFive		int = NULL,  
	@Image				NVARCHAR(500) = NULL,
	@ScreenFlag			INT = NULL,
	@IsActive			bit = NULL,
	@Status				int	= NULL,
	@UserId				int	= NULL
)
AS 

BEGIN
	
	--IF(@UPSERTFLAG=1)
	--BEGIN
	--Select * From tblScreens
	--PRINT '11'
	INSERT INTO tblScreens
			(ScreenName ,ScreenTypeId  ,FeatureColour   ,Title     ,SubTitle   ,CurrencySymbol  ,BackgroundImage
      ,ScreenOrder      ,HtmlCode      ,CurrencyFirst      ,CurrencyTwo      ,CurrencyThree      ,CurrencyFour   ,CurrencyFive
      ,[Image]   ,ScreenFlag   ,CreatedOn     ,UpdatedOn   ,IsActive,CreatedBy	)
			VALUES
			(@ScreenName,@ScreenType,@FeatureColour,@Title,@SubTitle,@CurrencyCode,@BackgroundImage,@ScreenOrder,@HtmlCode,
			@CurrencyFirst,@CurrencyTwo      ,@CurrencyThree      ,@CurrencyFour      ,@CurrencyFive,@Image,@ScreenFlag,GETDATE(),GETDATE(),1,@UserId
			)
	--END
END




GO
/****** Object:  StoredProcedure [dbo].[TableFilteredRow]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[TableFilteredRow]
	(@TableName VARCHAR(500)
	,@Filters VARCHAR(500) = null
	,@SortExpression VARCHAR(500) = null
	,@Columns VARCHAR(MAX) = null
	,@PageSize INT = null
	,@Page INT = null)
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @SqlStatement VARCHAR(MAX);

	IF (@Columns IS NULL OR LEN(@Columns) = 0)
	BEGIN
		SET @Columns = '*';
	END	

	SET @SqlStatement = 'SELECT ' + @Columns + ' FROM [' + LTRIM(@TableName) + '] WHERE 1 = 1';
	IF (@Filters IS NOT NULL AND LEN(@Filters) > 0)
	BEGIN
		SET @SqlStatement = @SqlStatement + ' AND ' + @Filters;
	END

	IF (@SortExpression IS NOT NULL AND LEN(@SortExpression) > 0)
	BEGIN
		SET @SqlStatement = @SqlStatement + ' ORDER BY ' + @SortExpression;
	END

	EXEC(@SqlStatement);
END

GO
/****** Object:  StoredProcedure [dbo].[User_Search]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[User_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable READONLY
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END
IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end
		Select *, FirstName + ' ' + LastName As FullName from [User] where Status <> 0




SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


GO
/****** Object:  StoredProcedure [dbo].[User_UpSert]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE  PROCEDURE [dbo].[User_UpSert] 	 
	 @Id INT OUT
	, @RoleId bigint = null
	, @SubscriptionType bigint = null
	, @BechmarkingCompanyId bigint = null
	, @Country bigint = null
	, @FirstName varchar(200) = null
	, @LastName varchar(200) = null
	, @email varchar(200) = null
	, @Password varchar(200) = null
	, @Status bigint = null
	, @CreatedBy bigint = null
	, @ModifiedBy bigint = null
	
	
	



AS

if(@Id > 0)
BEGIN

	update [User] set FirstName = @FirstName,
	LastName = @LastName,
	Email = @email,
	RoleId = @RoleId
	where UserID = @Id	

END

ELSE BEGIN

		INSERT INTO [dbo].[User]
           ([FirstName]
           ,[LastName]
           ,[Email]
           ,[Password]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[RoleId]
		   ,Status)
     VALUES
           (@FirstName
           ,@LastName
           ,@email
           ,@Password
           ,GETDATE()
           ,@CreatedBy
           ,@RoleId
		   ,@Status)

		   SET @Id = SCOPE_IDENTITY()
END
		   

GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Delete]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[UserAuthToken_Delete] 
	 @Id INT
	
AS

		DELETE
		
		FROM	UserAuthToken
		
		WHERE	UserAuthTokenID = @Id



GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Logout]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[UserAuthToken_Logout] 
    @TokenKey UNIQUEIDENTIFIER = NULL
	
AS

		UPDATE 	UserAuthToken

		SET		ExpiryDate = GETDATE()
						
		WHERE	TokenKey = @TokenKey 



GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Select]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE  PROCEDURE [dbo].[UserAuthToken_Select] 
	 @Email NVARCHAR(100) = NULL
   , @TokenKey UNIQUEIDENTIFIER = NULL
	
AS

		SELECT 	UserAuthTokenID
				, UserAuthToken.UserId
				, TokenKey
				, LoginDate	
				, ExpiryDate

				, FirstName
				, LastName
				, Email
				, [Password]
			
		FROM	UserAuthToken
				INNER JOIN [USER] ON UserAuthToken.UserId = [USER].UserID				
		
		WHERE	Email = CASE WHEN @Email IS NOT NULL OR @Email <> '' THEN @Email ELSE Email END
				AND TokenKey = CASE WHEN @TokenKey IS NOT NULL OR @TokenKey <> '' THEN @TokenKey ELSE TokenKey END
				AND ExpiryDate > GETDATE()


GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_UpSert]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[UserAuthToken_UpSert] 
	  @Id INT OUT
	, @UserId INT
	, @TokenKey UNIQUEIDENTIFIER
	, @LoginDate NVARCHAR(100)
	, @ExpiryDate DATETIME
	, @ExpiryHours INT
	, @Status INT 	

AS

		IF EXISTS (SELECT * FROM UserAuthToken WHERE UserId = @UserId AND ExpiryDate > Getdate()) 
			BEGIN

				UPDATE	[UserAuthToken] 

				SET		@Id = UserAuthTokenID
						, ExpiryDate = DATEADD(hh, @ExpiryHours, getdate())

				WHERE UserId = @UserId AND ExpiryDate > GETDATE()

			END

		ELSE
			BEGIN

				INSERT INTO [UserAuthToken]  (UserId,  TokenKey,  LoginDate, ExpiryDate)
							Values	(@UserId, @TokenKey, @LoginDate, DATEADD(hh, @ExpiryHours, getdate()))

				SET @Id = SCOPE_IDENTITY()

			END


GO
/****** Object:  StoredProcedure [dbo].[UserRole_Specific_Search]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UserRole_Specific_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END
IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end
	Select * from UserRole Where Role like '%'+@searchText+'%' and UserRoleId <> 1
	



SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


GO
/****** Object:  StoredProcedure [dbo].[Users_List_Search]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Users_List_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly 
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END
IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end

SELECT        u.UserID, u.FirstName, u.LastName, u.Email, IsNull(u.Status,0) 'Status', u.RoleId , ur.Role 'Role'
FROM            [User] u
left join UserRole UR on ur.UserRoleId = u.RoleId

where u.Status <> 0 and u.RoleId is not nUll

SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 


GO
/****** Object:  StoredProcedure [dbo].[UsersRegion_UpSert]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE  PROCEDURE [dbo].[UsersRegion_UpSert] 	 
	 @Id INT OUT
	, @RegionId bigint = null
	, @UserId bigint = null	
	, @Status bigint = null
	, @CreatedBy bigint = null
	, @ModifiedBy bigint = null
	
	
	

AS


--- delete all then insert

Delete UsersRegion where UserId = @UserId


		INSERT INTO [dbo].[UsersRegion]
           (
		   RegionId
		   ,UserId
           ,[CreatedDate]
           ,[CreatedBy]          
		   ,Status)
     VALUES
           (@RegionId
           ,@UserId                      
           ,GETDATE()
           ,@CreatedBy           
		   ,@Status)

		   SET @Id = SCOPE_IDENTITY()

GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteScreen]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_DeleteScreen]
	-- Add the parameters for the stored procedure here
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Delete tblScreens Where Id = @id
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAll_Currencies]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAll_Currencies] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT	Id				,
			CurrencyName	,
			CurrencyCode	,
			CurrencySymbol 
	FROM	tblCurrency
	WHERE	IsActive = 1

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAll_ScreenTypes]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAll_ScreenTypes]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT	Id					,
			ScreenType			,
			ScreenDescription 
	FROM	tblScreenType 
	WHERE	IsActive	=	1
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllScreensData]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAllScreensData] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT 
	TS.Id				,
	TS.ScreenName		,
	TST.ScreenType		,
	TS.FeatureColour	,
	TS.Title			,
	TS.SubTitle			,
	TC.CurrencyCode		,
	TS.ScreenOrder		,
	TS.BackgroundImage	,
	TS.HtmlCode			,
	TS.CurrencyFirst	,
	TS.CurrencyTwo		,
	TS.CurrencyThree	,
	TS.CurrencyFour		,
	TS.CurrencyFive		,
	TS.[Image]			,
	TS.ScreenFlag		,
	TS.IsActive


	FROM	tblScreens		TS										LEFT JOIN 
			tblScreenType	TST		ON	TST.Id	=	TS.ScreenTypeID	LEFT JOIN 
			tblCurrency		TC		ON	TC.ID	=	TS.CurrencySymbol 
	WHERE TS.IsActive = 1
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetScreen_ById]    Script Date: 21-12-2018 22:39:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetScreen_ById]
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Select Id,ScreenName,ScreenTypeId,
	FeatureColour,
	Title,
	SubTitle,
	CurrencySymbol
	BackgroundImage,
	ScreenOrder,
	HtmlCode,
	CurrencyFirst,
	CurrencyTwo,
	CurrencyThree,
	CurrencyFour,
	CurrencyFive,
	[Image]	
	from tblScreens
	Where Id = @id
END
GO
USE [master]
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET  READ_WRITE 
GO
