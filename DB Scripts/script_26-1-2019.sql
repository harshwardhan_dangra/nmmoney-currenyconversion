USE [master]
GO
/****** Object:  Database [NMoneyCurrencyConversion]    Script Date: 01/26/2019 6:30:02 PM ******/
CREATE DATABASE [NMoneyCurrencyConversion]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'NMoneyCurrencyConversion', FILENAME = N'C:\Users\HP\NMoneyCurrencyConversion.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'NMoneyCurrencyConversion_log', FILENAME = N'C:\Users\HP\NMoneyCurrencyConversion_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [NMoneyCurrencyConversion].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET ARITHABORT OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET  DISABLE_BROKER 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET  MULTI_USER 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET DB_CHAINING OFF 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [NMoneyCurrencyConversion]
GO
/****** Object:  UserDefinedTableType [dbo].[ItemsTable]    Script Date: 01/26/2019 6:30:02 PM ******/
CREATE TYPE [dbo].[ItemsTable] AS TABLE(
	[Key] [nvarchar](150) NULL,
	[Value] [nvarchar](max) NULL
)
GO
/****** Object:  StoredProcedure [dbo].[AccessRight_Select]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[AccessRight_Select] 
  @userId INT
 
AS

  SELECT [AccessRightsId]
     ,[UserId]
     ,[EntityType]
     ,[KorrectAccessRight]
     ,[SecurityPrincipalType]
     ,[AllowedRights]
     ,[DeniedRights]
    FROM [AccessRight]
    --INNER JOIN [USER] ON [AccessRight].UserId = [USER].UserID
  
  WHERE UserId = @userId



GO
/****** Object:  StoredProcedure [dbo].[PROC_SCREENDATAUPDATION]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*************************************************************************************************************
-------------------------------------------------------------------------------------  
PROCEDURE NAME  :	PROC_SCREENDATAUPDATION 
PURPOSE         :	SCREEN DATA INSERTION AND UPDATION
AUTHOR          :	MANSI PORWAL
DATE WRITTEN	:	13-DEC-2018
TEST SCRIPT		:

REVISION HISTORY:
DATE           DEVELOPER       MODIFICATION
-------------------------------------------------------------------------------------   
EXEC PROC_SCREENDATAUPDATION @P_PLANTCODE=N'N',@P_SYSCODE=N'S',@P_USER=N'MANSIPORWAL',@P_MODULEID=N'',@P_SUBMODULEID=N'',
@P_SAVESTRING=N'',@P_SEARCHSTRING=N'',@P_SORTSTRING=N'',@P_MODE=N'U',@P_DATEFORMAT=120,
@P_USERID=N'MANSIPORWAL',@P_SESSION_ID=N'D5TBXZGSPMTXJ0RRQHOJMUBT',@P_RESOURCESTRING=N'',@P_ERRORMSG=N'',@P_FOCUS=N''
**************************************************************************************************************/

CREATE PROC [dbo].[PROC_SCREENDATAUPDATION]  
( 
	@Id					INT OUTPUT,
    @ScreenName			NVARCHAR(500) = NULL,
    @ScreenType			INT = NULL, 
	@FeatureColour		NVARCHAR(500) = NULL, 
	@Title				NVARCHAR(500) = NULL, 
	@SubTitle			NVARCHAR(500) = NULL,  
	@CurrencyCode		INT = NULL,  
	@ScreenOrder		INT = NULL,  	
	@BackgroundImage	NVARCHAR(500) = NULL,  
	@HtmlCode			NVARCHAR(MAX) = NULL,
	@CurrencyFirst		INT = NULL,  
	@CurrencyTwo		INT = NULL,
	@CurrencyThree		INT = NULL,
	@CurrencyFour		INT = NULL,
	@CurrencyFive		int = NULL,  
	@Image				NVARCHAR(500) = NULL,
	@ScreenFlag			INT = NULL,
	@IsActive			bit = NULL,
	@Status				int	= NULL,
	@UserId				int	= NULL
)
AS 

BEGIN
	
	--IF(@UPSERTFLAG=1)
	--BEGIN
	--Select * From tblScreens
	--PRINT '11'
	INSERT INTO tblScreens
			(ScreenName ,ScreenTypeId  ,FeatureColour   ,Title     ,SubTitle   ,CurrencySymbol  ,BackgroundImage
      ,ScreenOrder      ,HtmlCode      ,CurrencyFirst      ,CurrencyTwo      ,CurrencyThree      ,CurrencyFour   ,CurrencyFive
      ,[Image]   ,ScreenFlag   ,CreatedOn     ,UpdatedOn   ,IsActive,CreatedBy	)
			VALUES
			(@ScreenName,@ScreenType,@FeatureColour,@Title,@SubTitle,@CurrencyCode,@BackgroundImage,@ScreenOrder,@HtmlCode,
			@CurrencyFirst,@CurrencyTwo      ,@CurrencyThree      ,@CurrencyFour      ,@CurrencyFive,@Image,@ScreenFlag,GETDATE(),GETDATE(),1,@UserId
			)
	--END
END





GO
/****** Object:  StoredProcedure [dbo].[Screen_Upsert]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*************************************************************************************************************
=============================================
Author:  Satender.Siwach	
Create date:    26 December, 2018
Description:	This proc is developed for insert a new screen record or update existing screen data
				

Change Log:
26 December, 2018 SS Initial Development
 
=============================================
**************************************************************************************************************/

CREATE PROC [dbo].[Screen_Upsert]  
( 
	@Id					INT OUT,
    @ScreenName			NVARCHAR(500) = NULL,
    @ScreenType			INT = NULL, 
	@FeatureColour		NVARCHAR(500) = NULL, 
	@Title				NVARCHAR(500) = NULL, 
	@SubTitle			NVARCHAR(500) = NULL,  
	@CurrencyCode		INT = NULL,  
	@ScreenOrder		NVARCHAR(100) = NULL,  	
	@BackgroundImage	Int = NULL,  
	@HtmlCode			NVARCHAR(MAX) = NULL,
	@CurrencyFirst		INT = NULL,  
	@CurrencyTwo		INT = NULL,
	@CurrencyThree		INT = NULL,
	@CurrencyFour		INT = NULL,
	@CurrencyFive		int = NULL,  
	@Image				int = NULL,
	@ScreenFlag			INT = NULL,
	@Status 			bit = NULL,
	@UserId				int	= NULL
)
AS 

BEGIN
	
	if(@Id >0)
	begin
	update tblScreens 
	set
	ScreenName=@ScreenName,
	ScreenTypeId=@ScreenType,
	FeatureColour=@FeatureColour,
	Title=@Title,
	SubTitle=@SubTitle,
	CurrencySymbol=@CurrencyCode,
	BackgroundImage=@BackgroundImage,
	ScreenOrder=@ScreenOrder,
	HtmlCode=@HtmlCode,
	CurrencyFirst=@CurrencyFirst,
	CurrencyTwo=@CurrencyTwo,
	CurrencyThree=@CurrencyThree,
	CurrencyFour=@CurrencyFour,
	CurrencyFive=@CurrencyFive,
	[Image]=@Image,
	ScreenFlag=@ScreenFlag,
	UpdatedOn=GETDATE(),
	IsActive=@Status,
	UpdatedBy=@UserId
	where Id=@Id
	end
	else
	begin

	INSERT INTO tblScreens
			(ScreenName ,ScreenTypeId  ,FeatureColour   ,Title     ,SubTitle   ,CurrencySymbol  ,BackgroundImage
      ,ScreenOrder      ,HtmlCode      ,CurrencyFirst      ,CurrencyTwo      ,CurrencyThree      ,CurrencyFour   ,CurrencyFive
      ,[Image]   ,ScreenFlag   ,CreatedOn     ,UpdatedOn   ,IsActive,CreatedBy	)
			VALUES
			(@ScreenName,@ScreenType,@FeatureColour,@Title,@SubTitle,@CurrencyCode,@BackgroundImage,@ScreenOrder,@HtmlCode,
			@CurrencyFirst,@CurrencyTwo      ,@CurrencyThree      ,@CurrencyFour      ,@CurrencyFive,@Image,@ScreenFlag,GETDATE(),GETDATE(),1,@UserId
			)

		set @Id=SCOPE_IDENTITY()
	END

	select * from tblScreens where id=@Id

END

GO
/****** Object:  StoredProcedure [dbo].[TableFilteredRow]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[TableFilteredRow]
	(@TableName VARCHAR(500)
	,@Filters VARCHAR(500) = null
	,@SortExpression VARCHAR(500) = null
	,@Columns VARCHAR(MAX) = null
	,@PageSize INT = null
	,@Page INT = null)
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @SqlStatement VARCHAR(MAX);

	IF (@Columns IS NULL OR LEN(@Columns) = 0)
	BEGIN
		SET @Columns = '*';
	END	

	SET @SqlStatement = 'SELECT ' + @Columns + ' FROM [' + LTRIM(@TableName) + '] WHERE 1 = 1';
	IF (@Filters IS NOT NULL AND LEN(@Filters) > 0)
	BEGIN
		SET @SqlStatement = @SqlStatement + ' AND ' + @Filters;
	END

	IF (@SortExpression IS NOT NULL AND LEN(@SortExpression) > 0)
	BEGIN
		SET @SqlStatement = @SqlStatement + ' ORDER BY ' + @SortExpression;
	END

	EXEC(@SqlStatement);
END


GO
/****** Object:  StoredProcedure [dbo].[User_Search]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[User_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable READONLY
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END
IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end
	SELECT        u.UserID, u.FirstName, u.LastName, u.Email, IsNull(u.Status,0) 'Status', u.RoleId , ur.Role 'Role'
FROM            [User] u
left join UserRole UR on ur.UserRoleId = u.RoleId

where u.Status <> 0 and u.RoleId is not nUll and  u.RoleId <> 1



SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 



GO
/****** Object:  StoredProcedure [dbo].[User_Select_ById]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[User_Select_ById]

@UserId int

AS

Select u.*,ur.Role from [User] u
Left join UserRole ur on u.RoleId = ur.UserRoleId
 Where UserID = CASE WHEN @UserId IS NOT NULL THEN @UserId ELSE UserID END 


GO
/****** Object:  StoredProcedure [dbo].[User_UpSert]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE  PROCEDURE [dbo].[User_UpSert] 	 
	 @Id INT OUT
	, @RoleId bigint = null
	, @FirstName varchar(200) = null
	, @LastName varchar(200) = null
	, @email varchar(200) = null
	, @Password varchar(200) = null
	, @Status bigint = null
	, @CreatedBy bigint = null
	, @ModifiedBy bigint = null
	,@ContactNumber varchar(max) = null
	
	
	



AS

if(@Id > 0)
BEGIN

	update [User] set FirstName = @FirstName,
	LastName = @LastName,
	Email = @email,
	RoleId = @RoleId,
	ContactNumber = @ContactNumber,
	Password = @Password,
	ModifiedDate = GETDATE(),
	ModifiedBy = @ModifiedBy
	where UserID = @Id	

END

ELSE BEGIN

		INSERT INTO [dbo].[User]
           ([FirstName]
           ,[LastName]
           ,[Email]
           ,[Password]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[RoleId]
		   ,ContactNumber
		   ,Status)
     VALUES
           (@FirstName
           ,@LastName
           ,@email
           ,@Password
           ,GETDATE()
           ,@CreatedBy
           ,@RoleId
		   ,@ContactNumber
		   ,@Status)

		   SET @Id = SCOPE_IDENTITY()
END
		   


GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Delete]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[UserAuthToken_Delete] 
	 @Id INT
	
AS

		DELETE
		
		FROM	UserAuthToken
		
		WHERE	UserAuthTokenID = @Id




GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Logout]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[UserAuthToken_Logout] 
    @TokenKey UNIQUEIDENTIFIER = NULL
	
AS

		UPDATE 	UserAuthToken

		SET		ExpiryDate = GETDATE()
						
		WHERE	TokenKey = @TokenKey 




GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_Select]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE  PROCEDURE [dbo].[UserAuthToken_Select] 
	 @Email NVARCHAR(100) = NULL
   , @TokenKey UNIQUEIDENTIFIER = NULL
	
AS

		SELECT 	UserAuthTokenID
				, UserAuthToken.UserId
				, TokenKey
				, LoginDate	
				, ExpiryDate

				, FirstName
				, LastName
				, Email
				, [Password]
			
		FROM	UserAuthToken
				INNER JOIN [USER] ON UserAuthToken.UserId = [USER].UserID				
		
		WHERE	Email = CASE WHEN @Email IS NOT NULL OR @Email <> '' THEN @Email ELSE Email END
				AND TokenKey = CASE WHEN @TokenKey IS NOT NULL OR @TokenKey <> '' THEN @TokenKey ELSE TokenKey END
				AND ExpiryDate > GETDATE()



GO
/****** Object:  StoredProcedure [dbo].[UserAuthToken_UpSert]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE  PROCEDURE [dbo].[UserAuthToken_UpSert] 
	  @Id INT OUT
	, @UserId INT
	, @TokenKey UNIQUEIDENTIFIER
	, @LoginDate NVARCHAR(100)
	, @ExpiryDate DATETIME
	, @ExpiryHours INT
	, @Status INT 	

AS

		IF EXISTS (SELECT * FROM UserAuthToken WHERE UserId = @UserId AND ExpiryDate > Getdate()) 
			BEGIN

				UPDATE	[UserAuthToken] 

				SET		@Id = UserAuthTokenID
						, ExpiryDate = DATEADD(hh, @ExpiryHours, getdate())

				WHERE UserId = @UserId AND ExpiryDate > GETDATE()

			END

		ELSE
			BEGIN

				INSERT INTO [UserAuthToken]  (UserId,  TokenKey,  LoginDate, ExpiryDate)
							Values	(@UserId, @TokenKey, @LoginDate, DATEADD(hh, @ExpiryHours, getdate()))

				SET @Id = SCOPE_IDENTITY()

			END



GO
/****** Object:  StoredProcedure [dbo].[UserRole_Specific_Search]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UserRole_Specific_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END
IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end
	Select * from UserRole Where UserRoleId <> 1
	



SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 



GO
/****** Object:  StoredProcedure [dbo].[Users_List_Search]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Users_List_Search]
	-- Add the parameters for the stored procedure here
	@ItemsTable ItemsTable readonly 
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @flag varchar(max) = null,
		@searchText varchar(max) = null

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'flag')
BEGIN
	SET @flag =(Select Value FROM  @ItemsTable WHERE [KEY] = 'flag')
END

IF EXISTS (SELECT 1 FROM @ItemsTable WHERE [Key] = 'searchText')
BEGIN
	SET @searchText =(Select Value FROM  @ItemsTable WHERE [KEY] = 'searchText')
END
IF @searchText is not null
begin
 SET @searchText = (SELECT dbo.Urldecode(@searchText))
end

SELECT        u.UserID, u.FirstName, u.LastName, u.Email, IsNull(u.Status,0) 'Status', u.RoleId , ur.Role 'Role'
FROM            [User] u
left join UserRole UR on ur.UserRoleId = u.RoleId

where u.Status <> 0 and u.RoleId is not nUll and  u.RoleId <> 1

SET TRANSACTION ISOLATION LEVEL READ COMMITTED; 



GO
/****** Object:  StoredProcedure [dbo].[UsersRegion_UpSert]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE  PROCEDURE [dbo].[UsersRegion_UpSert] 	 
	 @Id INT OUT
	, @RegionId bigint = null
	, @UserId bigint = null	
	, @Status bigint = null
	, @CreatedBy bigint = null
	, @ModifiedBy bigint = null
	
	
	

AS


--- delete all then insert

Delete UsersRegion where UserId = @UserId


		INSERT INTO [dbo].[UsersRegion]
           (
		   RegionId
		   ,UserId
           ,[CreatedDate]
           ,[CreatedBy]          
		   ,Status)
     VALUES
           (@RegionId
           ,@UserId                      
           ,GETDATE()
           ,@CreatedBy           
		   ,@Status)

		   SET @Id = SCOPE_IDENTITY()


GO
/****** Object:  StoredProcedure [dbo].[USP_Currency_DeleteById]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[USP_Currency_DeleteById]
	-- Add the parameters for the stored procedure here
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Delete tblCurrency Where Id = @id
END

GO
/****** Object:  StoredProcedure [dbo].[USP_Currency_Upsert]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*************************************************************************************************************
=============================================
Author:  Satender.Siwach	
Create date:    12 January, 2019
Description:	This proc is developed for delete currency data by id
				

Change Log:
12 January, 2019 SS Initial Development
 
=============================================
**************************************************************************************************************/
--create PROCEDURE [dbo].[USP_Currency_DeleteById]
--	@id INT
--AS
--BEGIN
--	-- SET NOCOUNT ON added to prevent extra result sets from
--	-- interfering with SELECT statements.
--	SET NOCOUNT ON;
--	Delete [tblCurrency] Where Id = @id
--END
--GO
--/***** Object:  StoredProcedure [dbo].[USP_Currency_Upsert]    Script Date: 1/12/2019 9:49:57 PM *****/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO


/*************************************************************************************************************
=============================================
Author:  Satender.Siwach	
Create date:    12 January, 2019
Description:	This proc is developed for insert a new currency record or update existing currency data
				

Change Log:
12 January, 2019 SS Initial Development
 
=============================================
**************************************************************************************************************/

CREATE PROC [dbo].[USP_Currency_Upsert]  
( 
	@Id					INT OUT,
    @CurrencyName			NVARCHAR(500) = NULL,   
	@CurrencyDescription NVARCHAR(500) = NULL, 
	@CurrencyCode				NVARCHAR(500) = NULL, 
	@CurrencySymbol			NVARCHAR(500) = NULL,  
	@ImageId		BIGINT = NULL,  
	@Status			bit = NULL,
	@UserId				int	= NULL
)
AS 

BEGIN
	
	if(@Id >0)
	begin
	update tblCurrency 
	set
	CurrencyName=@CurrencyName,
	CurrencyDescription=@CurrencyDescription,
	CurrencyCode=@CurrencyCode,
	CurrencySymbol=@CurrencySymbol,
	ImageId=@ImageId,
	UpdatedOn=GETDATE(),
	IsActive=@Status,
	UpdatedBy=@UserId
	where Id=@Id
	end
	else
	begin

	INSERT INTO tblCurrency
			([CurrencyName]
      ,[CurrencyDescription]
      ,[CurrencyCode]
      ,[CurrencySymbol]
      ,[CreatedOn]
      ,[CreatedBy]      
      ,[UpdatedOn]
	  ,[UpdatedBy]
      ,[IsActive]
	 , [ImageId]	)
			VALUES
			(
	   @CurrencyName
      ,@CurrencyDescription
      ,@CurrencyCode
      ,@CurrencySymbol	 
			,GETDATE(),@UserId,GETDATE(),@UserId,1, @ImageId
			)

		set @Id=SCOPE_IDENTITY()
	END

	select * from tblCurrency where id=@Id

END





GO
/****** Object:  StoredProcedure [dbo].[USP_CurrencyRateUpsert]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_CurrencyRateUpsert]
	@Date	nvarchar(50) = null,
	@Slot	int      = null,
	@Time	nvarchar(50) = null
AS
BEGIN
    Declare @count int =0;
	 Set @count = Case when (Select Count from tblCurrencyRate Where Date = @Date And Slots = @Slot) > 0 THEN (Select Count from tblCurrencyRate Where Date = @Date And Slots = @Slot) + 1 ELSE 1 END
	if exists (select 1 from tblCurrencyRate where date = @Date AND Slots = @Slot)
	BEGIN
	  Update tblCurrencyRate SET Count = @count where date = @Date AND Slots = @Slot
	END
	ELSE
	BEGIN
	 INSERT INTO tblCurrencyRate (Date,Time,Slots,Count)values(@Date,@Time,@Slot,@count)
	END

END

GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteImage]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satender.Siwach>
-- Create date: <06 January, 2019>
-- Description:	<to delete image information by id>
-- Logs
-- 06 January, 2019 SS Initial Development
-- =============================================
CREATE PROCEDURE [dbo].[USP_DeleteImage]
	@id bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	delete from [tblImage] Where Id = @id
END

GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteScreen]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_DeleteScreen]
	-- Add the parameters for the stored procedure here
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Delete tblScreens Where Id = @id
END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetAll_Currencies]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAll_Currencies] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT	Id				,
			CurrencyName	,
			CurrencyCode	,
			CurrencySymbol  ,
			ImageId			,
			(Select FileName From tblImage WHERE Id = ImageId) as ImageUrl 
	FROM	tblCurrency
	WHERE	IsActive = 1

END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetAll_ScreenTypes]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAll_ScreenTypes]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT	Id					,
			ScreenType			,
			ScreenDescription 
	FROM	tblScreenType 
	WHERE	IsActive	=	1
END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetAll_Slots]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[USP_GetAll_Slots] 
	@Date nvarchar(100) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT	Id				,
			Date			,
			Time			,
			Slots			,
			Count				
	FROM	tblCurrencyRate
	WHERE	Date  = @Date

END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllImageData]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satender.Siwach>
-- Create date: <06 January, 2019>
-- Description:	<to get image list stored in tblImage table>
-- Logs
-- 06 January, 2019 SS Initial Development
-- =============================================
create PROCEDURE [dbo].[USP_GetAllImageData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT 
	   [Id]
      ,[OrignalName]
      ,[FileName]
      ,[IsActive]
  FROM [tblImage]
END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllScreensData]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAllScreensData] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT 
	TS.Id				,
	TS.ScreenName		,
	TST.ScreenType		,
	TS.FeatureColour	,
	TS.Title			,
	TS.SubTitle			,
	TS.CurrencySymbol    as CurrencyCode		,
	(SELECT CurrencySymbol FROM tblCurrency WHERE Id = TS.CurrencySymbol) as CurrencySymbol		,
	TS.ScreenOrder		,
	TS.BackgroundImage ,
	(SELECT FileName From tblImage WHERE Id = TS.BackgroundImage) as BackgroundImageUrl	,
	TS.HtmlCode			,
	TS.CurrencyFirst	,
	TS.CurrencyTwo		,
	TS.CurrencyThree	,
	TS.CurrencyFour		,
	TS.CurrencyFive		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyFirst)	AS CurrencyFirstSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyTwo)	AS CurrencyTwoSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyThree)	AS CurrencyThreeSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyFour)	AS CurrencyFourSymbol		,
	(SELECT CurrencyCode FROM tblCurrency WHERE Id = TS.CurrencyFive)	AS CurrencyFiveSymbol		,
	TS.[Image]          ,
	(SELECT FileName From tblImage WHERE Id = TS.[Image]) as ImageUrl			,
	TS.ScreenFlag		,
	TS.IsActive			,
	Case When Ts.BackgroundImage is Null Then
	TI.FileName 
	When Ts.Image is null then 
	Ti.FileName
	end
	as ImageName,
	Case When Ts.BackgroundImage is Null Then
	TI.OrignalName 
	When Ts.Image is null then 
	TI.OrignalName
	end
	 as OriginalImageName

	FROM	tblScreens		TS	LEFT JOIN 
			tblScreenType	TST		ON	TST.Id	=	TS.ScreenTypeID	LEFT JOIN 
			tblCurrency		TC		ON	TC.ID	=	TS.CurrencySymbol Left Join 
			tblImage		TI		ON	TI.Id	=	TS.BackgroundImage OR TI.Id	= TS.Image
	WHERE TS.IsActive = 1
END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetCurrency_ById]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*************************************************************************************************************
=============================================
Author:  Satender.Siwach	
Create date:    12 January, 2019
Description:	This proc is developed for get currency data by id
				

Change Log:
12 January, 2019 SS Initial Development
 
=============================================
**************************************************************************************************************/
CREATE PROCEDURE [dbo].[USP_GetCurrency_ById]
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Select [Id]
      ,[CurrencyName]
      ,[CurrencyDescription]
      ,[CurrencyCode]
      ,[CurrencySymbol]
	  ,[ImageId]
	  ,(Select FileName From tblImage WHERE Id = ImageId) as ImageUrl
	from [tblCurrency]
	Where Id = @id
END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetImage_ById]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satender.Siwach>
-- Create date: <06 January, 2019>
-- Description:	<to get image information by id>
-- Logs
-- 06 January, 2019 SS Initial Development
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetImage_ById]
	@id bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT 
	   [Id]
      ,[OrignalName]
      ,[FileName]
      ,[IsActive]
  FROM [tblImage]
	Where Id = @id
END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetScreen_ById]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetScreen_ById]
	@id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Select 
	TS.Id				,
	TS.ScreenName		,
	TS.ScreenTypeID  as ScreenType		,
	TS.FeatureColour	,
	TS.Title			,
	TS.SubTitle			,
	TS.CurrencySymbol  as CurrencyCode,
	TS.ScreenOrder		,
	TS.BackgroundImage	,
	(SELECT FileName From tblImage WHERE Id = TS.BackgroundImage) as BackgroundImageUrl			,
	TS.HtmlCode			,
	TS.CurrencyFirst	,
	TS.CurrencyTwo		,
	TS.CurrencyThree	,
	TS.CurrencyFour		,
	TS.CurrencyFive		,
	TS.[Image]			,
	(SELECT FileName From tblImage WHERE Id = TS.[Image]) as ImageUrl			,
	TS.ScreenFlag		,
	TS.IsActive			,
	Case When Ts.BackgroundImage is Null Then
	TI.FileName 
	When Ts.Image is null then 
	Ti.FileName
	end
	as ImageName,
	Case When Ts.BackgroundImage is Null Then
	TI.OrignalName 
	When Ts.Image is null then 
	TI.OrignalName
	end
	 as OriginalImageName

	FROM	tblScreens		TS	LEFT JOIN 
			tblScreenType	TST		ON	TST.Id	=	TS.ScreenTypeID	LEFT JOIN 
			tblImage		TI		ON	TI.Id	=	TS.BackgroundImage OR TI.Id	= TS.Image
	Where TS.Id = @id
END

GO
/****** Object:  StoredProcedure [dbo].[USP_Image_Upsert]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satender.Siwach>
-- Create date: <06 January, 2019>
-- Description:	<to insert or update image information>
-- Logs
-- 06 January, 2019 SS Initial Development
-- =============================================
CREATE PROCEDURE [dbo].[USP_Image_Upsert]
	@id bigint = 0 OUT,
    @OrignalName	NVARCHAR(500) = NULL,
	@FileName		NVARCHAR(500) = NULL,
	@Status 		bit = NULL,
	@UserId int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if(@id>0)
	begin
	update	tblImage
	set OrignalName=@OrignalName,
	FileName=@FileName,
	IsActive=@Status
	where Id=@id
	end
	else
	begin
	
	insert into tblImage([OrignalName],[FileName],[IsActive])
	values(@OrignalName,@FileName,1) 	
	set @Id=SCOPE_IDENTITY()

	end
	select * from tblImage where Id=@id
END

GO
/****** Object:  UserDefinedFunction [dbo].[UrlDecode]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[UrlDecode](@url varchar(3072))
RETURNS varchar(3072)
AS
BEGIN 
    DECLARE @count int, @c char(1), @cenc char(2), @i int, @urlReturn varchar(3072) 
    SET @count = Len(@url) 
    SET @i = 1 
    SET @urlReturn = '' 
    WHILE (@i <= @count) 
     BEGIN 
        SET @c = substring(@url, @i, 1) 
        IF @c LIKE '[!%]' ESCAPE '!' 
         BEGIN 
            SET @cenc = substring(@url, @i + 1, 2) 
            SET @c = CHAR(CASE WHEN SUBSTRING(@cenc, 1, 1) LIKE '[0-9]' 
                                THEN CAST(SUBSTRING(@cenc, 1, 1) as int) 
                                ELSE CAST(ASCII(UPPER(SUBSTRING(@cenc, 1, 1)))-55 as int) 
                            END * 16 + 
                            CASE WHEN SUBSTRING(@cenc, 2, 1) LIKE '[0-9]' 
                                THEN CAST(SUBSTRING(@cenc, 2, 1) as int) 
                                ELSE CAST(ASCII(UPPER(SUBSTRING(@cenc, 2, 1)))-55 as int) 
                            END) 
            SET @urlReturn = @urlReturn + @c 
            SET @i = @i + 2 
         END 
        ELSE 
         BEGIN 
            SET @urlReturn = @urlReturn + @c 
         END 
        SET @i = @i +1 
     END 
    RETURN @urlReturn
END

GO
/****** Object:  Table [dbo].[AccessRight]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccessRight](
	[AccessRightsId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[EntityType] [int] NULL,
	[KorrectAccessRight] [int] NULL,
	[SecurityPrincipalType] [int] NULL,
	[AllowedRights] [int] NULL,
	[DeniedRights] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_AccessRight] PRIMARY KEY CLUSTERED 
(
	[AccessRightsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Locales]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Locales](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](max) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[Culture] [nvarchar](5) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
 CONSTRAINT [PK_Localization] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblCurrency]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCurrency](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrencyName] [nvarchar](500) NULL,
	[CurrencyDescription] [nvarchar](500) NULL,
	[CurrencyCode] [nvarchar](500) NULL,
	[CurrencySymbol] [nvarchar](50) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
	[ImageId] [bigint] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblCurrencyRate]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCurrencyRate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [nvarchar](50) NULL,
	[Time] [nvarchar](50) NULL,
	[Slots] [int] NULL,
	[Count] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblImage]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblImage](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrignalName] [varchar](max) NULL,
	[FileName] [varchar](max) NULL,
	[IsActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblScreens]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblScreens](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ScreenName] [nvarchar](500) NULL,
	[ScreenTypeId] [int] NULL,
	[FeatureColour] [nvarchar](100) NULL,
	[Title] [nvarchar](500) NULL,
	[SubTitle] [nvarchar](500) NULL,
	[CurrencySymbol] [int] NULL,
	[BackgroundImage] [nvarchar](max) NULL,
	[ScreenOrder] [int] NULL,
	[HtmlCode] [nvarchar](max) NULL,
	[CurrencyFirst] [int] NULL,
	[CurrencyTwo] [int] NULL,
	[CurrencyThree] [int] NULL,
	[CurrencyFour] [int] NULL,
	[CurrencyFive] [int] NULL,
	[Image] [nvarchar](max) NULL,
	[ScreenFlag] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_tblScreens] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblScreenType]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblScreenType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ScreenType] [nvarchar](500) NULL,
	[ScreenDescription] [nvarchar](500) NULL,
	[IsActive] [bit] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](150) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[Status] [int] NULL,
	[RoleId] [bigint] NULL,
	[ContactNumber] [varchar](max) NULL,
 CONSTRAINT [PK_UserID] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserAuthToken]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserAuthToken](
	[UserAuthTokenID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[TokenKey] [uniqueidentifier] NOT NULL,
	[LoginDate] [datetime] NULL,
	[ExpiryDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
 CONSTRAINT [PK_UserAuthTokenID] PRIMARY KEY CLUSTERED 
(
	[UserAuthTokenID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 01/26/2019 6:30:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[UserRoleId] [bigint] IDENTITY(1,1) NOT NULL,
	[Role] [nvarchar](500) NULL,
	[Status] [int] NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_ApplicationRole] PRIMARY KEY CLUSTERED 
(
	[UserRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[AccessRight] ON 

INSERT [dbo].[AccessRight] ([AccessRightsId], [UserId], [EntityType], [KorrectAccessRight], [SecurityPrincipalType], [AllowedRights], [DeniedRights], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [status]) VALUES (1, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[AccessRight] OFF
SET IDENTITY_INSERT [dbo].[tblCurrency] ON 

INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (1, N'Leke', NULL, N'ALL', N'Lek', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (2, N'Dollars', NULL, N'USD', N'$', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (3, N'Afghanis', NULL, N'AFN', N'?', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (4, N'Pesos', NULL, N'ARS', N'$', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (5, N'Guilders', NULL, N'AWG', N'ƒ', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (6, N'Dollars', NULL, N'AUD', N'$', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (7, N'New Manats', NULL, N'AZN', N'???', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (8, N'Dollars', NULL, N'BSD', N'$', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (9, N'Dollars', NULL, N'BBD', N'$', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (10, N'Rubles', N'test1', N'BYR', N'p.', NULL, NULL, 0, CAST(0x0000A9DA018A9916 AS DateTime), 1, 0)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (11, N'Euro', NULL, N'EUR', N'€', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (12, N'Dollars', NULL, N'BZD', N'BZ$', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (13, N'Dollars', NULL, N'BMD', N'$', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (14, N'Bolivianos', NULL, N'BOB', N'$b', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (15, N'Convertible Marka', NULL, N'BAM', N'KM', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (16, N'Pula', NULL, N'BWP', N'P', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (17, N'Leva', NULL, N'BGN', N'??', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (18, N'Reais', NULL, N'BRL', N'R$', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (19, N'Pounds', NULL, N'GBP', N'£', NULL, NULL, 0, CAST(0x0000A9DB0008814D AS DateTime), 1, 20029)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (20, N'Dollars', NULL, N'BND', N'$', NULL, NULL, 0, CAST(0x0000A9DB000B05CB AS DateTime), 1, 20033)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (21, N'Riels', NULL, N'KHR', N'?', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (22, N'Dollars', N'test1234', N'CAD', N'$', NULL, NULL, 0, CAST(0x0000A9DB000C4CE1 AS DateTime), 1, 20036)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (23, N'Dollars', NULL, N'KYD', N'$', NULL, NULL, 0, CAST(0x0000A9DB000CE503 AS DateTime), 1, 20038)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (25, N'Yuan Renminbi', N'test123', N'CNY', N'¥', NULL, NULL, 0, CAST(0x0000A9DB000F44C6 AS DateTime), 1, 20040)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (26, N'Pesos', N'test123', N'COP', N'$', NULL, NULL, 0, CAST(0x0000A9DB0010E1FA AS DateTime), 1, 20042)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (27, N'Colón', N'test123', N'CRC', N'¢', NULL, NULL, 0, CAST(0x0000A9DB00117C2F AS DateTime), 1, 20046)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (28, N'Kuna', N'test12', N'HRK', N'kn', NULL, NULL, 0, CAST(0x0000A9DB0011D7A9 AS DateTime), 1, 20048)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (29, N'Pesos', NULL, N'CUP', N'?', NULL, NULL, 0, CAST(0x0000A9DB0014D294 AS DateTime), 1, 20050)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (30, N'Koruny', NULL, N'CZK', N'Kc', NULL, NULL, 0, CAST(0x0000A9DB001548E9 AS DateTime), 1, 20051)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (31, N'Kroner', N'test123', N'DKK', N'kr', NULL, NULL, 0, CAST(0x0000A9DB0018BB8B AS DateTime), 1, 20052)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (32, N'Pesos', N'test34', N'DOP ', N'RD$', NULL, NULL, 0, CAST(0x0000A9DB0019035E AS DateTime), 1, 0)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (33, N'Dollars', N'jkhj', N'XCD', N'$', NULL, NULL, 0, CAST(0x0000A9DB00195B96 AS DateTime), 1, 0)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (34, N'Pounds', N'test45', N'EGP', N'£', NULL, NULL, 0, CAST(0x0000A9DB001A4A17 AS DateTime), 1, 0)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (35, N'Colones', NULL, N'SVC', N'$', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (36, N'Pounds', NULL, N'FKP', N'£', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (37, N'Dollars', NULL, N'FJD', N'$', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (38, N'Cedis', NULL, N'GHC', N'¢', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (39, N'Pounds', NULL, N'GIP', N'£', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (40, N'Quetzales', NULL, N'GTQ', N'Q', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (41, N'Pounds', NULL, N'GGP', N'£', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (42, N'Dollars', NULL, N'GYD', N'$', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (43, N'Lempiras', NULL, N'HNL', N'L', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (44, N'Dollars', NULL, N'HKD', N'$', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (45, N'Forint', NULL, N'HUF', N'Ft', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (46, N'Kronur', NULL, N'ISK', N'kr', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (47, N'Rupees', NULL, N'INR', N'Rp', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (48, N'Rupiahs', NULL, N'IDR', N'Rp', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (49, N'Rials', NULL, N'IRR', N'?', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (50, N'Pounds', NULL, N'IMP', N'£', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (51, N'New Shekels', NULL, N'ILS', N'?', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (52, N'Dollars', NULL, N'JMD', N'J$', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (53, N'Yen', NULL, N'JPY', N'¥', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (54, N'Pounds', NULL, N'JEP', N'£', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (55, N'Tenge', NULL, N'KZT', N'??', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (56, N'Won', NULL, N'KPW', N'?', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (57, N'Won', NULL, N'KRW', N'?', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (58, N'Soms', NULL, N'KGS', N'??', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (59, N'Kips', NULL, N'LAK', N'?', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (60, N'Lati', NULL, N'LVL', N'Ls', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (61, N'Pounds', NULL, N'LBP', N'£', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (62, N'Dollars', NULL, N'LRD', N'$', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (63, N'Switzerland Francs', NULL, N'CHF', N'CHF', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (64, N'Litai', NULL, N'LTL', N'Lt', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (65, N'Denars', NULL, N'MKD', N'???', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (66, N'Ringgits', NULL, N'MYR', N'RM', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (67, N'Rupees', NULL, N'MUR', N'?', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (68, N'Pesos', NULL, N'MXN', N'$', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (69, N'Tugriks', NULL, N'MNT', N'?', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (70, N'Meticais', NULL, N'MZN', N'MT', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (71, N'Dollars', NULL, N'NAD', N'$', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (72, N'Rupees', NULL, N'NPR', N'?', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (73, N'Guilders', NULL, N'ANG', N'ƒ', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (74, N'Dollars', NULL, N'NZD', N'$', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (75, N'Cordobas', NULL, N'NIO', N'C$', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (76, N'Nairas', NULL, N'NGN', N'?', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (77, N'Krone', NULL, N'NOK', N'kr', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (78, N'Rials', NULL, N'OMR', N'?', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (79, N'Rupees', NULL, N'PKR', N'?', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (80, N'Balboa', NULL, N'PAB', N'B/.', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (81, N'Guarani', NULL, N'PYG', N'Gs', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (82, N'Nuevos Soles', NULL, N'PEN', N'S/.', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (83, N'Pesos', NULL, N'PHP', N'Php', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (84, N'Zlotych', NULL, N'PLN', N'zl', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (85, N'Rials', NULL, N'QAR', N'?', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (86, N'New Lei', NULL, N'RON', N'lei', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (87, N'Rubles', NULL, N'RUB', N'???', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (88, N'Pounds', NULL, N'SHP', N'£', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (89, N'Riyals', NULL, N'SAR', N'?', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (90, N'Dinars', NULL, N'RSD', N'???.', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (91, N'Rupees', NULL, N'SCR', N'?', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (92, N'Dollars', NULL, N'SGD', N'$', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (93, N'Dollars', NULL, N'SBD', N'$', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (113, N'ASDF', N'test', N'asd', N'sac', CAST(0x0000A9DA0019225F AS DateTime), 0, 0, CAST(0x0000A9DA018587EE AS DateTime), 1, 0)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (95, N'Rand', NULL, N'ZAR', N'R', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (96, N'Rupees', NULL, N'LKR', N'?', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (97, N'Kronor', NULL, N'SEK', N'kr', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (98, N'Dollars', NULL, N'SRD', N'$', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (99, N'Pounds', NULL, N'SYP', N'£', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (100, N'New Dollars', NULL, N'TWD', N'NT$', NULL, NULL, NULL, NULL, 1, NULL)
GO
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (101, N'Baht', NULL, N'THB', N'?', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (102, N'Dollars', NULL, N'TTD', N'TT$', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (103, N'Lira', NULL, N'TRY', N'TL', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (104, N'Liras', NULL, N'TRL', N'£', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (105, N'Dollars', NULL, N'TVD', N'$', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (106, N'Hryvnia', NULL, N'UAH', N'?', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (107, N'Pesos', NULL, N'UYU', N'$U', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (108, N'Sums', NULL, N'UZS', N'??', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (109, N'Bolivares Fuertes', NULL, N'VEF', N'Bs', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (110, N'Dong', NULL, N'VND', N'?', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (111, N'Rials', NULL, N'YER', N'?', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (112, N'Zimbabwe Dollars', NULL, N'ZWD', N'Z$', NULL, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (114, N'ASDF', N'test1345', N'asd', N'sac', CAST(0x0000A9DA0019250E AS DateTime), 0, 0, CAST(0x0000A9DB00066DE6 AS DateTime), 1, 20026)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (115, N'ASDF', NULL, N'asd', N'sac', CAST(0x0000A9DA0019A8DB AS DateTime), 0, 0, CAST(0x0000A9DA0019A8DB AS DateTime), 1, 10037)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (122, N'ASDF', NULL, N'kjl', N'hjjk', CAST(0x0000A9DA00210F5D AS DateTime), 0, 0, CAST(0x0000A9DA00210F5D AS DateTime), 1, 0)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (1113, N'test', N'test', N'test', N'test', CAST(0x0000A9DA01810CAD AS DateTime), 0, 0, CAST(0x0000A9DA01810CAD AS DateTime), 1, 0)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (1114, N'test', N'test', N'test', N'test', CAST(0x0000A9DA018114F5 AS DateTime), 0, 0, CAST(0x0000A9DA018114F5 AS DateTime), 1, 0)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (1115, N'test1', N'test2', N'test2', N'test', CAST(0x0000A9DA01816B8B AS DateTime), 0, 0, CAST(0x0000A9DA01816B8B AS DateTime), 1, 0)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (1116, N'test1', N'test2', N'test2', N'test', CAST(0x0000A9DA018171C8 AS DateTime), 0, 0, CAST(0x0000A9DA018171C8 AS DateTime), 1, 0)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (116, NULL, NULL, NULL, NULL, CAST(0x0000A9DA001F0F53 AS DateTime), 0, 0, CAST(0x0000A9DA001F0F53 AS DateTime), 1, 0)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (117, N'ASDF', NULL, N'kjl', N'hjjk', CAST(0x0000A9DA001F3D68 AS DateTime), 0, 0, CAST(0x0000A9DA001F3D68 AS DateTime), 1, 0)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (118, N'ASDF', NULL, N'kjl', N'hjjk', CAST(0x0000A9DA00207D89 AS DateTime), 0, 0, CAST(0x0000A9DA00207D89 AS DateTime), 1, 0)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (119, N'ASDF', NULL, N'kjl', N'hjjk', CAST(0x0000A9DA002084FE AS DateTime), 0, 0, CAST(0x0000A9DA002084FE AS DateTime), 1, 0)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (120, N'ASDF', NULL, N'kjl', N'hjjk', CAST(0x0000A9DA0020C1EA AS DateTime), 0, 0, CAST(0x0000A9DA0020C1EA AS DateTime), 1, 0)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (121, N'ASDF', NULL, N'kjl', N'hjjk', CAST(0x0000A9DA0020C971 AS DateTime), 0, 0, CAST(0x0000A9DA0020C971 AS DateTime), 1, 0)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (123, N'ASDF', NULL, N'kjl', N'hjjk', CAST(0x0000A9DA00217F66 AS DateTime), 0, 0, CAST(0x0000A9DA00217F66 AS DateTime), 1, 0)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (124, N'ASDF', NULL, N'kjl', N'hjjk', CAST(0x0000A9DA0021AB2A AS DateTime), 0, 0, CAST(0x0000A9DA0021AB2A AS DateTime), 1, 0)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (125, N'ASDF', NULL, N'kjl', N'hjjk', CAST(0x0000A9DA0021B3F1 AS DateTime), 0, 0, CAST(0x0000A9DA0021B3F1 AS DateTime), 1, 0)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (126, N'ASDF', NULL, N'kjl', N'hjjk', CAST(0x0000A9DA0021B73E AS DateTime), 0, 0, CAST(0x0000A9DA0021B73E AS DateTime), 1, 0)
INSERT [dbo].[tblCurrency] ([Id], [CurrencyName], [CurrencyDescription], [CurrencyCode], [CurrencySymbol], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn], [IsActive], [ImageId]) VALUES (1117, N'test12', N'test2', N'test2', N'test', CAST(0x0000A9DA01819E57 AS DateTime), 0, 0, CAST(0x0000A9DB00008788 AS DateTime), 1, 0)
SET IDENTITY_INSERT [dbo].[tblCurrency] OFF
SET IDENTITY_INSERT [dbo].[tblCurrencyRate] ON 

INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (2, N'25/1/2019', N'00:23:04.1054477', 0, 4)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (1002, N'26/1/2019', N'13:38:07.6013733', 13, 1)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (2002, N'26/1/2019', N'14:35:24.8789113', 14, 3)
INSERT [dbo].[tblCurrencyRate] ([Id], [Date], [Time], [Slots], [Count]) VALUES (3002, N'26/1/2019', N'15:04:24.8208848', 15, 14)
SET IDENTITY_INSERT [dbo].[tblCurrencyRate] OFF
SET IDENTITY_INSERT [dbo].[tblImage] ON 

INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (1, N'0014825.png', N'5803db5f-c398-4fc4-8a30-00aae8db77e8.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (2, N'0014825.png', N'0cc9128c-1804-4463-844a-f36516723382.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (3, N'0014825.png', N'f49c9b52-5fb9-4c81-b7e5-2de793cea500.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (4, N'0014825.png', N'd6158e72-4814-46c4-a755-9e2ae4ba7265.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (5, N'0014825.png', N'a7e456e3-78d9-4c00-9a82-dcbbfe48c97a.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (6, N'0014825.png', N'd02978a2-768d-43c3-9e6c-e28dde641f47.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (7, N'0014825.png', N'7f8589c8-55da-405f-b75a-3ddfa7bdefc6.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (8, N'0014825.png', N'ccba2d25-6db4-4a55-a980-f03c36869da7.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (9, N'0014825.png', N'09c0b6f7-92b3-44fb-8b8e-05e59430bca4.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (10, N'0014825.png', N'1157ed09-e711-4c13-8737-84834260571b.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (11, N'api info.txt', N'8b22456f-9918-4a85-9c32-49aa889fdd91.txt', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (12, N'0014825.png', N'6c58773d-a90e-43f4-995e-6b72b532ff4d.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (13, N'appatore.png', N'f904c3aa-7b92-43a9-adbb-15cb0b9c362d.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (14, N'appatore.png', N'77ff1dcb-377a-4f51-86fc-16978ee1d682.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (15, N'default-image_64.png', N'ab83e7b6-a3a3-4c53-bfa1-2d6e911ca6df.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (16, N'default-image_64.png', N'b5374762-9b59-431f-af6f-464d2f47d30e.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (17, N'default-image_64.png', N'c8a2b871-5832-4b28-bfe5-dcff30fd6149.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (18, N'default-image_64.png', N'3cf3db8e-5154-42e6-aa76-b60498e215ab.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (19, N'default-image_64.png', N'0346bf97-8e8b-4d97-a2cc-0a9b46d81925.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20, N'default-image_64.png', N'94c44981-3738-4fb7-9a8d-078cb9a2d71f.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (21, N'appatore.png', N'8713c1c8-e3b0-4cd8-a528-59551899c395.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (22, N'umbraco-logo-png-2.png', N'03d03c3d-6a34-4cbe-85d3-0d70a1888f53.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (23, N'zyz-theme1.jpg', N'2a38164e-adbf-4cf6-8d5e-2db2dc94aa0b.jpg', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (24, N'Passport Appointment.png', N'24d1252f-ebb3-4846-b441-d73363cae05f.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (25, N'Passport Appointment.png', N'9d3c7674-749e-4d1e-a1d1-45533778cc0d.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (10025, N'Passport Appointment.png', N'b963a378-265f-4f32-bf69-90c7d4af9308.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (10026, N'Passport Appointment.png', N'49316121-f7f9-4a47-923a-8b9e52eb2e73.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (10027, N'Passport Appointment.png', N'b207c4ab-7bc0-4479-afb7-ffa3b23db826.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (10028, N'Passport Appointment.png', N'7cfcde7c-52be-4678-963b-82a48541dd85.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (10029, N'Passport Appointment.png', N'511f2a84-94f9-4456-8c5b-f226ac1ee0c8.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (10030, N'Passport Appointment.png', N'651a5f0e-3e70-4aa6-a8f3-dbe25cd1dc25.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (10031, N'Passport Appointment.png', N'0fa4cb38-4833-4621-b184-1860e0946f3e.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (10032, N'Passport Appointment.png', N'6b41f667-9836-4da1-b532-9a3bc66a7cc3.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (10033, N'Passport Appointment.png', N'bd82638e-e5f9-4fc7-b4a0-002cef223ea4.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (10034, N'Passport Appointment.png', N'2815d2d5-7e72-4388-8fc4-12f52a8de100.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (10035, N'Passport Appointment.png', N'4ada889e-ae2c-48c2-b5ca-b6114b6339d3.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (10036, N'Passport Appointment.png', N'7f99e45b-b03d-4d12-a572-3919e0718c93.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (10037, N'Passport Appointment.png', N'571a69df-a316-4ae3-800b-915122cac6f1.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20025, N'Passport Appointment.png', N'0ddd5419-03bb-4ef7-af45-df6f28e0a506.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20026, N'Passport Appointment.png', N'a33ac9b1-967a-45a4-a46d-771f7206be57.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20027, N'Passport Appointment.png', N'a22b89c4-7ad1-40ba-b4ac-18abd7d86d04.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20028, N'Passport Appointment.png', N'c7bab79d-1017-41fd-bf19-ae8da0ecf7c2.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20029, N'Passport Appointment.png', N'9ae00ff5-dd42-4753-98be-69d2ce533224.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20030, N'Passport Appointment.png', N'62eec75e-3d8d-4f04-8426-483ad597eb20.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20031, N'Passport Appointment.png', N'9ccc72cd-160e-4041-a8b6-0f66641e87c9.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20032, N'Passport Appointment.png', N'b975bcc5-54ef-4676-886b-1dd9e3f521f9.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20033, N'Passport Appointment.png', N'71267051-bcac-45ab-97f7-d81f47509c67.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20034, N'Passport Appointment.png', N'5c7ce86d-c180-4083-b502-5a72b30565a4.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20035, N'Passport Appointment.png', N'2b8f0d9b-72ab-4693-8872-fb0b87efc080.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20036, N'Passport Appointment.png', N'518d03b6-db54-46be-ab61-5e6e5f5e73c0.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20037, N'Passport Appointment.png', N'38152f51-9275-4924-b254-2edb9f34ceee.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20038, N'Passport Appointment.png', N'0e501eba-d68b-4aee-b4fc-bb370eedc2ae.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20039, N'Passport Appointment.png', N'1423d94b-4877-401b-8547-91e0a4dbea1e.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20040, N'Passport Appointment.png', N'ec158370-8316-46a8-b7d8-357ebc05b625.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20041, N'Passport Appointment.png', N'99f09247-54ed-4555-8455-d822591ad303.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20042, N'Passport Appointment.png', N'5c1c9273-7e70-4f28-a952-9d7dbee9a9b5.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20043, N'Passport Appointment.png', N'683d3fff-031c-4ea5-8999-f550def0397c.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20044, N'Passport Appointment.png', N'8def10b4-2257-49a2-9096-13f569183a81.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20045, N'Passport Appointment.png', N'e51c1959-1803-4d87-a08a-155ba1f0f518.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20046, N'Passport Appointment.png', N'04f1d298-369f-4ff9-9bce-436c592395e8.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20047, N'Passport Appointment.png', N'971b8faa-8ab1-4b54-86e6-18e7fd39bd5e.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20048, N'Passport Appointment.png', N'80b909f3-8651-46ae-a42c-26aee18a31d2.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20049, N'Passport Appointment.png', N'4c24937c-8934-43fc-aee1-051f19dee525.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20050, N'Passport Appointment.png', N'804fc882-3b67-448f-9308-918f30f298ca.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20051, N'Passport Appointment.png', N'71f71247-04c5-4462-b660-51d22a4cb350.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20052, N'Passport Appointment.png', N'f7cc3af6-7194-4eb6-9ec3-9950f232345b.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20053, N'Passport Appointment.png', N'51e63fc3-49cf-41c4-8460-07c2d65f38b5.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20054, N'Passport Appointment.png', N'00678615-fb69-4e64-a502-14f4482422f0.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20055, N'Passport Appointment.png', N'44fb3796-2d48-4e8a-b52c-948234e9854d.png', 1)
INSERT [dbo].[tblImage] ([Id], [OrignalName], [FileName], [IsActive]) VALUES (20056, N'Passport Appointment.png', N'f7000d39-40ab-4360-a3b2-a1590a6dbd02.png', 1)
SET IDENTITY_INSERT [dbo].[tblImage] OFF
SET IDENTITY_INSERT [dbo].[tblScreens] ON 

INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (3, N'Test1', 1, N'Test3', N'Test4', N'Test 5', 36, NULL, 8, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9B80020FFEE AS DateTime), 0, CAST(0x0000A9DB0033F0D8 AS DateTime), 0, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4, N'Screen 1', 1, N'Black', N'Test Screen', N'Sub Test Screen', 1, N'20056', 1, NULL, 0, 0, 0, 0, 0, N'20056', 0, CAST(0x0000A9CB00256A14 AS DateTime), 0, CAST(0x0000A9DB00345AE6 AS DateTime), 0, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (5, N'Screen 1', 1, N'Black', N'Test Screen', N'Sub Test Screen', 1, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9CB00284EB6 AS DateTime), 0, CAST(0x0000A9CB00284EB6 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (6, N'Screen 1', 1, N'Black', N'Test Screen', N'Sub Test Screen', 1, N'20054', 1, NULL, 0, 0, 0, 0, 0, N'20054', 0, CAST(0x0000A9CB00287D16 AS DateTime), 0, CAST(0x0000A9DB002907A2 AS DateTime), 0, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (7, N'test', 4, NULL, NULL, NULL, 0, NULL, 14, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9CF0126A61D AS DateTime), 0, CAST(0x0000A9DB00294D48 AS DateTime), 0, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (8, N'test', 4, NULL, NULL, NULL, NULL, N'9', 231, NULL, 0, 0, 0, 0, 0, N'9', 0, CAST(0x0000A9CF012912E2 AS DateTime), 0, CAST(0x0000A9CF012912E2 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (9, N'testtese', 4, NULL, NULL, NULL, NULL, N'10', 12, NULL, 0, 0, 0, 0, 0, N'10', 0, CAST(0x0000A9CF012AD2DF AS DateTime), 0, CAST(0x0000A9CF012AD2DF AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (10, N'test', 4, NULL, NULL, NULL, NULL, N'11', 123, NULL, 0, 0, 0, 0, 0, N'11', 0, CAST(0x0000A9CF012B15F7 AS DateTime), 0, CAST(0x0000A9CF012B15F7 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (11, N'test', 4, NULL, NULL, NULL, 0, NULL, 123, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9CF012B7986 AS DateTime), 0, CAST(0x0000A9DB0029BCC9 AS DateTime), 0, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (12, N'Test1', 1, N'Test3', N'Test4', N'Test 5', 36, N'20053', 8, NULL, 0, 0, 0, 0, 0, N'20053', 0, CAST(0x0000A9D000006716 AS DateTime), 0, CAST(0x0000A9DB0022E675 AS DateTime), 0, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (13, N'Test1', 1, N'Test3', N'Test4', N'Test 5', 36, NULL, 8, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D00002FACA AS DateTime), 0, CAST(0x0000A9D00002FACA AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (14, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D5017BF443 AS DateTime), 0, CAST(0x0000A9D5017BF443 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (1014, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D501814F11 AS DateTime), 0, CAST(0x0000A9D501814F11 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (1015, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D501815C3F AS DateTime), 0, CAST(0x0000A9D501815C3F AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (1016, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D50181A989 AS DateTime), 0, CAST(0x0000A9D50181A989 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (1017, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D501838B61 AS DateTime), 0, CAST(0x0000A9D501838B61 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (1018, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D5018818AE AS DateTime), 0, CAST(0x0000A9D5018818AE AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (1019, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D501895CE9 AS DateTime), 0, CAST(0x0000A9D501895CE9 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (1020, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D5018B71B5 AS DateTime), 0, CAST(0x0000A9D5018B71B5 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (1021, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D6000036B3 AS DateTime), 0, CAST(0x0000A9D6000036B3 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (1022, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D600005FA3 AS DateTime), 0, CAST(0x0000A9D600005FA3 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (1023, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D60001BA8F AS DateTime), 0, CAST(0x0000A9D60001BA8F AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (1024, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D600025072 AS DateTime), 0, CAST(0x0000A9D600025072 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (1025, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D60002C108 AS DateTime), 0, CAST(0x0000A9D60002C108 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (1026, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D60004172E AS DateTime), 0, CAST(0x0000A9D60004172E AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (1027, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D6000530BD AS DateTime), 0, CAST(0x0000A9D6000530BD AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (1028, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D6000738AE AS DateTime), 0, CAST(0x0000A9D6000738AE AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (1029, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D60007A66F AS DateTime), 0, CAST(0x0000A9D60007A66F AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (1030, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D60007EE86 AS DateTime), 0, CAST(0x0000A9D60007EE86 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (1031, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D600081772 AS DateTime), 0, CAST(0x0000A9D600081772 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (1032, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D6000856C5 AS DateTime), 0, CAST(0x0000A9D6000856C5 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (1033, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D6000A3F6E AS DateTime), 0, CAST(0x0000A9D6000A3F6E AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (1034, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D6000B2252 AS DateTime), 0, CAST(0x0000A9D6000B2252 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (1035, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D6000B2BA8 AS DateTime), 0, CAST(0x0000A9D6000B2BA8 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (1036, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D6000C2987 AS DateTime), 0, CAST(0x0000A9D6000C2987 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (1037, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D6000CCAC7 AS DateTime), 0, CAST(0x0000A9D6000CCAC7 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (2014, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D60165EF7B AS DateTime), 0, CAST(0x0000A9D60165EF7B AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (2015, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D60168B745 AS DateTime), 0, CAST(0x0000A9D60168B745 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (2016, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D601694B5A AS DateTime), 0, CAST(0x0000A9D601694B5A AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (2017, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D6016A76F0 AS DateTime), 0, CAST(0x0000A9D6016A76F0 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (2018, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D6016ACA22 AS DateTime), 0, CAST(0x0000A9D6016ACA22 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (2019, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D6016B0C45 AS DateTime), 0, CAST(0x0000A9D6016B0C45 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (2020, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D6016C668C AS DateTime), 0, CAST(0x0000A9D6016C668C AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (2021, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D6016FDF34 AS DateTime), 0, CAST(0x0000A9D6016FDF34 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (2022, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D60170096B AS DateTime), 0, CAST(0x0000A9D60170096B AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (3014, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D601747818 AS DateTime), 0, CAST(0x0000A9D601747818 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4014, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D6017E9839 AS DateTime), 0, CAST(0x0000A9D6017E9839 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4015, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D6017ED511 AS DateTime), 0, CAST(0x0000A9D6017ED511 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4016, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D6017F4AD7 AS DateTime), 0, CAST(0x0000A9D6017F4AD7 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4017, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D6017F6E79 AS DateTime), 0, CAST(0x0000A9D6017F6E79 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4018, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D6018042A2 AS DateTime), 0, CAST(0x0000A9D6018042A2 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4019, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D60180A030 AS DateTime), 0, CAST(0x0000A9D60180A030 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4020, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D6018100D2 AS DateTime), 0, CAST(0x0000A9D6018100D2 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4021, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D6018290CC AS DateTime), 0, CAST(0x0000A9D6018290CC AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4022, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D60182F2FC AS DateTime), 0, CAST(0x0000A9D60182F2FC AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4023, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D601831B4A AS DateTime), 0, CAST(0x0000A9D601831B4A AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4024, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D60183E385 AS DateTime), 0, CAST(0x0000A9D60183E385 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4025, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D601840B33 AS DateTime), 0, CAST(0x0000A9D601840B33 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4026, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D601847BA0 AS DateTime), 0, CAST(0x0000A9D601847BA0 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4027, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D60184DD02 AS DateTime), 0, CAST(0x0000A9D60184DD02 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4028, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D60185A106 AS DateTime), 0, CAST(0x0000A9D60185A106 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4029, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D60185EACD AS DateTime), 0, CAST(0x0000A9D60185EACD AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4030, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D60186E01D AS DateTime), 0, CAST(0x0000A9D60186E01D AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4031, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D60187D30A AS DateTime), 0, CAST(0x0000A9D60187D30A AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4032, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D601888323 AS DateTime), 0, CAST(0x0000A9D601888323 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4033, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D6018ADD5D AS DateTime), 0, CAST(0x0000A9D6018ADD5D AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4034, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D6018B4327 AS DateTime), 0, CAST(0x0000A9D6018B4327 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4035, NULL, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D70001C128 AS DateTime), 0, CAST(0x0000A9D70001C128 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4036, NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D700044F8D AS DateTime), 0, CAST(0x0000A9D700044F8D AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4037, NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D70004B076 AS DateTime), 0, CAST(0x0000A9D70004B076 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4038, NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D70004B61A AS DateTime), 0, CAST(0x0000A9D70004B61A AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4039, NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D70004CC2B AS DateTime), 0, CAST(0x0000A9D70004CC2B AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4040, NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D70005182E AS DateTime), 0, CAST(0x0000A9D70005182E AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4041, NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D70005BF3B AS DateTime), 0, CAST(0x0000A9D70005BF3B AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (4042, NULL, 3, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D70005CAA6 AS DateTime), 0, CAST(0x0000A9D70005CAA6 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (5014, NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D7016D4782 AS DateTime), 0, CAST(0x0000A9D7016D4782 AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (5015, NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D7016D551C AS DateTime), 0, CAST(0x0000A9D7016D551C AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (5016, NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D7016E432F AS DateTime), 0, CAST(0x0000A9D7016E432F AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (5017, NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9D7016E891D AS DateTime), 0, CAST(0x0000A9D7016E891D AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (6014, N'test123', 1, N'123', N'test123test', N'test123test', 50, NULL, 1, NULL, 0, 0, 0, 0, 0, NULL, 0, CAST(0x0000A9DB002C76AB AS DateTime), 0, CAST(0x0000A9DB002C76AB AS DateTime), NULL, 1)
INSERT [dbo].[tblScreens] ([Id], [ScreenName], [ScreenTypeId], [FeatureColour], [Title], [SubTitle], [CurrencySymbol], [BackgroundImage], [ScreenOrder], [HtmlCode], [CurrencyFirst], [CurrencyTwo], [CurrencyThree], [CurrencyFour], [CurrencyFive], [Image], [ScreenFlag], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [IsActive]) VALUES (6015, N'new test34', 4, NULL, NULL, NULL, 0, N'20055', 900, NULL, 0, 0, 0, 0, 0, N'20055', 0, CAST(0x0000A9DB002DB6FA AS DateTime), 0, CAST(0x0000A9DB002DB6FA AS DateTime), NULL, 1)
SET IDENTITY_INSERT [dbo].[tblScreens] OFF
SET IDENTITY_INSERT [dbo].[tblScreenType] ON 

INSERT [dbo].[tblScreenType] ([Id], [ScreenType], [ScreenDescription], [IsActive], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn]) VALUES (1, N'PromoScreen', N'Promo screen', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblScreenType] ([Id], [ScreenType], [ScreenDescription], [IsActive], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn]) VALUES (2, N'HTML', N'html', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblScreenType] ([Id], [ScreenType], [ScreenDescription], [IsActive], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn]) VALUES (3, N'PromoScreen2', N'Promo screen', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblScreenType] ([Id], [ScreenType], [ScreenDescription], [IsActive], [CreatedOn], [CreatedBy], [UpdatedBy], [UpdatedOn]) VALUES (4, N'ImageScreen', N'Image screen', 1, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblScreenType] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([UserID], [FirstName], [LastName], [Email], [Password], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Status], [RoleId], [ContactNumber]) VALUES (1, N'dev', N'dev', N'dev@dev.com', N'QEbdqkVOX0yjRxKbJ4KFYW9MDUhj58JRbhDaF5fcXA171k2GHCvbqAvMKWp0KVCuWwMVK0DCizoHC9mwJ8/5uVhflPdDq+g=', NULL, NULL, CAST(0x0000A958001F5507 AS DateTime), NULL, 1, 1, NULL)
INSERT [dbo].[User] ([UserID], [FirstName], [LastName], [Email], [Password], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [Status], [RoleId], [ContactNumber]) VALUES (2, N'test', N'test', N'gdassociates.info@gmail.com', N'Q2V+YMPcBubwmpKvYwSWXNhtdhw1X2xBswsiIWPDAzu8u56Gw8HnRMUUuX5Hu2ongM1GRzF+w5a+XxKilUEQAMGesI9S', CAST(0x0000A9D00014792A AS DateTime), NULL, NULL, NULL, 1, 2, N'3432421')
SET IDENTITY_INSERT [dbo].[User] OFF
SET IDENTITY_INSERT [dbo].[UserAuthToken] ON 

INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1, 1, N'2116b9b1-065f-4f67-a8c8-33338ecac5c2', CAST(0x0000A9B201871D00 AS DateTime), CAST(0x0000A9B3005EBF54 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (2, 1, N'6d40a898-b80f-402b-b9ff-e8033b515ceb', CAST(0x0000A9B6010E89D0 AS DateTime), CAST(0x0000A9B6017199B9 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (3, 1, N'cf5feea0-3c18-4715-8f06-800afdf073e3', CAST(0x0000A9B6010E89D0 AS DateTime), CAST(0x0000A9B6017199B9 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (4, 1, N'b77a0d99-ff09-4075-ae22-60c4e44d37ea', CAST(0x0000A9B6010E89D0 AS DateTime), CAST(0x0000A9B6017199BF AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (5, 1, N'ad458b6f-c2b1-40ea-9b55-c25d1173d888', CAST(0x0000A9B6010E89D0 AS DateTime), CAST(0x0000A9B6017199E8 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (6, 1, N'6e2bf9d2-eb3a-45dd-8ddd-96aa68150f3a', CAST(0x0000A9B6010E89D0 AS DateTime), CAST(0x0000A9B6017199E8 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1003, 1, N'67346e5e-88ab-47e8-9782-5c5f01123489', CAST(0x0000A9B801601CA0 AS DateTime), CAST(0x0000A9B90037A6CA AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1004, 1, N'c7a4057e-bfc4-4c91-897f-a698364bdf56', CAST(0x0000A9B9017542B0 AS DateTime), CAST(0x0000A9BA004CDEE9 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1005, 1, N'5cf84ac5-c7d2-41ab-81bd-aafabba315c7', CAST(0x0000A9B9017542B0 AS DateTime), CAST(0x0000A9BA004CDEE9 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1006, 1, N'10a1822b-8575-4f69-8656-6afde65aeecd', CAST(0x0000A9CA0176E890 AS DateTime), CAST(0x0000A9CB004E72F0 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1007, 1, N'581b502b-f86e-421d-8826-07ff624ee4a8', CAST(0x0000A9CE0034BC00 AS DateTime), CAST(0x0000A9CE0097B7FE AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1008, 1, N'1f5683a9-c37c-4156-8a81-9c7080582284', CAST(0x0000A9CE01552020 AS DateTime), CAST(0x0000A9CF002C9748 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1009, 1, N'51fffa28-4bb5-45cb-96e5-17d41a128872', CAST(0x0000A9CE01552020 AS DateTime), CAST(0x0000A9CF002C9748 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1010, 1, N'8f7b08ab-337d-4be7-881b-7fcfa7fceb7c', CAST(0x0000A9CF00EA0240 AS DateTime), CAST(0x0000A9CF014D1708 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1011, 1, N'941005e9-ebe1-4a18-a291-8ddd4ad9a28c', CAST(0x0000A9D0000D2F00 AS DateTime), CAST(0x0000A9D000705366 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1012, 1, N'6617834a-0213-463c-928e-8f364e2c840d', CAST(0x0000A9D200E36AC0 AS DateTime), CAST(0x0000A9D201466415 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1013, 1, N'8108579b-695b-4e64-8d6d-e8385a27a5f5', CAST(0x0000A9D201632210 AS DateTime), CAST(0x0000A9D3003AA79B AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1014, 1, N'3b72ac82-4d37-4978-9ed0-309bf18ec64e', CAST(0x0000A9D201632210 AS DateTime), CAST(0x0000A9D3003AA79B AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[UserAuthToken] ([UserAuthTokenID], [UserId], [TokenKey], [LoginDate], [ExpiryDate], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) VALUES (1015, 1, N'e113f51f-3aa7-4ab9-9d37-e881d93dadc5', CAST(0x0000A9D301292E20 AS DateTime), CAST(0x0000A9D400009B20 AS DateTime), NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[UserAuthToken] OFF
SET IDENTITY_INSERT [dbo].[UserRole] ON 

INSERT [dbo].[UserRole] ([UserRoleId], [Role], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'Admin', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[UserRole] ([UserRoleId], [Role], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'Sub Admin', 1, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[UserRole] OFF
USE [master]
GO
ALTER DATABASE [NMoneyCurrencyConversion] SET  READ_WRITE 
GO
