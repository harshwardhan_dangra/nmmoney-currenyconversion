/***Maga Menu***/
$('.nav-toggle').click(function(){
	$('.maga-menu-main').show();
	})
	$('.menu-dropdown-close').click(function(){
	$('.maga-menu-main').hide();
	})
$('.menu-dropdown-nav li').hover(function(){
	$(this).addClass('open-nav')
	},function(){
		$(this).removeClass('open-nav')
		})


$('.brand_slider').owlCarousel({
	     loop: true,
	     margin: 30,
	     nav: false,
	     responsive: {
	         0: {
	             items: 2,
	         },
	         380: {
	             items: 3,
	         },
			 540: {
				 items: 4,
			 },
	         1000: {
	             items: 5,
	         },
	         1199: {
	             items: 6
	         }
	     }
	 });
	 
	 
//$('.cutom_select').select2();

	 
	 
    $(document).ready(function () {
        if ($("#PreferredDay").val() == null) {
            $("#PreferredTime").prop("disabled", true);
        }
        else
            $("#PreferredTime").prop("disabled", false);
    });

    $("#PreferredDay").change(function () {
        $("#PreferredTime").prop("disabled", false);
        if ($(this).data('options') === undefined) {
            /*Taking an array of all options-2 and kind of embedding it on the select1*/
            $(this).data('options', $('#PreferredTime option').clone());

        }
        var id = $(this).val();
        var options = $(this).data('options').filter('[value=' + id + ']');
        $('#PreferredTime').html(options);
    });
