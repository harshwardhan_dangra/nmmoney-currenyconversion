﻿using NCC.Core.Data;

namespace NCC.Common.Data
{
    public interface ICommonDbContext : IDbContext
    {
        IUploadFileRepository UploadFileRepository { get; }
        ICommonBusinessRepository CommonBusinessRepository { get; }
        INotificationRepository NotificationRepository { get; }
        ITranslationRepository TranslationRepository { get; }
    
    }
}
