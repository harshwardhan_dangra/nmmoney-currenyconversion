﻿using NCC.Common.Entities;
using NCC.Core.Data;

namespace NCC.Common.Data
{
    public interface ITranslationRepository : IRepository<Locale>
    {

    }
}
