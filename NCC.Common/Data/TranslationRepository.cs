﻿using NCC.Common.Entities;
using NCC.Core.Data;

namespace NCC.Common.Data
{
    public class TranslationRepository : BaseRepository<Locale>, ITranslationRepository
    {
        private const string GetByIdStoredProcName = "Locales_SelectById";
        private const string GetFilteredStoredProcName = "TableFilteredRow";
        private const string UpSertStoredProcName = "Locales_UpSert";
        private const string DeleteStoredProcName = "Locales_DeleteById";

        protected override string GetByIdStoredProcedureName { get { return GetByIdStoredProcName; } }

        protected override string GetFilteredStoredProcedureName { get { return GetFilteredStoredProcName; } }

        protected override string UpSertStoredProcedureName { get { return UpSertStoredProcName; } }

        protected override string DeleteStoredProcedureName { get { return DeleteStoredProcName; } }

        public TranslationRepository(IDbContext dbContext)
            :base(dbContext)
        { }
    }
}
