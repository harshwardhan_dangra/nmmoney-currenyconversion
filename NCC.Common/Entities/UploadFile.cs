﻿using NCC.Core.Entities;
using System.Runtime.Serialization;

namespace NCC.Common.Entities
{
    [DataContract]
    public class UploadFile : BaseEntity
    {
        public UploadFile()
            : base("FileID") { }

        [DataMember(Name = "fileId")]
        public int FileID { get; set; }

        [DataMember(Name = "fileName")]
        public string FileName { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "filePath")]
        public string FilePath { get; set; }

        [DataMember(Name = "fileSize")]
        public int FileSize { get; set; }

     }
}