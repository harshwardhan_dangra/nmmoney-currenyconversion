﻿using NCC.Common.Data;

namespace NCC.Common.Services
{
    public class CommonService : ICommonService
    {
        public ICommonDbContext CommonDbContext;

        public CommonService(ICommonDbContext commonDbContext)
        {

            this.CommonDbContext = commonDbContext;
        }

    }
}
