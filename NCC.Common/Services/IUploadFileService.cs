﻿using NCC.Common.Entities;

namespace NCC.Common.Services
{
    public interface IUploadFileService
    {
        UploadFile SaveUploadFile(UploadFile uploadFile);
    }
}
