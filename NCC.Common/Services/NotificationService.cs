﻿using NCC.Common.Data;
using NCC.Common.Entities;
using NCC.Core.Services;
using System;
using System.Collections.Generic;
namespace NCC.Common.Services
{
    public class NotificationService : INotificationService
    {
        public ICommonDbContext CommonDbContext;

        public NotificationService(ICommonDbContext commonDbContext)
        {
            this.CommonDbContext = commonDbContext;
        }

        public bool DeleteNotification(Notification notification)
        {

            return CommonDbContext.NotificationRepository.Delete(notification.Id);

        }

        public Notification SaveNotification(Notification notification)
        {

            if (notification == null)
            {
                BusinessException.Instance.ThrowHttpResponseException(new BusinessError()
                {

                    BusinessModule = BusinessModule.Module.Common,
                    ErrorCodeDefinition = ErrorCode.InvalidParameter,
                    ReasonPhrase = "Incorrect parameter passed for method SaveNotification"

                });
            }

            if (notification.DateCreated == DateTime.MinValue)
            {
                notification.DateCreated = System.Data.SqlTypes.SqlDateTime.MinValue.Value;
            }

            if (notification.TimeStamp == DateTime.MinValue)
            {
                notification.TimeStamp = System.Data.SqlTypes.SqlDateTime.MinValue.Value;
            }

            return CommonDbContext.NotificationRepository.Add(notification);

        }

        public Notification GetNotificationById(int id)
        {
            return CommonDbContext.NotificationRepository.GetById(id);

        }

        public List<Notification> GetAllNotifications()
        {
            return CommonDbContext.NotificationRepository.GetAllNotications();

        }

        public List<NotificationType> GetAllNoticationTypes()
        {
            return CommonDbContext.NotificationRepository.GetAllNoticationTypes();
        }
    }
}
