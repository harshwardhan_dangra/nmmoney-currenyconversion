﻿using System.Collections.Generic;
using System.Linq;
using NCC.Common.Data;
using NCC.Common.Entities;
using NCC.Common.BusinessObjects;
using NCC.Core.Services;

namespace NCC.Common.Services
{
    public class TranslationService :ITranslationService
    {
        public ICommonDbContext CommonDbContext;

        public TranslationService(ICommonDbContext commonDbContext)
        {
            this.CommonDbContext = commonDbContext;
        }

        public Locale GetLocale(long id)
        {
            if (id == null || (id != null && id < 0))
            {
                BusinessException.Instance.ThrowHttpResponseException(new BusinessError()
                {
                    BusinessModule = BusinessModule.Module.Security,
                    ErrorCodeDefinition = ErrorCode.InvalidLocaleId,
                    ReasonPhrase = "Invalid locale id"
                });
            }

            return CommonDbContext.TranslationRepository.GetById(id);
        }

        public Dictionary<string,string> GetDictionaryLocale(string culture)
        {
            int? totalCount;
            return  CommonDbContext.TranslationRepository.GetFiltered("locales", string.Format("Culture= '{0}'", culture), null, "[key],[value]", null, null, out totalCount).ToDictionary(x => x.Key, y => y.Value);            
        }

        public Locales GetAllLocales(string culture)
        {
            Locales locales = new Locales();
            int? totalCount;
            var results = CommonDbContext.TranslationRepository.GetFiltered("locales", string.Format("Culture= '{0}'", culture), null, "[Id],[key],[value]", null, null, out totalCount);
            if(results.Any())
            {
                locales.Culture = culture;
                locales.LocaleItems = results.ToList();
            }
            return locales;
        }

        public Locale SaveLocale(Locale locale)
        {
            int? totalCount;
            Locale existingDBLocale = CommonDbContext.TranslationRepository.GetFiltered("locales", string.Format("[Key]= '{0}' AND Culture= '{1}'", locale.Key, locale.Culture), null, "[Id],[key],[Culture]", null, null, out totalCount).FirstOrDefault();
            if (existingDBLocale != null)
            {
                if (existingDBLocale.Id != locale.Id)
                {
                    BusinessException.Instance.ThrowHttpResponseException(new BusinessError()
                    {
                        BusinessModule = BusinessModule.Module.Security,
                        ErrorCodeDefinition = ErrorCode.LocaleAlreadyExist,
                        ReasonPhrase = "Locale already exists"
                    });
                }
            }
            return CommonDbContext.TranslationRepository.Add(locale);
        }

        public bool DeleteLocale(long id)
        {
            if (id == null || (id != null && id < 0))
            {
                BusinessException.Instance.ThrowHttpResponseException(new BusinessError()
                {
                    BusinessModule = BusinessModule.Module.Security,
                    ErrorCodeDefinition = ErrorCode.InvalidLocaleId,
                    ReasonPhrase = "Invalid Locale id"
                });
            }

            return CommonDbContext.TranslationRepository.Delete(id);
        }
    }
}
