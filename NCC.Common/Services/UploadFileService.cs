﻿using NCC.Common.Data;
using NCC.Common.Entities;
using NCC.Core.Services;

namespace NCC.Common.Services
{
    public class UploadFileService : IUploadFileService
    {
        public ICommonDbContext CommonDbContext;

        public UploadFileService(ICommonDbContext commonDbContext)
        {
            this.CommonDbContext = commonDbContext;
        }

        public UploadFile SaveUploadFile(UploadFile uploadFile)
        {
            if (uploadFile == null)
            {
                BusinessException.Instance.ThrowHttpResponseException(new BusinessError()
                {

                    BusinessModule = BusinessModule.Module.Common,
                    ErrorCodeDefinition = ErrorCode.InvalidParameter,
                    ReasonPhrase = "Incorrect parameter passed for method SaveUploadFile"

                });
            }

            return CommonDbContext.UploadFileRepository.Add(uploadFile);
        }
    }
}
