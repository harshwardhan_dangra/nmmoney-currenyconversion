﻿namespace NCC.Core.Utilities.Constants
{
    public enum HashAlgorithmEnum
    {
        None,
        SHA1,
        SHA256,
        SHA384,
        SHA512

    }
}
