﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.ModelBinding;

namespace Core.Utilities.Extensions
{
    public static class QueryOptionsHelper
    {
        public static void RetrieveDateOptions(ModelBindingContext bindingContext, QueryOptions options)
        {
            //GET FromDate
            var val = bindingContext.ValueProvider.GetValue(QueryOptions.FROMDATE_KEY);
            if (val != null)
            {
                DateTime dtFrom;
                if (DateTime.TryParseExact(val.AttemptedValue, QueryOptions.DATEFORMAT_UTC, null, System.Globalization.DateTimeStyles.None, out dtFrom))
                {
                    options.FromDate = dtFrom;
                }
                else
                {
                    bindingContext.ModelState.AddModelError(QueryOptions.FROMDATE_KEY, "invalid fromdate");
                }

            }
            val = null;

            //GET ToDate
            val = bindingContext.ValueProvider.GetValue(QueryOptions.TODATE_KEY);
            if (val != null)
            {
                DateTime dtTo;

                if (DateTime.TryParseExact(val.AttemptedValue, QueryOptions.DATEFORMAT_UTC, null, System.Globalization.DateTimeStyles.None, out dtTo))
                {
                    options.ToDate = dtTo;
                }
                else
                {
                    bindingContext.ModelState.AddModelError(QueryOptions.TODATE_KEY, "invalid todate");
                }
            }
        }
        public static void RetrieveRowCount(ModelBindingContext bindingContext, QueryOptions options)
        {
            var rowCount = bindingContext.ValueProvider.GetValue(QueryOptions.ROWCOUNT_KEY);
            int count;

            if (rowCount != null)
            {
                if (int.TryParse(rowCount.AttemptedValue, out count))
                {
                    options.RowCount = count;
                }
                else
                {
                    bindingContext.ModelState.AddModelError(QueryOptions.ROWCOUNT_KEY, "invalid rowcount");
                }
            }
        }
        public static void RetrieveCurrencyId(ModelBindingContext bindingContext, QueryOptions options)
        {
            var val = bindingContext.ValueProvider.GetValue(QueryOptions.CURRENCY_KEY);
            int currency;

            if (val != null)
            {
                if (int.TryParse(val.AttemptedValue, out currency))
                {
                    options.CurrencyId = currency;
                    if (currency <= 0)
                    {
                        bindingContext.ModelState.AddModelError(QueryOptions.CURRENCY_KEY, "invalid currencyid");
                    }
                }
                else
                {
                    bindingContext.ModelState.AddModelError(QueryOptions.CURRENCY_KEY, "invalid currencyid");
                }
            }
            else
            {
                options.CurrencyId = ApplicationConfig.DefaultCurrencyId; //Using Default CurrencyId from web config  -- USD
            }

        }

        public static void RetrieveModel(ModelBindingContext bindingContext, QueryOptions options)
        {
            var val = bindingContext.ValueProvider.GetValue(QueryOptions.MODEL_KEY);

            if (val != null)
            {
                options.Model = val.AttemptedValue;
            }
            else
            {
                bindingContext.ModelState.AddModelError(QueryOptions.MODEL_KEY, "invalid model");
            }
        }

        public static void RetrieveEntities(ModelBindingContext bindingContext, QueryOptions options)
        {
            RetrieveEntityTypeId(bindingContext, options);

            if (options.EntityTypeId.HasValue)
            {
                RetrieveEntityId(bindingContext, options);
            }

        }

        public static void RetrieveInterval(ModelBindingContext bindingContext, QueryOptions options)
        {
            var val = bindingContext.ValueProvider.GetValue(QueryOptions.INTERVAL_KEY);

            if (val != null)
            {
                options.Interval = val.AttemptedValue;
            }
            else
            {
                bindingContext.ModelState.AddModelError(QueryOptions.INTERVAL_KEY, "invalid interval key");
            }
        }

        private static void RetrieveEntityTypeId(ModelBindingContext bindingContext, QueryOptions options)
        {
            var val = bindingContext.ValueProvider.GetValue(QueryOptions.ENTITYTYPEID_KEY);
            int entityTypeId;

            if (val != null)
            {
                if (int.TryParse(val.AttemptedValue, out entityTypeId))
                {
                    options.EntityTypeId = entityTypeId;
                    if (entityTypeId <= 0)
                    {
                        bindingContext.ModelState.AddModelError(QueryOptions.CURRENCY_KEY, "invalid entityTypeId");
                    }
                }
                else
                {
                    bindingContext.ModelState.AddModelError(QueryOptions.ENTITYTYPEID_KEY, "invalid entityTypeId");
                }
            }
        }

        private static void RetrieveEntityId(ModelBindingContext bindingContext, QueryOptions options)
        {
            var val = bindingContext.ValueProvider.GetValue(QueryOptions.ENTITYID_KEY);
            int entityId;

            if (val != null)
            {
                if (int.TryParse(val.AttemptedValue, out entityId))
                {
                    options.EntityTypeId = entityId;
                    if (entityId <= 0)
                    {
                        bindingContext.ModelState.AddModelError(QueryOptions.CURRENCY_KEY, "invalid entityId");
                    }
                }
                else
                {
                    bindingContext.ModelState.AddModelError(QueryOptions.ENTITYTYPEID_KEY, "invalid entityId");
                }
            }
        }
    }
}
