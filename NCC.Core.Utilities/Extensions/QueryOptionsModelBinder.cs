﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.ModelBinding;

namespace Core.Utilities.Extensions
{
    public class QueryOptionsModelBinder : IModelBinder
    {
        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType != typeof(QueryOptions))
                return false;

            var options = new QueryOptions();
            QueryOptionsHelper.RetrieveDateOptions(bindingContext, options);
            QueryOptionsHelper.RetrieveRowCount(bindingContext, options);
            QueryOptionsHelper.RetrieveCurrencyId(bindingContext, options);

            bindingContext.Model = options;

            if (!bindingContext.ModelState.IsValid)
            {
                string errorMessage = string.Empty;

                foreach (var value in bindingContext.ModelState.Values)
                {
                    errorMessage += value.Errors[0].ErrorMessage + " - ";
                }
                return false;
            }
            return true;
        }
    }
}
