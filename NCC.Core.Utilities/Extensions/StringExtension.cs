﻿namespace NCC.Core.Utilities.Extensions
{
    public static class StringExtension
    {
        public static string HandleNull(this string value)
        {
            return (value == "null") ? null : value;
            
        }

        public static object HandleNull(this object value)
        {
            return (value != null &&  value.ToString() == "null") ? null : value;
            
        }
    }
}
