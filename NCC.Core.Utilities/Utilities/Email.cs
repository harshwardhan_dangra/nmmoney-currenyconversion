﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Core.Utilities
{
    public static class Email
    {
        #region Constant
        #endregion Constant


        public static bool SendMailFromTemplate(string emailAddresses, string subject, string templatePath, string mediaType, bool toCC)
        {
            bool isValid = false;

            if (templatePath.Length > 0 && File.Exists(templatePath))
            {
                string content = File.ReadAllText(templatePath);

                isValid = SendMail(emailAddresses, subject, content, mediaType, toCC);
            }

            return isValid;
        }


        public static bool SendMail(string emailAddresses, string subject, string message, string mediaType, bool toCC)
        {
            MailMessage objMailMsg = null;
            bool isValid = false;

            //Don't send mail if mail address is not known
            if (!String.IsNullOrEmpty(emailAddresses) && message.Length > 0)
            {
                try
                {
                    SmtpClient smtpClient = InitializeMailObject(emailAddresses, subject, message, mediaType, ref objMailMsg, toCC);

                    if (smtpClient != null && objMailMsg != null)
                    {
                        smtpClient.Send(objMailMsg);
                        //smtpClient.Dispose();
                    }

                    isValid = true;
                }
                catch (Exception)
                {
                    isValid = false;
                }
            }

            return isValid;

        }


        public static bool SendMail(string emailAddresses, string subject, string message, string mediaType, List<string> attachements, bool toCC)
        {
            MailMessage objMailMsg = null;
            bool isValid = false;

            //Don't send mail if mail address is not known
            if (!String.IsNullOrEmpty(emailAddresses) && message.Length > 0)
            {
                try
                {
                    SmtpClient smtpClient = InitializeMailObject(emailAddresses, subject, message, mediaType, ref objMailMsg, toCC);

                    if (smtpClient != null && objMailMsg != null)
                    {

                        if (attachements != null)
                        {
                            //AppDomain.CurrentDomain.SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal);
                            //WindowsIdentity identity = new WindowsIdentity("cleoshare", "cleoshare");
                            //WindowsImpersonationContext context = identity.Impersonate();

                            foreach (var filepath in attachements)
                                objMailMsg.Attachments.Add(new Attachment(filepath));

                        }

                        smtpClient.SendAsync(objMailMsg, null);
                        //smtpClient.Dispose();
                    }

                    isValid = true;
                }
                catch (Exception)
                {
                    isValid = false;
                }
            }

            return isValid;

        }


        private static SmtpClient InitializeMailObject(string emailAddresses, string subject, string message, string mediaType, ref MailMessage objMailMsg, bool toCC)
        {
            //Initialisations
            objMailMsg = new MailMessage();
            

            List<String> lstMailTo = new List<String>();
            
            lstMailTo.AddRange(emailAddresses.Split(','));

            foreach (string toItem in lstMailTo)
            {
                objMailMsg.To.Add(new MailAddress(toItem));
            }


            objMailMsg.From = new MailAddress(ApplicationConfig.MailFrom);



            // Carbon Copy
            if (!string.IsNullOrEmpty(ApplicationConfig.MailCC) && toCC)
            {

                List<String> lstMailCc = new List<String>();
                lstMailCc.AddRange(ApplicationConfig.MailCC.Split(','));

                foreach (string ccItem in lstMailCc)
                {
                    objMailMsg.CC.Add(new MailAddress(ccItem));
                }
            }
            


            // Subject and multipart/alternative Body
            objMailMsg.Subject = subject;
            objMailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(message, null, mediaType));
            objMailMsg.BodyEncoding = Encoding.UTF8;
            objMailMsg.IsBodyHtml = (mediaType == MediaTypeNames.Text.Html);

            

            // Init SmtpClient and send
            SmtpClient smtpClient = new SmtpClient(ApplicationConfig.SmtpServer, ApplicationConfig.SmtpPortNumber);
            smtpClient.EnableSsl = true;
            smtpClient.Timeout = 10000;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = new System.Net.NetworkCredential(ApplicationConfig.SmtpUserName, ApplicationConfig.SmtpUserPass);
            return smtpClient;
        }



        public static void SendMailAsync(string emailAddresses, string subject, string message, string mediaType, bool toCC)
        {
            //Task<bool> task1 = new Task<bool>(() => SendMail(emailAddresses, subject, message, mediaType));
            Task task1 = new Task(() => SendMail(emailAddresses, subject, message, mediaType, toCC));
            task1.Start();
            //task1.Wait();            
        }
    }
}
