﻿using System;
using System.Xml;

namespace NCC.Core.Utilities
{
    public static class XMLUtil
    {

        public static string ElementValue(XmlNode node, string name)
        {

            string value = null;

            if (node.SelectSingleNode(name) != null)
            {
                value = node.SelectSingleNode(name).InnerText;
            }

            return value;

        }

        public static int ElementIntegerBooleanValue(XmlNode node, string name)
        {

            int value = 0;

            if (node.SelectSingleNode(name) != null)
            {
                value = node.SelectSingleNode(name).InnerText.Equals("true", StringComparison.OrdinalIgnoreCase) ? 1 : 0;
            }

            return value;

        }

        public static int ElementIntegerValue(XmlNode node, string name)
        {

            int value = 0;

            if (node.SelectSingleNode(name) != null)
            {
                double val = 0;
                double.TryParse(node.SelectSingleNode(name).InnerText, out val);
                value = (int)val;
            }

            return value;

        }

        //public static int ElementIntegerRoundValue(XmlNode node, string name)
        //{

        //    int value = 0;

        //    if (node.SelectSingleNode(name) != null)
        //    {
        //        decimal d;
        //        decimal.TryParse(node.SelectSingleNode(name).InnerText, out d);

        //        if (d > 0)
        //        {
        //            value = Math.Round(d) as int;
        //        }

        //    }

        //    return value;

        //}

        public static DateTime? ElementDateValue(XmlNode node, string name)
        {

            DateTime? dt = null;

            if (node.SelectSingleNode(name) != null)
            {
                DateTime value = DateTime.MinValue;
                DateTime.TryParse(node.SelectSingleNode(name).InnerText, out value);

                if (value > DateTime.MinValue)
                    dt = value;

            }

            return dt;

        }


        public static string ElementAttribute(XmlAttributeCollection attributes, string name)
        {

            string value = null;

            if (attributes != null && attributes[name] != null && !string.IsNullOrEmpty(attributes[name].Value))
            {
                value = attributes[name].Value;
            }

            return value;

        }

        public static int ElementIntegerAttribute(XmlAttributeCollection attributes, string name)
        {
            int value = 0;

            if (attributes != null && attributes[name] != null && !string.IsNullOrEmpty(attributes[name].Value))
            {
                double val = 0;
                double.TryParse(attributes[name].Value, out val);
                value = (int)val;
            }

            return value;
        }


        public static DateTime? ElementDateTimeAttribute(XmlAttributeCollection attributes, string name)
        {
            DateTime? dt = null;

            if (attributes != null && attributes[name] != null && !string.IsNullOrEmpty(attributes[name].Value))
            {
                DateTime value = DateTime.MinValue;
                DateTime.TryParse(attributes[name].Value, out value);

                if (value > DateTime.MinValue)
                    dt = value;
            }

            return dt;
        }

    }
}
