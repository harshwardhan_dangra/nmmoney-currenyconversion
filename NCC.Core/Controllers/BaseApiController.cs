﻿using NCC.Core.Data;
using NCC.Core.Services;
using System.Web.Http;

namespace NCC.Core.Controllers
{
    public abstract class BaseApiController : ApiController
    {
        public IDbContext CommonDbContext
        {
            get;
            private set;

        }

        public IDbContext SecurityDbContext
        {
            get;
            private set;

        }

        public IService UserService
        {
            get;
            private set;

        }

        public IMainContract MainContractService
        {
            get;
            private set;

        }

        public IThirdPartyContract ThirdPartyContractService
        {
            get;
            private set;

        }

        protected BaseApiController(IDbContext commonDbContext, IDbContext securityDbContext, IService userService)
        {
            this.CommonDbContext = commonDbContext;
            this.SecurityDbContext = securityDbContext;
            this.UserService = userService;
           // this.MainContractService = mainContractService;
           // this.ThirdPartyContractService = thirdPartyContractService;

        }


    }
}
