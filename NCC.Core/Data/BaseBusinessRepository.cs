﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Web;
using NCC.Core.Services;

namespace NCC.Core.Data
{
    public abstract class BaseBusinessRepository//<T> : IBusinessRepository<T> where T: new()
    {
        protected IDbContext dbContext;
        protected Database database;        

        protected BaseBusinessRepository(IDbContext unitOfWork)
        {
            this.dbContext = unitOfWork;
            database = unitOfWork.CurrentDatabase;
        }
        
        public ITokenPrincipal GetTokenPrincipal()
        {
             ITokenPrincipal tokenPrincipal = null;
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                tokenPrincipal = (ITokenPrincipal)HttpContext.Current.User;
            }

            return tokenPrincipal;
        }
    }
}