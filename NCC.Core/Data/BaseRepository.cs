﻿using NCC.Core.Entities;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using NCC.Core.Services;
using NCC.Core.Utilities;
using NCC.Core.Utilities.Extensions;

namespace NCC.Core.Data
{
    public abstract class BaseRepository<T> : IRepository<T>
        where T : BaseEntity, new()
    {
        protected IDbContext dbContext;
        protected Database database;
        protected abstract string GetByIdStoredProcedureName { get; }
        protected abstract string GetFilteredStoredProcedureName { get; }
        protected abstract string UpSertStoredProcedureName { get; }
        protected abstract string DeleteStoredProcedureName { get; }
        protected long UserId;

        protected BaseRepository(IDbContext unitOfWork)
        {
            this.dbContext = unitOfWork;
            database = unitOfWork.CurrentDatabase;
            ITokenPrincipal tokenPrincipal = GetTokenPrincipal();

        }

        public ITokenPrincipal GetTokenPrincipal()
        {
            ITokenPrincipal tokenPrincipal = null;
            if (HttpContext.Current == null)
            {
                UserId = 0;
                return tokenPrincipal;
                
            }
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                tokenPrincipal = (ITokenPrincipal)HttpContext.Current.User;
                UserId = tokenPrincipal.UserId;
            }
            return tokenPrincipal;
        }

        public IEnumerable<T> GetAll()
        {
            //get from datastore
            var list = new List<T>();
            ITokenPrincipal tokenPrincipal = GetTokenPrincipal();

            object[] args = new object[] { UserId }; //replaced GetTokenPrincipal().UserId with UserId


            using (var command = database.GetStoredProcCommand(GetFilteredStoredProcedureName))
            {
                command.CommandTimeout = ApplicationConfig.CommandTimeout;
                SetGetFilteredStoredProcedureParameters(database, command, args);
                using (var reader = database.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        var obj = new T();
                        FillEntity(reader, obj);
                        list.Add(obj);
                    }
                    reader.Close();
                }

                return list;
            }
        }

        public virtual T GetById(long id)
        {
            //get from datastore
            var obj = new T();
            using (var command = database.GetStoredProcCommand(GetByIdStoredProcedureName))
            {
                command.CommandTimeout = ApplicationConfig.CommandTimeout;
                SetGetByIdStoredProcedureParameters(database, command, new object[] { id, UserId });//replaced GetTokenPrincipal().UserId with UserId
                using (var reader = database.ExecuteReader(command))
                {
                    if (reader.Read())
                        FillEntity(reader, obj);
                }

                return obj;
            }
        }

        //public T GetById(long id, string cipherKey)
        //{
        //    //get from datastore
        //    var obj = new T();
        //    using (var command = database.GetStoredProcCommand(GetByIdStoredProcedureName))
        //    {
        //        SetGetByIdStoredProcedureParameters(database, command, new object[] { id, cipherKey, UserId });//replaced GetTokenPrincipal().UserId with UserId
        //        using (var reader = database.ExecuteReader(command))
        //        {
        //            if (reader.Read())
        //                FillEntity(reader, obj);
        //        }

        //        return obj;
        //    }
        //}

        public IEnumerable<T> GetFiltered(string tableName, string filters, string sortExpression, string columns, int? pageSize, int? page, out int? totalCount)
        {
            //get from datastore
            var list = new List<T>();
            totalCount = default(int?);
            using (var command = database.GetStoredProcCommand(GetFilteredStoredProcedureName))
            {
                command.CommandTimeout = ApplicationConfig.CommandTimeout;
                SetGetFilteredStoredProcedureParameters(database, command, tableName, filters, sortExpression, columns, pageSize, page);
                using (var reader = database.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        var obj = new T();
                        FillEntity(reader, obj);

                        if (!totalCount.HasValue)
                        {
                            // Todo evaluate total row
                        }
                        list.Add(obj);
                    }
                    reader.Close();
                }


                return list;
            }
        }

        protected virtual void FillEntity(IDataReader reader, T entity)
        {
            reader.FillEntity(entity);
        }
        protected virtual void SetUpSertStoredProcedureParameters(Database database, string storedProcedureName, DbCommand command, T entity)
        {
            command.FillUpSertParameters(database, entity);
        }

        protected virtual void SetGetByIdStoredProcedureParameters(Database database, DbCommand command, params object[] parameters)
        {
            if (parameters == null)
                return;

            database.AddInParameter(command, "@Id", DbType.Int64, parameters[0]);
            //database.AddInParameter(command, "@UserId", DbType.Int64, parameters[1]);
        }

        protected virtual void SetDeleteStoredProcedureParameters(Database database, DbCommand command, params object[] parameters)
        {
            if (parameters == null)
                return;

            ITokenPrincipal tokenPrincipal = GetTokenPrincipal();
            long modifiedBy = 0;
            if (tokenPrincipal != null)
            {
                modifiedBy = UserId; //replaced tokenPrincipal.UserId with UserId
            }

            database.AddInParameter(command, "@Id", DbType.Int64, parameters[0]);
            //database.AddInParameter(command, "@ModifiedBy", DbType.Int64, modifiedBy);
        }

        protected virtual void SetGetFilteredStoredProcedureParameters(Database database, DbCommand command, params object[] parameters)
        {
            if (parameters == null)
                return;

            //database.AddInParameter(command, "@UserId", DbType.Int64, parameters[0]);
            if (parameters.Count() > 1)
            {
                if (parameters[0] != null)
                    database.AddInParameter(command, "@TableName", DbType.String, parameters[0]);
                if (parameters[1] != null)
                    database.AddInParameter(command, "@Filters", DbType.String, parameters[1]);
                if (parameters[2] != null)
                    database.AddInParameter(command, "@SortExpression", DbType.String, parameters[2]);
                if (parameters[3] != null)
                    database.AddInParameter(command, "@Columns", DbType.String, parameters[3]);
                if (parameters[4] != null)
                    database.AddInParameter(command, "@PageSize", DbType.Int32, parameters[4]);
                if (parameters[5] != null)
                    database.AddInParameter(command, "@Page", DbType.Int32, parameters[5]);
            }
        }

        public T Add(T entity)
        {
            //update in datastore
            using (var command = database.GetStoredProcCommand(UpSertStoredProcedureName))
            {
                command.CommandTimeout = ApplicationConfig.CommandTimeout;
                SetUpSertStoredProcedureParameters(database, UpSertStoredProcedureName, command, entity);
                var rowsAffected = 0;
                if (dbContext.Transaction != null)
                    rowsAffected = database.ExecuteNonQuery(command, dbContext.Transaction as DbTransaction);
                else
                    rowsAffected = database.ExecuteNonQuery(command);

                if (command.Parameters["@Id"].Value != DBNull.Value)
                {
                    var id = Convert.ToInt32(command.Parameters["@Id"].Value);
                    entity.GetType().GetProperty(entity.PrimaryKeyColumnName).SetValue(entity, id);
                }
                else
                {
                    entity = null;
                }

                return entity;
            }
        }


        public T Add(T entity,string StoredProcedureName)
        {
            //update in datastore
            using (var command = database.GetStoredProcCommand(StoredProcedureName))
            {
                command.CommandTimeout = ApplicationConfig.CommandTimeout;
                SetUpSertStoredProcedureParameters(database, StoredProcedureName, command, entity);
                var rowsAffected = 0;
                if (dbContext.Transaction != null)
                    rowsAffected = database.ExecuteNonQuery(command, dbContext.Transaction as DbTransaction);
                else
                    rowsAffected = database.ExecuteNonQuery(command);

                if (command.Parameters["@Id"].Value != DBNull.Value)
                {
                    var id = Convert.ToInt32(command.Parameters["@Id"].Value);
                    entity.GetType().GetProperty(entity.PrimaryKeyColumnName).SetValue(entity, id);
                }
                else
                {
                    entity = null;
                }

                return entity;
            }
        }

        public T Update(T entity)
        {
            //update in datastore
            using (var command = database.GetStoredProcCommand(UpSertStoredProcedureName))
            {
                command.CommandTimeout = ApplicationConfig.CommandTimeout;
                SetUpSertStoredProcedureParameters(database, UpSertStoredProcedureName, command, entity);
                var rowsAffected = 0;
                if (dbContext.Transaction != null)
                    rowsAffected = database.ExecuteNonQuery(command, dbContext.Transaction as DbTransaction);
                else
                    rowsAffected = database.ExecuteNonQuery(command);

                if (rowsAffected < 1)
                {
                    BusinessException.Instance.ThrowHttpResponseException(new BusinessError()
                    {
                        BusinessModule = BusinessModule.Module.Core,
                        ErrorCodeDefinition = ErrorCode.UpdateOperationFailed
                    });
                }

                return entity;
            }
        }

        public bool Delete(long id)
        {
            var rowsAffected = 0;
            //delete from datastore
            using (var command = database.GetStoredProcCommand(DeleteStoredProcedureName))
            {
                command.CommandTimeout = ApplicationConfig.CommandTimeout;
                SetDeleteStoredProcedureParameters(database, command, new object[] { id });

                if (dbContext.Transaction != null)
                    rowsAffected = database.ExecuteNonQuery(command, dbContext.Transaction as DbTransaction);
                else
                    rowsAffected = database.ExecuteNonQuery(command);
                //if rows affected is zero, throw exception ???
            }

            return rowsAffected > 0;
        }

        public Guid RunSqlJob(string jobName, string sqlStatement)
        {
            Guid jobId = Guid.Empty;
            using (var command = database.GetStoredProcCommand("ExecuteSQL_ByAgentJob")) //Kor_Construct_Test
            {
                command.CommandTimeout = ApplicationConfig.CommandTimeout;

                database.AddOutParameter(command, "@JobIdOut", DbType.Guid, 100);
                database.AddInParameter(command, "@SqlStatement", DbType.String, sqlStatement);
                database.AddInParameter(command, "@SPNameOrStmntTitle", DbType.String, jobName);
                database.AddInParameter(command, "@JobRunningUser", DbType.String, "sa");

                database.ExecuteScalar(command);

                if (command.Parameters["@JobIdOut"].Value != DBNull.Value)
                {
                    Guid.TryParse(command.Parameters["@JobIdOut"].Value.ToString(), out jobId);
                }
            }

            return jobId;
        }
        
    }
}
