﻿using NCC.Core.Services;
using NCC.Core.Utilities;
using System;
using System.Data;

namespace NCC.Core.Data
{
    public class CommonAppLogRepository : BaseRepository<CommonAppLog>
    {

        private const string GetByIdStoredProcName = "";
        private const string GetFilteredStoredProcName = "";
        private const string UpSertdateStoredProcName = "InsertCommonLogApp";
        private const string DeleteStoredProcName = "";

        protected override string GetByIdStoredProcedureName { get { return GetByIdStoredProcName; } }

        protected override string GetFilteredStoredProcedureName { get { return GetFilteredStoredProcName; } }

        protected override string UpSertStoredProcedureName { get { return UpSertdateStoredProcName; } }

        protected override string DeleteStoredProcedureName { get { return DeleteStoredProcName; } }


        protected override void SetUpSertStoredProcedureParameters(Microsoft.Practices.EnterpriseLibrary.Data.Database database, string storedProcedureName, System.Data.Common.DbCommand command, CommonAppLog entity)
        {
            base.SetUpSertStoredProcedureParameters(database, storedProcedureName, command, entity);

            database.AddInParameter(command, "@LogType", DbType.String, entity.LogType.ToString());
        }

        protected override void FillEntity(IDataReader reader, CommonAppLog entity)
        {
            base.FillEntity(reader, entity);

            if (!reader.IsClosed && reader["LogType"] != DBNull.Value)
            {
                entity.LogType = EnumUtilities.ParseEnum<LogType>(reader["LogType"].ToString());
            }
        }

        public CommonAppLogRepository(IDbContext dbContext)
            : base(dbContext)
        {
        }
    }
}