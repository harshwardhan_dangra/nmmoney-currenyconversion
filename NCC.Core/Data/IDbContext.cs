﻿using NCC.Core.Services;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace NCC.Core.Data
{
    public interface IDbContext
    {
        Database CurrentDatabase { get; }
        IDbTransaction Transaction { get; }
        IDbConnection Connection { get; }
        IRepository<CommonAppLog> CommonAppLogRepository { get;}

        long UserId { get; }
        void SetUser(long userId);

        void StartTransaction();
        void Commit();
        void Rollback();
    }
}
