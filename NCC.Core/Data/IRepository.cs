﻿using NCC.Core.Entities;
using System.Collections.Generic;

namespace NCC.Core.Data
{
    public interface IRepository<T> 
        where T : BaseEntity
    {
        IEnumerable<T> GetAll();
        T GetById(long key);
        //T GetById(long key, string cipherKey);
        IEnumerable<T> GetFiltered(string tableName, string filters, string sortExpression, string columns, int? pageSize, int? page, out int? totalCount);

        T Add(T entity);
        T Update(T entity);
        bool Delete(long key);
        
    }
}
