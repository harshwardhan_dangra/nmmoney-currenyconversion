﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace NCC.Core.Filters
{
    public class EmailValidatorAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

            if (value == null || (value != null && Regex.IsMatch(value.ToString(), @"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$")))
                return ValidationResult.Success;
            //throw business exception            
            return new ValidationResult("BillEmail");
        }

    }
}