﻿using NCC.Core.Services;
//using log4net;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;


namespace NCC.Core.Filters
{

    public class ExceptionHandlingAttribute : ExceptionFilterAttribute
    {

        //private readonly ILog log;


        //override 


        public override void OnException(HttpActionExecutedContext context)
        {
            //log info into Event log

            //log.Warn("test");

            if (!IsExceptionEnable)
            {
                HttpError internalError = new HttpError(){ { "ErrorCode", ErrorCode.InternalError.Code } , {"Message", ErrorCode.InternalError.Message}};
                context.Response = context.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, internalError);
            }
        }


        private bool IsExceptionEnable
        {
            get
            {
                bool sslAllow = true;
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ShowException"]))
                {
                    sslAllow = bool.Parse(ConfigurationManager.AppSettings["ShowException"]);
                }

                return sslAllow;
            }
        }



        private string BuildLogEntry(HttpActionContext actionContext)
        {
            string route = actionContext.Request.GetRouteData().Route.RouteTemplate;
            string method = actionContext.Request.Method.Method;
            string url = actionContext.Request.RequestUri.AbsoluteUri;
            string controllerName = actionContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            string actionName = actionContext.ActionDescriptor.ActionName;

            return string.Format("{0} {1}, route: {2}, controller:{3}, action:{4}", method, url, route, controllerName, actionName);
        }
    }
}
