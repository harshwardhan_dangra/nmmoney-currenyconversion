﻿using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;
using System.Net;

namespace NCC.Core.Services
{
    public class BusinessException
    {

        static object sync = new object();
        static BusinessException instance;

        private BusinessException()
        {
        }

        public static BusinessException Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (sync)
                    {
                        if (instance == null)
                        {
                            instance = new BusinessException();
                        }
                    }
                }

                return instance;
            }
        }

        public void ThrowHttpResponseException(BusinessError error)
        {
            HttpRequestMessage request = new HttpRequestMessage();
            request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            if (error.HttpStatusCode == 0)
            {
                error.HttpStatusCode = System.Net.HttpStatusCode.BadRequest;
            }

            //HttpContext.Current.Response = request.CreateResponse(error.HttpStatusCode, error)

            throw new HttpResponseException(request.CreateResponse(error.HttpStatusCode, error));
        }

        public void ThrowHttpResponse(HttpStatusCode code)
        {
            HttpRequestMessage request = new HttpRequestMessage();
            request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            throw new HttpResponseException(request.CreateResponse(code, new object()));
        }
    }
}
