﻿namespace NCC.Core.Services
{
    public class ErrorCode : BaseErrorCode
    {
        #region Base implementation
        public ErrorCode(string code, string message) : base(code, message) { }

        public override string ToString() { return Code; }
        #endregion

        /// <summary>
        /// Invalid Login response
        /// </summary>
        public static ErrorCode InternalError = new ErrorCode("1001", "Internal error");
        public static ErrorCode UpdateOperationFailed = new ErrorCode("1002", "Update operation failed");
        public static ErrorCode SqlTimeout = new ErrorCode("1003", "Data Currently Uploading");
        //public static ErrorCode UpdateContacAddressesFailed = new ErrorCode("1003", "Fail to update contact addresses");
    }
}
