﻿namespace NCC.Core.Services
{
    public enum LogType
    {
        None,
        Error,
        BusinessError,
        Information
    }
}