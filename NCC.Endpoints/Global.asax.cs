﻿using System.Web.Http;
using Microsoft.Practices.Unity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Web.Mvc;
using System.Web.Http.Cors;
using NCC.Core.Service.Services;
using NCC.Security.Service.Data;
using NCC.CMDB.Service.Data;
using NCC.Common.Data;
using NCC.Security.Service.Services;
using NCC.Common.Services;
using NCC.Security.Service.Filters;
using NCC.Security.Service.Handlers;
using NCC.Service.Services;
using System.Web.Http.ExceptionHandling;
using Swashbuckle.Application;

namespace NCC.Endpoints.API
{
    public class WebApiApplication : BaseWebApiApplication
    {

        protected override void SetHttpControllerTypeResolver(HttpConfiguration config)
        {
            //var cors = new EnableCorsAttribute(origins: "http://dev.alhuda.com/", headers: "*", methods: "*");
            config.EnableCors();
            GlobalConfiguration.Configuration.EnableSwagger(c => c.SingleApiVersion("v1", "A title for your API")).
            EnableSwaggerUi();
            //config.Services.Replace(typeof(IHttpControllerTypeResolver), new CustomHttpControllerTypeResolver(new[] { "Contract" }));
        }


        protected override void RegisterServices(IUnityContainer container)
        {
            var database = new DatabaseProviderFactory().CreateDefault();
            //var Database2 = new DatabaseProviderFactory().Create("Connection2");

            container.RegisterType<ISecurityDbContext, SecurityDbContext>(new InjectionConstructor(database));

            container.RegisterType<ICMDBContext, CMDBContext>(new InjectionConstructor(database));
            container.RegisterType<ICommonDbContext, CommonDbContext>(new InjectionConstructor(database));

            container.RegisterType<IUserService, UserService>();
            container.RegisterType<ICommonService, CommonService>();
            container.RegisterType<ITranslationService, TranslationService>();
            container.RegisterType<IUserRoleService, UserRoleService>();
            container.RegisterType<IScreenService, ScreenService>();
            container.RegisterType<ICurrencyService, CurrencyService>();
            container.RegisterType<IUploadService, UploadService>();
            container.RegisterType<IBranchService, BranchService>();

        }


        protected override void RegisterFilters(GlobalFilterCollection filters)
        {
            base.RegisterFilters(filters);
        }


        protected override void Configure(HttpConfiguration config)
        {
            //config.Filters.Add(new ValidateModelAttribute());
            config.Filters.Add(new TokenAttribute());
            config.MessageHandlers.Add(new ResponseHandler());
            config.MessageHandlers.Add(new HTTPSGuard());
            base.Configure(config);
        }

    }
}