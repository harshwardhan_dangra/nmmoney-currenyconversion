﻿using log4net;
using NCC.Core.Controllers;
using NCC.Core.Utilities;
using NCC.Security.Service.Data;
using NCC.Security.Service.Filters;
using NCC.Security.Service.Services;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;
using NCC.Service.Helpers;
using NCC.Service.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NCC.Endpoints.API.NCCCtl
{
    public class BranchController : BaseApiController
    {
        ISecurityDbContext _securityDbContext;
        IBranchService _branchService;
        IUserService _userService;
        ILog Log;
        public BranchController(ISecurityDbContext securityDbContext, IUserService userService, IBranchService branchService) : base(null, null, userService)
        {
            _securityDbContext = securityDbContext;
            _branchService = branchService;
            _userService = userService;
            Log = LogManager.GetLogger("Notify");
        }
        [HttpGet]
        [CancelToken]
        [Route("GetBranch")]
        public List<Branch> GetAllBranch()
        {
            try
            {
                return _branchService.GetAllBranch().ToList();
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }
        }
        [HttpDelete]
        [Route("DeleteBranch/{id}")]
        [CancelToken]
        public bool DeleteBranch(int id)
        {
            try
            {
                return _branchService.DeleteBranch(id);
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }
        }

        [HttpPost]
        [Route("SaveBranch")]
        [CancelToken]
        public CommonResponse<int> SaveBranchData(Branch branch)
        {
            try
            {
                var commonResponse = new CommonResponse<int>();
                if (ModelState.IsValid)
                {
                    var responseScreen = _branchService.SaveBranchData(branch);
                    if (responseScreen.Id > 0)
                    {
                        commonResponse.Result = 0;
                        commonResponse.Status = true;
                        commonResponse.Message = "Data Save Successfully";
                    }
                }
                else
                {
                    commonResponse.Result = 0;
                    commonResponse.Status = false;
                    commonResponse.Message = "Please fill correct information";
                }
                return commonResponse;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }

        }

        [HttpGet]
        [Route("GetSingleBranch/{id}")]
        [CancelToken]
        public BranchBO GetBranchById(int id)
        {
            try
            {
                var branchData = _branchService.GetSingleBranchRecord(id);
                return branchData;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }

        }
    }


}
