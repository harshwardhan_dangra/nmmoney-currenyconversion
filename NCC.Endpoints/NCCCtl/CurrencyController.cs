﻿using log4net;
using NCC.Core.Controllers;
using NCC.Core.Utilities;
using NCC.Endpoints.API.NMMoneyWebServices;
using NCC.Security.Service.Data;
using NCC.Security.Service.Filters;
using NCC.Security.Service.Services;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;
using NCC.Service.Helpers;
using NCC.Service.Services;
using OfficeOpenXml;
using OfficeOpenXml.Drawing.Chart;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace NCC.Endpoints.API.NCCCtl
{
    public class CurrencyController : BaseApiController
    {
        ISecurityDbContext _securityDbContext;
        ICurrencyService _currencyService;
        IUserService _userService;
        ILog Log;
        public CurrencyController(ISecurityDbContext securityDbContext, IUserService userService, ICurrencyService currencyService) : base(null, null, userService)
        {
            _securityDbContext = securityDbContext;
            _currencyService = currencyService;
            _userService = userService;
            Log = LogManager.GetLogger("Notify");
        }

        [HttpGet]
        [CancelToken]
        [Route("GetCurrency")]
        public List<CurrencyBO> GetAllCurrencies()
        {
            try
            {
                return _currencyService.GetAllCurrencies().ToList();
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }
        }

        [HttpPost]
        [CancelToken]
        [Route("UploadFileApi")]
        public HttpResponseMessage UploadJsonFile()
        {
            try
            {
                HttpResponseMessage response = new HttpResponseMessage();
                var httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count > 0)
                {
                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        var filePath = HttpContext.Current.Server.MapPath("~/UploadFile/" + postedFile.FileName);
                        postedFile.SaveAs(filePath);
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }

        }

        [HttpDelete]
        [Route("DeleteCurrency/{id}")]
        [CancelToken]
        public bool DeleteCurrency(int id)
        {
            try
            {
                return _currencyService.DeleteCurrency(id);
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }
        }

        [HttpPost]
        [Route("SaveCurrency")]
        [CancelToken]
        public CommonResponse<int> SaveCurrencyData(CurrencyBO currencyBO)
        {
            try
            {
                var commonResponse = new CommonResponse<int>();
                if (ModelState.IsValid)
                {
                    var responseScreen = _currencyService.SaveCurrencyData(currencyBO);
                    if (responseScreen.Id > 0)
                    {
                        commonResponse.Result = 0;
                        commonResponse.Status = true;
                        commonResponse.Message = "Data Save Successfully";
                    }
                }
                else
                {
                    commonResponse.Result = 0;
                    commonResponse.Status = false;
                    commonResponse.Message = "Please fill correct information";
                }
                return commonResponse;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }

        }

        [HttpGet]
        [Route("GetSingleCurrency/{id}")]
        [CancelToken]
        public CurrencyBO GetScreenRecordById(int id)
        {
            try
            {
                var screenData = _currencyService.GetSingleCurrencyRecord(id);
                return screenData;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }

        }

        [HttpPost]
        [Route("ConvertCurrency")]
        [CancelToken]
        public ConvertCurrencyBO ConvertCurrency(ConvertCurrencyBO currencyconvert)
        {
            try
            {
                ConvertCurrencyBO currencyConvertBo = new ConvertCurrencyBO();
                var client = new NMMoneyWebServicesSoapClient();
                var response = client.GetRates();
                DataTable table = new DataTable();
                int k = 0;
                foreach (var val in response)
                {
                    string[][] values = val.Split(';').Select(x => x.Split(',')).ToArray();
                    if (k == 0)
                    {
                        foreach (var item in values[0])
                        {
                            table.Columns.Add(item.ToString(), typeof(string));
                        }
                        //table.Columns.Add("col2", typeof(string));
                    }
                    else
                    {
                        DataRow newRow = table.NewRow();
                        int i = 0;
                        foreach (var item in values[0])
                        {
                            newRow[i] = item;
                            i++;
                            //table.Columns.Add(item.ToString(), typeof(string));
                        }
                        table.Rows.Add(newRow);
                    }
                    k++;
                }

                _currencyService.UpsertSlotRecord();
                var currentCurrency = table.AsEnumerable().Where(elem => elem.Field<string>("CURRCODE") == currencyconvert.CurrencyCode).FirstOrDefault();
                if (currentCurrency == null)
                {
                    currencyConvertBo.CurrecnyRate = 0;
                    currencyConvertBo.ExchangeRate = 0;
                }
                else
                {
                    if (Convert.ToDecimal(currencyconvert.CurrencyAmount) > Convert.ToDecimal(currentCurrency.ItemArray[4]))
                    {
                        currencyConvertBo.CurrecnyRate = Convert.ToDecimal(currentCurrency.ItemArray[3]);
                    }
                    else
                    {
                        currencyConvertBo.CurrecnyRate = Convert.ToDecimal(currentCurrency.ItemArray[2]);

                    }

                    //currencyconvert.CurrencyAmount = ((decimal)(currencyConvertBo.CurrecnyRate)).ToString();

                    currencyConvertBo.ExchangeRate = currencyConvertBo.CurrecnyRate * Convert.ToDecimal(currencyconvert.CurrencyAmount);
                }
                return currencyConvertBo;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }

        }

        [HttpGet]
        [Route("GetSlots")]
        [CancelToken]
        public List<ConvertCurrencyBO> GetAllSlots()
        {
            try
            {
                var data = _currencyService.GetAllSlots();
                return data;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }

        }


        [HttpGet]
        [Route("DownloadExcelColumn")]
        [CancelToken]
        public string GetPeerGroup_ComposerExcel()
        {
            try
            {
                var list = _currencyService.GetAllSlots();
                string filename = string.Format("Daily_Interaction_Excel-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", DateTime.UtcNow);

                string rootFolder = "ExcelTemplate";
                string template_fileName = @"Daily_Interaction_Excel_Template.xlsx";
                var urlFile = HttpContext.Current.Server.MapPath(Path.Combine(rootFolder, template_fileName));
                FileInfo files = new FileInfo(urlFile);
                var xlPackage = new ExcelPackage(files);

                int index = 15;

                ExcelWorksheet Sheet = xlPackage.Workbook.Worksheets["Sheet1"];

                for (int i = 0; i < 24; i++)
                //foreach (var r in list)
                {
                    //if (r.Slots == 0)
                    if (i == 0)
                        Sheet.Cells[index, 1].Value = "0-1 am";
                    //else if (r.Slots == 1)
                    if (i == 1)
                        Sheet.Cells[index, 1].Value = "1-2 am";
                    //else if (r.Slots == 2)
                    if (i == 2)
                        Sheet.Cells[index, 1].Value = "2-3 am";
                    //else if (r.Slots == 3)
                    if (i == 3)
                        Sheet.Cells[index, 1].Value = "3-4 am";
                    //else if (r.Slots == 4)
                    if (i == 4)
                        Sheet.Cells[index, 1].Value = "4-5 am";
                    //else if (r.Slots == 5)
                    if (i == 5)
                        Sheet.Cells[index, 1].Value = "5-6 am";
                    //else if (r.Slots == 6)
                    if (i == 6)
                        Sheet.Cells[index, 1].Value = "6-7 am";
                    //else if (r.Slots == 7)
                    if (i == 7)
                        Sheet.Cells[index, 1].Value = "7-8 am";
                    //else if (r.Slots == 8)
                    if (i == 8)
                        Sheet.Cells[index, 1].Value = "8-9 am";
                    //else if (r.Slots == 9)
                    if (i == 9)
                        Sheet.Cells[index, 1].Value = "9-10 am";
                    //else if (r.Slots == 10)
                    if (i == 10)
                        Sheet.Cells[index, 1].Value = "10-11 am";
                    //else if (r.Slots == 11)
                    if (i == 11)
                        Sheet.Cells[index, 1].Value = "11-12 pm";
                    //else if (r.Slots == 12)
                    if (i == 12)
                        Sheet.Cells[index, 1].Value = "12-1 pm";
                    //else if (r.Slots == 13)
                    if (i == 13)
                        Sheet.Cells[index, 1].Value = "1-2 pm";
                    //else if (r.Slots == 14)
                    if (i == 14)
                        Sheet.Cells[index, 1].Value = "2-3 pm";
                    //else if (r.Slots == 15)
                    if (i == 15)
                        Sheet.Cells[index, 1].Value = "3-4 pm";
                    //else if (r.Slots == 16)
                    if (i == 16)
                        Sheet.Cells[index, 1].Value = "4-5 pm";
                    //else if (r.Slots == 17)
                    if (i == 17)
                        Sheet.Cells[index, 1].Value = "5-6 pm";
                    //else if (r.Slots == 18)
                    if (i == 18)
                        Sheet.Cells[index, 1].Value = "6-7 pm";
                    //else if (r.Slots == 19)
                    if (i == 19)
                        Sheet.Cells[index, 1].Value = "7-8 pm";
                    //else if (r.Slots == 20)
                    if (i == 20)
                        Sheet.Cells[index, 1].Value = "8-9 pm";
                    //else if (r.Slots == 21)
                    if (i == 21)
                        Sheet.Cells[index, 1].Value = "9-10 pm";
                    //else if (r.Slots == 22)
                    if (i == 22)
                        Sheet.Cells[index, 1].Value = "10-11 pm";
                    //else if (r.Slots == 23)
                    if (i == 23)
                        Sheet.Cells[index, 1].Value = "11-12 am";
                    var SlotData = list.Where(x => x.Slots == i);
                    if (SlotData.Any())
                        Sheet.Cells[index, 2].Value = SlotData.FirstOrDefault().Count;//r.Count;
                    else
                        Sheet.Cells[index, 2].Value = 0;

                    Sheet.Cells[index, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.FromArgb(14, 136, 167));
                    Sheet.Cells[index, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.FromArgb(14, 136, 167));

                    index = index + 1;
                }

                var returnfilepath = HttpContext.Current.Server.MapPath(Path.Combine("downloads", filename));
                try
                {
                    xlPackage.SaveAs(new FileInfo(returnfilepath));
                }
                catch (Exception ex)
                {
                    var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                    var fileExists = File.Exists(filepath);
                    using (var writer = new StreamWriter(filepath, true))
                    {
                        if (!fileExists)
                        {
                            writer.WriteLine("Start Error Log for today");
                        }
                        writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                    }
                    throw ex;
                }


                return filename;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }

        }

        [HttpGet]
        [Route("downloadexcel")]
        [CancelToken]
        public HttpResponseMessage GetBookForHRM(string fileName)
        {
            try
            {
                string reqBook = HttpContext.Current.Server.MapPath(Path.Combine("downloads", fileName.Replace("&", "%26")));
                string bookName = reqBook;
                //converting Pdf file into bytes array  
                var dataBytes = File.ReadAllBytes(reqBook);
                //adding bytes to memory stream   
                var dataStream = new MemoryStream(dataBytes);
                HttpResponseMessage httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
                httpResponseMessage.Content = new StreamContent(dataStream);
                httpResponseMessage.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                httpResponseMessage.Content.Headers.ContentDisposition.FileName = fileName;
                httpResponseMessage.Content.Headers.ContentDisposition.FileNameStar = fileName;
                string extension = Path.GetExtension(bookName);
                httpResponseMessage.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");
                return httpResponseMessage;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }

        }

        [HttpGet]
        [Route("DownloadExcelColumnPie")]
        [CancelToken]
        public string PieChartCreate()
        {
            try
            {
                int index = 22;
                int chartIndex = 22;
                var list = _currencyService.GetAllSlots();
                string filename = string.Format("Peak_Times_Daily_average_interactions_Excel-{0:yyyy-MM-dd-HH-mm-ss}.xlsx", DateTime.UtcNow);

                string rootFolder = "ExcelTemplate";
                string template_fileName = @"Peak_Times_Daily_average_interactions.xlsx";
                var urlFile = HttpContext.Current.Server.MapPath(Path.Combine(rootFolder, template_fileName));
                FileInfo files = new FileInfo(urlFile);
                var xlPackage = new ExcelPackage(files);

                ExcelWorksheet Sheet = xlPackage.Workbook.Worksheets["Sheet1"];
                var lstAvg = list.Average(x => x.Count);
                list = list.Where(x => x.Count >= lstAvg).ToList();
                //Fill the table

                foreach (var r in list)
                {
                    if (r.Slots == 0)
                        Sheet.Cells[index, 1].Value = "0-1 am";
                    else if (r.Slots == 1)
                        Sheet.Cells[index, 1].Value = "1-2 am";
                    else if (r.Slots == 2)
                        Sheet.Cells[index, 1].Value = "2-3 am";
                    else if (r.Slots == 3)
                        Sheet.Cells[index, 1].Value = "3-4 am";
                    else if (r.Slots == 4)
                        Sheet.Cells[index, 1].Value = "4-5 am";
                    else if (r.Slots == 5)
                        Sheet.Cells[index, 1].Value = "5-6 am";
                    else if (r.Slots == 6)
                        Sheet.Cells[index, 1].Value = "6-7 am";
                    else if (r.Slots == 7)
                        Sheet.Cells[index, 1].Value = "7-8 am";
                    else if (r.Slots == 8)
                        Sheet.Cells[index, 1].Value = "8-9 am";
                    else if (r.Slots == 9)
                        Sheet.Cells[index, 1].Value = "9-10 am";
                    else if (r.Slots == 10)
                        Sheet.Cells[index, 1].Value = "10-11 am";
                    else if (r.Slots == 11)
                        Sheet.Cells[index, 1].Value = "11-12 pm";
                    else if (r.Slots == 12)
                        Sheet.Cells[index, 1].Value = "12-1 pm";
                    else if (r.Slots == 13)
                        Sheet.Cells[index, 1].Value = "1-2 pm";
                    else if (r.Slots == 14)
                        Sheet.Cells[index, 1].Value = "2-3 pm";
                    else if (r.Slots == 15)
                        Sheet.Cells[index, 1].Value = "3-4 pm";
                    else if (r.Slots == 16)
                        Sheet.Cells[index, 1].Value = "4-5 pm";
                    else if (r.Slots == 17)
                        Sheet.Cells[index, 1].Value = "5-6 pm";
                    else if (r.Slots == 18)
                        Sheet.Cells[index, 1].Value = "6-7 pm";
                    else if (r.Slots == 19)
                        Sheet.Cells[index, 1].Value = "7-8 pm";
                    else if (r.Slots == 20)
                        Sheet.Cells[index, 1].Value = "8-9 pm";
                    else if (r.Slots == 21)
                        Sheet.Cells[index, 1].Value = "9-10 pm";
                    else if (r.Slots == 22)
                        Sheet.Cells[index, 1].Value = "10-11 pm";
                    else if (r.Slots == 23)
                        Sheet.Cells[index, 1].Value = "11-12 am";
                    Sheet.Cells[index, 2].Value = r.Count;

                    Sheet.Cells[index, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.FromArgb(14, 136, 167));
                    Sheet.Cells[index, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.FromArgb(14, 136, 167));

                    index = index + 1;
                }

                //Add the chart to the sheet
                var piechart = Sheet.Drawings["Chart_1"] as ExcelPieChart;
                var series = piechart.Series.Add(Sheet.Cells[chartIndex, 2, (chartIndex - 1) + list.Count, 2], Sheet.Cells[chartIndex, 1, (chartIndex - 1) + list.Count, 1]);


                var returnfilepath = HttpContext.Current.Server.MapPath(Path.Combine("downloads", filename));
                try
                {
                    xlPackage.SaveAs(new FileInfo(returnfilepath));
                }
                catch
                {
                    throw new Exception("there is an error while downloading report");
                }

                return filename;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }

        }



    }



}
