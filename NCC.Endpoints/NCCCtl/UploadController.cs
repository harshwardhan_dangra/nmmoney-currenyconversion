﻿using log4net;
using NCC.Core.Controllers;
using NCC.Core.Utilities;
using NCC.Security.Service.Data;
using NCC.Security.Service.Filters;
using NCC.Security.Service.Services;
using NCC.Service.Entities;
using NCC.Service.Helpers;
using NCC.Service.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NCC.Endpoints.API.NCCCtl
{

    public class UploadController : BaseApiController
    {
        ISecurityDbContext _securityDbContext;
        IUploadService _uploadService;
        IUserService _userService;
        ILog Log;
        public UploadController(ISecurityDbContext securityDbContext, IUserService userService, IUploadService uploadService) : base(null, null, userService)
        {
            _securityDbContext = securityDbContext;
            _uploadService = uploadService;
            _userService = userService;
            Log = LogManager.GetLogger("Notify");
        }

        [HttpGet]
        [Route("GetImages")]
        [CancelToken]
        public IList<Images> GetImagesData()
        {
            try
            {
                var data = _uploadService.GetAllImageData();
                return data;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }

        }

        [HttpPost]
        [Route("UploadImage")]
        [CancelToken]
        public CommonResponse<int> SaveImageData()
        {
            try
            {
                string sPath = System.Web.Hosting.HostingEnvironment.MapPath(ApplicationConfig.ImageFolderPath);
                System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
                System.Web.HttpPostedFile hpf = hfc[0];
                string fileextension = string.Empty;
                string newFileName = string.Empty;
                string originalFileName = string.Empty;
                if (hpf.ContentLength > 0)
                {
                    fileextension = Path.GetExtension(hpf.FileName);
                    newFileName = Guid.NewGuid().ToString() + fileextension;
                    originalFileName = hpf.FileName;
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists(sPath + Path.GetFileName(newFileName)))
                    {
                        // SAVE THE FILES IN THE FOLDER.
                        hpf.SaveAs(sPath + Path.GetFileName(newFileName));
                    }
                }

                Images imageBO = new Images()
                {
                    FileName = newFileName,
                    OrignalName = originalFileName,
                    Status = 1
                };

                var commonResponse = new CommonResponse<int>();
                if (ModelState.IsValid)
                {
                    var responseScreen = _uploadService.SaveImageData(imageBO);
                    if (responseScreen.Id > 0)
                    {
                        commonResponse.Id = responseScreen.Id;
                        commonResponse.Result = 0;
                        commonResponse.Status = true;
                        commonResponse.Message = "Data Save Successfully";
                    }
                }
                else
                {
                    commonResponse.Result = 0;
                    commonResponse.Status = false;
                    commonResponse.Message = "Please fill correct information";
                }
                return commonResponse;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }

        }

        [HttpGet]
        [Route("GetSingleImage/{id}")]
        [CancelToken]
        public Images GetImageRecordById(int id)
        {
            try
            {
                var imagesData = _uploadService.GetSingleImageRecord(id);
                return imagesData;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }
        }

        [HttpDelete]
        [Route("DeleteImage/{id}")]
        [CancelToken]
        public bool DeleteImage(string id)
        {
            bool delete = false;
            try
            {
                var deleteImage = id.Split('_');
                foreach (var item in deleteImage)
                {
                    if(item != "null")
                    {
                        delete= _uploadService.DeleteImage(Convert.ToInt32(item));
                    }
                }
                return delete;
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }
        }
    }
}
