﻿using log4net;
using NCC.CMDB.Service.Data;
using NCC.Core.Controllers;
using NCC.Core.Utilities;
using NCC.Security.Service.BusinessObjects;
using NCC.Security.Service.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web.Http;

namespace BMS.Endpoints.API.BMSCtl
{
    [RoutePrefix("userrole")]
    public class UserRoleController : BaseApiController
    {
        #region DB contexts
        private ICMDBContext CMDBContext;
        #endregion

        #region Services
        private IUserRoleService _userRoleService;
        #endregion Services

        ILog Log;

        #region Constructor
        public UserRoleController(ICMDBContext CMDBContext, IUserService userService, IUserRoleService userRoleService) : base(null, null, userService)
        {
            this.CMDBContext = CMDBContext;
            this._userRoleService = userRoleService;
            Log = LogManager.GetLogger("Notify");
        }
        #endregion Constructor

        [Route("list")]
        [HttpGet]
        public List<UserRoleBO> List()
        {
            try
            {
                Dictionary<string, string> searchFields = ControllerContext.Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value, StringComparer.OrdinalIgnoreCase);
                return _userRoleService.UserRoleSearchList(searchFields);
            }
            catch (Exception ex)
            {
                var filepath = String.Format(String.Concat(ApplicationConfig.ErrorLogFolderPath, "ErrorLog_{0}.txt"), DateTime.Today.ToString("dd-MMM-yyyy"));
                var fileExists = File.Exists(filepath);
                using (var writer = new StreamWriter(filepath, true))
                {
                    if (!fileExists)
                    {
                        writer.WriteLine("Start Error Log for today");
                    }
                    writer.WriteLine("Error Message in  Occured at-- " + DateTime.Now + " : " + ex.ToString());
                }
                throw ex;
            }
        }

    }
}
