﻿using NCC.Core.Controllers;
using NCC.Security.Service.BusinessObjects;
using NCC.Security.Service.Constants;
using NCC.Security.Service.Data;
using NCC.Security.Service.Entities;
using NCC.Security.Service.Filters;
using NCC.Security.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;

namespace NCC.Endpoints.API.Controllers
{
    public class UsersController : BaseApiController
    {
        #region DB contexts
        ISecurityDbContext SecurityDbContext;
        IUserService UserService;
        #endregion

        public UsersController(ISecurityDbContext securityDbContext, IUserService userService)
            : base(null, securityDbContext, userService)
        {
            this.SecurityDbContext = securityDbContext;
            this.UserService = userService;
        }


        [SecurityPrincipal(Right.Read, SecurityPrincipalType.All)]        
        [HttpPost]
        [Route("login")]
        [CancelToken]
        public LoginResponse Login(LoginRequest loginRequest)
        {
            return UserService.Login(loginRequest);
        }


        [HttpGet]
        [Route("users/{id}")]
        //[SecurityPrincipalAttribute(Right.Read, SecurityPrincipalType.ManageUsers)]
        public User Get(long id)
        {
            // Validation 
            return UserService.GetUserId(id);
        }


        [HttpGet]
        [Route("user/{id}")]
        //[SecurityPrincipalAttribute(Right.Read, SecurityPrincipalType.ManageUsers)]
        public UserBO GetById(long id)
        {
            // Validation 
            return UserService.GetById(id);
        }


        [HttpGet]
        [Route("me")]
        //[SecurityPrincipalAttribute(Right.Read, SecurityPrincipalType.All)]
        public User GetMe()
        {
            User currentUser = ProfileContextHelper.CurrentTokenPrincipal.User;
            return UserService.GetUserId(currentUser.UserId);
        }


        [Route("users/register")]
        [HttpPost]
      
        // [CancelToken]
        public User Register(User user)
        {
            User u;
            if (user.UserId > 0)
            {
                u = UserService.UpdateUser(user);
            }
            else
            {
                u = UserService.InsertUser(user);
            }
            return u;

        }

        [HttpPost]
        [Route("registerFirstUser")]
        [CancelToken]
        public User RegisterFirstUser()
        {

            return UserService.RegisterFirstUser();

        }


        //[HttpPut]
        //[Route("users/{id}")]
        ////[SecurityPrincipalAttribute(Right.Update, SecurityPrincipalType.ManageUsers)]
        //public User UpdateUser(User user, long id)
        //{
        //    if (user != null)
        //    {
        //        user.UserId = id;
        //    }
        //    return UserService.Update(user);
        //}


        [HttpDelete]
        [Route("users/{id}")]
        //[SecurityPrincipalAttribute(Right.Delete, SecurityPrincipalType.ManageUsers)]
        public bool Delete(long id)
        {
            return UserService.DeleteUser(id);
        }


        //[SecurityPrincipalAttribute(Right.Read, SecurityPrincipalType.All)]
        [HttpPost]
        [Route("changePassword")]
        public object ChangePassword(ChangePasswordRequest changePasswordRequest)
        {
            return UserService.ChangePassword(changePasswordRequest);
        }


        //[SecurityPrincipalAttribute(Right.Read, SecurityPrincipalType.All)]
        [HttpPost]
        [Route("forgetPassword")]
        [CancelToken]
        public object ForgetPassword(LoginRequest emailRequest)
        {

            return UserService.ResetPassword(emailRequest, true);

        }


        [HttpGet]
        [Route("logout")]
        public void Logout(Guid userToken)
        {
            UserService.Logout(userToken);
        }


        [HttpGet]
        [Route("purge-all-auth-cache")]
        [CancelToken]
        public bool PurgeAllAuthCache()
        {
            ProfileContextHelper.PurgeAllCachedAuthProfiles();
            return true;
        }

        [HttpGet]
        [Route("users/list")]
        [CancelToken]
        public List<UserBO> SystemUserSearchList()
        {
            //HttpRequestMessage request
            Dictionary<string, string> searchFields = ControllerContext.Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value, StringComparer.OrdinalIgnoreCase);

            return UserService.UserList(searchFields);
        }

        [HttpGet]
        [Route("users/getUsersHomeCount")]
        public UserHomeBO getUsersHomeCount()
        {
            //HttpRequestMessage request
            Dictionary<string, string> searchFields = ControllerContext.Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value, StringComparer.OrdinalIgnoreCase);

            return UserService.UsersHomeCount(searchFields).FirstOrDefault();
        }


    }
}
