﻿using System.Runtime.Serialization;
using NCC.Security.Service.Entities;

namespace NCC.Security.Service.BusinessObjects
{
    [DataContract]
    public class UsersRegionBO : UsersRegion
    {


        [DataMember(Name = "totalRows")]
        public int TotalRows { get; set; }

    }
}
