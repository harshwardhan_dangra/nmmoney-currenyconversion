﻿namespace NCC.Security.Service.Constants
{
    public struct ConstantAttributes
    {
        public const string X_AUTH_TOKEN = "X-AuthToken";
    }
}