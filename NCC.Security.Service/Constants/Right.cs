﻿using System;

namespace NCC.Security.Service.Constants
{
    [Flags]
    public enum Right
    {
        None = 0,
        Create = 1,
        Read = 2,
        Update = 4,
        Delete = 8,
        Special = 1024
    }
}