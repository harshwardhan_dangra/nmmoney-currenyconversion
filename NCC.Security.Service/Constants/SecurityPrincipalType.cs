﻿namespace NCC.Security.Service.Constants
{
    public enum SecurityPrincipalType
    {
        /*
        None = 0,
        SuperUser = 1, //S0
        ManageUsers = 2,
        GeneralAccess = 3,
        Affiliate = 4,
        All = 5
        */
        None = 0,
        SuperAdmin=1,
        ClientAdmin=2,
        ClientUser=3,
        Affiliate=4,
        All =5

    }
}