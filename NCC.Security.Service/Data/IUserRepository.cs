﻿using NCC.Core.Data;
using NCC.Security.Service.BusinessObjects;
using NCC.Security.Service.Entities;
using System.Collections.Generic;
namespace NCC.Security.Service.Data
{
    public interface IUserRepository : IRepository<User>
    {

        List<AccessRight> GetUserEntityAccessRights(long? userId);
        List<UserBO> UserList(Dictionary<string, string> searchFields);
        List<UserHomeBO> UsersHomeCount(Dictionary<string, string> searchFields);

        List<UserBO> UserSearchList(Dictionary<string, string> searchFields);
        UserBO GetUserById(long id);
    }
}
