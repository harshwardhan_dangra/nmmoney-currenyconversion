﻿using NCC.Core.Data;
using NCC.Security.Service.BusinessObjects;
using NCC.Security.Service.Entities;
using System.Collections.Generic;
namespace NCC.Security.Service.Data
{
    public interface IUserRoleRepository : IRepository<UserRole>
    {

        List<UserRoleBO> UserRoleSearchList(Dictionary<string, string> searchFields);

    }
}
