﻿using NCC.Core.Data;
using NCC.Security.Service.Entities;
namespace NCC.Security.Service.Data
{
    public interface IUsersRegionRepository : IRepository<UsersRegion>
    {        

    }
}
