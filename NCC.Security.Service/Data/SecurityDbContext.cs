﻿using NCC.Core.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;


namespace NCC.Security.Service.Data
{
    public class SecurityDbContext : BaseDbContext, ISecurityDbContext
    {
        
        public IUserRepository UserRepository { get { return new UserRepository(this); } }
        public IUserAuthTokenRepository UserAuthTokenRepository { get { return new UserAuthTokenRepository(this); } }
        public IUserRoleRepository UserRoleRepository { get { return new UserRoleRepository(this); } }
        public IUsersRegionRepository UsersRegionRepository { get { return new UsersRegionRepository(this); } }
        public SecurityDbContext(Database database)
            : base(database)
        {
        }

    }
}
