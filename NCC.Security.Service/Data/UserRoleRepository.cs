﻿using System.Collections.Generic;
using NCC.Core.Data;
using NCC.Security.Service.Entities;
using System.Data;
using NCC.Security.Service.BusinessObjects;
using NCC.Core.Utilities;
using NCC.Core.Utilities.Extensions;
using System.Data.SqlClient;

namespace NCC.Security.Service.Data
{
    public class UserRoleRepository : BaseRepository<UserRole>, IUserRoleRepository
    {
        private const string GetByIdStoredProcName = "UserRole_Select";
        private const string GetFilteredStoredProcName = "TableFilteredRow";
        private const string UpSertStoredProcName = "UserRole_UpSert";
        private const string DeleteStoredProcName = "UserRole_Delete";

        protected override string GetByIdStoredProcedureName { get { return UserRoleRepository.GetByIdStoredProcName; } }

        protected override string GetFilteredStoredProcedureName { get { return UserRoleRepository.GetFilteredStoredProcName; } }

        protected override string UpSertStoredProcedureName { get { return UserRoleRepository.UpSertStoredProcName; } }

        protected override string DeleteStoredProcedureName { get { return UserRoleRepository.DeleteStoredProcName; } }

        public UserRoleRepository(IDbContext dbContext)
            : base(dbContext)
        {

        }

        public List<UserRoleBO> UserRoleSearchList(Dictionary<string, string> searchFields)
        {
            DataTable table = TableValueParameterExtension.GetKeyValueTypeTable(searchFields);
            List<UserRoleBO> userRoleBOList = new List<UserRoleBO>();
            UserRoleBO userRoleBO;

            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("UserRole_Specific_Search", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@ItemsTable", table); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Structured;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {

                            userRoleBO = new UserRoleBO();
                            DataReaderExtensions.FillEntity<UserRoleBO>(reader, userRoleBO);
                            userRoleBOList.Add(userRoleBO);
                        }
                    }
                }

            }

            return userRoleBOList;
        }

    }
}
