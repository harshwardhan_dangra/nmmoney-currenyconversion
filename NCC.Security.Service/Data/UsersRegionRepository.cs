﻿using NCC.Core.Data;
using NCC.Security.Service.Entities;

namespace NCC.Security.Service.Data
{
    public class UsersRegionRepository : BaseRepository<UsersRegion>, IUsersRegionRepository
    {
        private const string GetByIdStoredProcName = "UsersRegion_Select";
        private const string GetFilteredStoredProcName = "TableFilteredRow";
        private const string UpSertStoredProcName = "UsersRegion_UpSert";
        private const string DeleteStoredProcName = "UsersRegion_Delete";

        protected override string GetByIdStoredProcedureName { get { return UsersRegionRepository.GetByIdStoredProcName; } }

        protected override string GetFilteredStoredProcedureName { get { return UsersRegionRepository.GetFilteredStoredProcName; } }

        protected override string UpSertStoredProcedureName { get { return UsersRegionRepository.UpSertStoredProcName; } }

        protected override string DeleteStoredProcedureName { get { return UsersRegionRepository.DeleteStoredProcName; } }

        public UsersRegionRepository(IDbContext dbContext)
            : base(dbContext)
        {

        }       

    }
}
