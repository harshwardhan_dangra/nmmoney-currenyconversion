﻿using NCC.Core.Entities;
using System.Runtime.Serialization;

namespace NCC.Security.Service.Entities
{
    [DataContract]
    public class UsersRegion : BaseEntity
    {
        [DataMember(Name = "UsersRegionId")]
        public long UsersRegionId { get; set; }


        [DataMember(Name = "RegionId")]
        public long RegionId { get; set; }

        [DataMember(Name = "UserId")]
        public long UserId { get; set; }


        public UsersRegion()
            : base("UsersRegionId") { }
    }
}
