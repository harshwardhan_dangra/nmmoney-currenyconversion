﻿using System;

namespace NCC.Security.Service.Filters
{
    /// <summary>
    /// Attribute to cancel the global action of Token Attribute
    /// </summary>
    public class CancelTokenAttribute : Attribute
    {

    }
}
