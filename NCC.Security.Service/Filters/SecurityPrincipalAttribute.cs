﻿using NCC.Core.Services;
using NCC.Security.Service.BusinessObjects;
using NCC.Security.Service.Constants;
using NCC.Security.Service.Services;
using System;
using System.Linq;
using System.Net;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace NCC.Security.Service.Filters
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class SecurityPrincipalAttribute : ActionFilterAttribute
    {
        public SecurityPrincipalType SecPrincipalId { get; set; }
        public Right Right { get; set; }

        public SecurityPrincipalAttribute(Right right, SecurityPrincipalType secPrincipal)
        {
            SecPrincipalId = secPrincipal;
            Right = right;
        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            //base.OnAuthorization(actionContext);
            base.OnActionExecuting(actionContext);

            //If Right is set to All then allow everyone
            if (SecPrincipalId != SecurityPrincipalType.All)
            {

                TokenPrincipal tokenPrincipal = ProfileContextHelper.CurrentTokenPrincipal;
                AuthProfile authProfile = null;
                bool valid = false;

                if (tokenPrincipal != null)
                {
                    authProfile = ProfileContextHelper.CachedUserAuthProfile(tokenPrincipal.Token);
                }

                // Check if a profile exists. Otherwise return false
                if (authProfile != null)
                {

                    //Check if user has a super admin role
                    if (authProfile.IsSuperAdmin)
                    {
                        valid = true;
                    }
                    //If user does not have a super admin role
                    else
                    {
                        //TODO: verify if user is allow to access this ressource


                        //filter by security principal
                        var profileSecurityPrinciples = authProfile.ProfileAccessRights.Where(ps => SecPrincipalId.HasFlag(ps.SecurityPrincipal));

                        //if (profileSecurityPrinciples != null && profileSecurityPrinciples.Count() > 0)
                        //{
                        //    //TODO: To filter based on user rights according to its corresponding EntityType 

                                // alhuda = 0,
                                // Contract = 1000,
                                // Metadata = 2000,
                                // Affiliate = 3000

                        //    var profileEntities = profileSecurityPrinciples.Where(
                        //        pe => pe.XX == YY 
                        //        );

                        //    valid = authProfile.IsEntityAllowed(profileEntities, Right);

                        //}

                    }
                }

                if (!valid)
                {
                    BusinessException.Instance.ThrowHttpResponseException(new BusinessError()
                    {
                        BusinessModule = BusinessModule.Module.Security,
                        ErrorCodeDefinition = NCC.Security.Service.Services.ErrorCode.AccessDenied,
                        ReasonPhrase = "Invalid rights",
                        HttpStatusCode = HttpStatusCode.Forbidden
                    });
                }
            }

            // return valid;
        }
    }
}