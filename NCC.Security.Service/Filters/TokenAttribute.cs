﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using NCC.Security.Service.Entities;
using System.Web.Http.Controllers;
using NCC.Core.Controllers;
using NCC.Core.Utilities;
using NCC.Security.Service.Services;
using NCC.Security.Service.Constants;
using NCC.Core.Services;
using NCC.Security.Service.BusinessObjects;

namespace NCC.Security.Service.Filters
{
    public class TokenAttribute : AuthorizeAttribute
    {
        protected IUserService UserService { get; set; }

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            if (ApplicationConfig.DisableToken)
            {
                return true;
            }
            ValidateHeaderAuthToken(actionContext);
            return true;
        }

        #region Private method
        private void ValidateHeaderAuthToken(HttpActionContext actionContext)
        {
            if (actionContext.ActionDescriptor.GetCustomAttributes<CancelTokenAttribute>().Any() == false)
            {
                IEnumerable<string> values;
                var isAuthTokenAvailable = actionContext.Request.Headers.TryGetValues(ConstantAttributes.X_AUTH_TOKEN, out values);

                if (isAuthTokenAvailable)
                {
                    // Compare origin here with a list of valid origins (domains) in the DB (maybe cache this list)
                    var authTokenValue = values.First();

                    isAuthTokenAvailable = isAuthTokenValid(authTokenValue, actionContext);
                }

                if (!isAuthTokenAvailable)
                {
                    DisplayErrorOnAccessDenied();
                }
            }
        }

        private bool isAuthTokenValid(string authTokenValue, HttpActionContext actionContext)
        {

            // Check DB for validity for token; eventually check with cache.
            Guid tokenKey;
            Guid.TryParse(authTokenValue, out tokenKey);

            bool valid = false;

            if (tokenKey != null && tokenKey != Guid.Empty)
            {
                AuthProfile authProfile = ProfileContextHelper.CachedUserAuthProfile(tokenKey);

                if (authProfile != null)
                {
                    //Check from cache token expiry
                    if (authProfile.TokenPrincipal != null)
                    {
                        if (authProfile.TokenPrincipal.ExpiryDate > DateTime.Now)
                        {

                            //Check if user is accessing the right domain/uri
                            //authProfile.Sites.wher

                            //Assign the cache token to the current thread
                            ProfileContextHelper.CurrentTokenPrincipal = authProfile.TokenPrincipal;

                            //Todo check against cache profile to see if user is allowed
                            valid = true;
                        }

                        else
                        {
                            authProfile = null;
                        }
                    }
                }

                UserService = ((BaseApiController)actionContext.ControllerContext.Controller).UserService as IUserService;

                //Load from DB in case the cache has been reset
                if (authProfile == null)
                {

                    TokenPrincipal tokenPrincipal = UserService.GetUserAuthToken(tokenKey, null);

                    if (tokenPrincipal != null)
                    {
                        if (tokenPrincipal.ExpiryDate < DateTime.Now)
                        {
                            BusinessException.Instance.ThrowHttpResponseException(new BusinessError()
                            {
                                BusinessModule = BusinessModule.Module.Security,
                                ErrorCodeDefinition = NCC.Security.Service.Services.ErrorCode.TokenExpired,
                                ReasonPhrase = "Token has expired",
                                HttpStatusCode = HttpStatusCode.Unauthorized
                            });
                        }

                        List<AccessRight> userAccessRights = UserService.GetUserEntityAccessRights(tokenPrincipal.User.UserId);
                        authProfile = ProfileContextHelper.AssignCurrentPrincipal(tokenPrincipal, userAccessRights);

                        valid = true;
                    }
                }

                // Todo: additional check upon user profile 
            }

            return valid;

        }

        private void DisplayErrorOnAccessDenied()
        {

            BusinessException.Instance.ThrowHttpResponseException(new BusinessError()
            {
                BusinessModule = BusinessModule.Module.Security
                ,
                ErrorCodeDefinition = NCC.Security.Service.Services.ErrorCode.AccessDenied
                ,
                ReasonPhrase = "Request filterred by TokenAttribute"
                ,
                HttpStatusCode = HttpStatusCode.Unauthorized
            });

        }
        #endregion Private method
    }
}
