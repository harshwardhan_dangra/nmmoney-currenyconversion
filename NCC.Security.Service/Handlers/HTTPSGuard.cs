﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Configuration;
using System.Web.Http;

namespace NCC.Security.Service.Handlers
{
    public class HTTPSGuard : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (!request.RequestUri.Scheme.Equals(Uri.UriSchemeHttps, StringComparison.OrdinalIgnoreCase) && IsSSLEnable)
            {
                HttpError InvalidSSl = new HttpError() { { "ErrorCode", NCC.Security.Service.Services.ErrorCode.HttpsRequired.Code }, { "Message", NCC.Security.Service.Services.ErrorCode.HttpsRequired.Message } };

                HttpResponseMessage reply = request.CreateErrorResponse(HttpStatusCode.BadRequest, InvalidSSl);
                return Task.FromResult(reply);
            }

            return base.SendAsync(request, cancellationToken);
        }

        private bool IsSSLEnable
        {
            get
            {
                bool sslAllow = true;
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["EnableSSL"]))
                {
                    sslAllow = bool.Parse(ConfigurationManager.AppSettings["EnableSSL"]);
                }

                return sslAllow;
            }
        }


    }
}