﻿using NCC.Core.Services;

namespace NCC.Security.Service.Services
{
    public class ErrorCode : BaseErrorCode
    {
        #region Base implementation
        public ErrorCode(string code, string message) : base(code, message) { }
        

        public override string ToString() { return Code; }
        #endregion

        /// <summary>
        /// Invalid Login response
        /// </summary>
        public static ErrorCode HttpsRequired = new ErrorCode("2001", "Secured Sockets Layer (HTTPS) is required for security reason.");
        public static ErrorCode InvalidLogin  = new ErrorCode("2002", "Invalid Login");
        public static ErrorCode AccessDenied  = new ErrorCode("2003", "Authorization has been denied for this request.");
        public static ErrorCode InvalidUserDetails = new ErrorCode("2004", "Missing user information");
        public static ErrorCode UserIdMissing = new ErrorCode("2005", "User id missing");
        public static ErrorCode UserNotCreated = new ErrorCode("2006", "User not created");
        public static ErrorCode InvalidUserEmailOrPassword = new ErrorCode("2007", "Invalid user email or password");
        public static ErrorCode EmailAlreadyInUse = new ErrorCode("2008", "Email already in use");
        public static ErrorCode TokenExpired = new ErrorCode("2009", "Token Expired");
        public static ErrorCode TokenPasswordMismatch = new ErrorCode("2010", "Authentication failed - Invalid Token or Password");
        public static ErrorCode FailToGenerateUserAuthToken = new ErrorCode("2011", "Fail to generate user authentication token");
        public static ErrorCode UserNotExists = new ErrorCode("2012", "User does not exist");
    }
}
