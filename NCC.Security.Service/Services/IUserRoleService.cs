﻿using NCC.Core.Services;
using NCC.Security.Service.BusinessObjects;
using System.Collections.Generic;

namespace NCC.Security.Service.Services
{
    public interface IUserRoleService : IService
    {
        List<UserRoleBO> UserRoleSearchList(Dictionary<string, string> searchFields);
    }
}
