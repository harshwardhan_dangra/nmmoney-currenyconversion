﻿using NCC.Core.Services;
using NCC.Security.Service.BusinessObjects;
using NCC.Security.Service.Entities;
using System;
using System.Collections.Generic;

namespace NCC.Security.Service.Services
{
    public interface IUserService : IService
    {

        User GetUserId(long id);
        User InsertUser(User user);
        User UpdateUser(User user);
        bool DeleteUser(long id);
        LoginResponse Login(LoginRequest loginRequest);
        void Logout(Guid userToken);
        object ChangePassword(ChangePasswordRequest changePasswordRequest);
        object ResetPassword(LoginRequest emailRequest, bool isForgetPassword);
        TokenPrincipal GetUserAuthToken(Guid token, string email);
        List<AccessRight> GetUserEntityAccessRights(long? userId);
        List<UserBO> UserList(Dictionary<string, string> searchFields);
        User RegisterFirstUser();
        List<UserHomeBO> UsersHomeCount(Dictionary<string, string> searchFields);
        List<UserBO> UserSearchList(Dictionary<string, string> searchFields);
        UserBO GetById(long id);
    }
}
