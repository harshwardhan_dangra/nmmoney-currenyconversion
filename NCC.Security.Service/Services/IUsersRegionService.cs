﻿using NCC.Core.Services;
using NCC.Security.Service.Entities;

namespace NCC.Security.Service.Services
{
    public interface IUsersRegionService : IService
    {
        UsersRegion InsertUserRegion(UsersRegion user);
    }
}
