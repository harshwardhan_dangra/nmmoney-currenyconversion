﻿using NCC.Core.Utilities;
using NCC.Security.Service.Entities;
using System.IO;
using System.Web;

namespace NCC.Security.Service.Services
{
    public static class SecurityUtilities
    {
        
        public static string GetNewUserEmailContent(User user)
        {
            string content = "";
            string templatePath = string.Concat(ApplicationConfig.TemplatesPath, "Email\\NewUserAccount.html");
            //HostingEnvironment.MapPath("~\\Templates\\Email\\NewUserAccount.html");
            if (templatePath.Length > 0 && File.Exists(templatePath))
            {
                content = File.ReadAllText(templatePath);

                if (content.Length > 0)
                {
                    content = content.Replace("{User_Firstname}", user.FirstName).Replace("{User_Lastname}", user.LastName).Replace("{User_password}", user.Password);

                    content = content.Replace("{Protocol}", HttpContext.Current.Request.Url.Scheme).Replace("{Domain}", HttpContext.Current.Request.Url.Host);
                
                }
            }
            return content;
        }
    
    }
}
