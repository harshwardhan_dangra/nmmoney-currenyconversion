﻿using NCC.Security.Service.BusinessObjects;
using NCC.Security.Service.Data;
using System.Collections.Generic;

namespace NCC.Security.Service.Services
{
    public class UserRoleService : IUserRoleService
    {

        private ISecurityDbContext SecurityDbContext { get; set; }


        public UserRoleService(ISecurityDbContext securityDbContext)
        {
            this.SecurityDbContext = securityDbContext;
        }

        public List<UserRoleBO> UserRoleSearchList(Dictionary<string, string> searchFields)
        {
            return this.SecurityDbContext.UserRoleRepository.UserRoleSearchList(searchFields);
        }
    }
}
