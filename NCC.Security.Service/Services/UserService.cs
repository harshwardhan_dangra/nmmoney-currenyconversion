﻿using NCC.Core.Services;
using NCC.Core.Utilities;
using NCC.Core.Utilities.Constants;
using NCC.Security.Service.BusinessObjects;
using NCC.Security.Service.Data;
using NCC.Security.Service.Entities;
using NCC.CMDB.Service.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;

namespace NCC.Security.Service.Services
{
    public class UserService : IUserService
    {

        private ISecurityDbContext SecurityDbContext { get; set; }
        private ICMDBContext CMDBContext;


        public UserService(ISecurityDbContext securityDbContext, ICMDBContext cmdbContext)
        {
            this.SecurityDbContext = securityDbContext;
            this.CMDBContext = cmdbContext;
        }


        public LoginResponse Login(LoginRequest loginRequest)
        {

            LoginResponse loginResponse = null;

            if (loginRequest == null)
            {
                BusinessException.Instance.ThrowHttpResponseException(new BusinessError()
                {
                    BusinessModule = BusinessModule.Module.Security,
                    ErrorCodeDefinition = NCC.Security.Service.Services.ErrorCode.InvalidUserEmailOrPassword,
                    ReasonPhrase = "user does not exist"
                });
            }

            //Get User from DB
            int? totalCount;
            User user = SecurityDbContext.UserRepository.GetFiltered("User", string.Format("email = '{0}' AND Status > 0", loginRequest.Email), null, null, null, null, out totalCount).FirstOrDefault();

            if (user == null)
            {

                // User does not exists
                BusinessException.Instance.ThrowHttpResponseException(new BusinessError()
                {
                    BusinessModule = BusinessModule.Module.Security,
                    ErrorCodeDefinition = NCC.Security.Service.Services.ErrorCode.UserNotExists,
                    ReasonPhrase = "user does not exist"
                });
            }

            if (Hash.VerifyHash(loginRequest.Password, HashAlgorithmEnum.SHA512, user.Password))
            {
                // User exist

                //Check if any token has not yet expired
                UserAuthToken userAuthToken = SecurityDbContext.UserAuthTokenRepository.GetFiltered("UserAuthToken", string.Format("UserId = '{0}' AND ExpiryDate > GETDATE()", user.UserId), null, null, null, null, out totalCount).FirstOrDefault(); // cast('{1}' as datetime) ==  DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss")

                //If token does not exisgt create one
                if (userAuthToken == null)
                {
                    userAuthToken = new UserAuthToken
                    {
                        ExpiryHours = ApplicationConfig.LoginTokenExpiryHours,
                        ExpiryDate = DateTime.Now.AddHours(ApplicationConfig.LoginTokenExpiryHours),
                        LoginDate = DateTime.Now,
                        TokenKey = Guid.NewGuid(),
                        UserId = user.UserId
                    };

                    userAuthToken = SecurityDbContext.UserAuthTokenRepository.Add(userAuthToken);
                }

                //If unable to create token then fire message
                if (userAuthToken == null)
                {

                    // User does not exists
                    BusinessException.Instance.ThrowHttpResponseException(new BusinessError()
                    {
                        BusinessModule = BusinessModule.Module.Security,
                        ErrorCodeDefinition = NCC.Security.Service.Services.ErrorCode.FailToGenerateUserAuthToken,
                        ReasonPhrase = "failed to generate user auth token"
                    });

                }
                var accessRights = GetUserEntityAccessRights(user.UserId);

                loginResponse = new LoginResponse
                {
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    //Password = user.Password,
                    Token = userAuthToken.TokenKey,
                    UserId = user.UserId,
                    UserType = GetUserType(accessRights)

                };

            }
            else
            {

                // User does not exists
                BusinessException.Instance.ThrowHttpResponseException(new BusinessError()
                {
                    BusinessModule = BusinessModule.Module.Security,
                    ErrorCodeDefinition = NCC.Security.Service.Services.ErrorCode.InvalidUserEmailOrPassword,
                    ReasonPhrase = "Invalid user name"
                });

            }

            return loginResponse;
        }

        public void Logout(Guid userToken)
        {

            SecurityDbContext.UserAuthTokenRepository.Logout(userToken);
        }


        public User GetUserId(long id)
        {
            if (id == null || (id != null && id < 0))
            {
                BusinessException.Instance.ThrowHttpResponseException(new BusinessError()
                {
                    BusinessModule = BusinessModule.Module.Security,
                    ErrorCodeDefinition = NCC.Security.Service.Services.ErrorCode.UserIdMissing,
                    ReasonPhrase = "Invalid user id"
                });
            }

            return SecurityDbContext.UserRepository.GetById(id);
        }


        public User InsertUser(User user)
        {
            if (user == null)
            {
                BusinessException.Instance.ThrowHttpResponseException(new BusinessError()
                {
                    BusinessModule = BusinessModule.Module.Security,
                    ErrorCodeDefinition = NCC.Security.Service.Services.ErrorCode.InvalidUserDetails,
                    ReasonPhrase = "Invalid user details"
                });
            }
            int? totalCount = null;

            if (SecurityDbContext.UserRepository.GetFiltered("User", "email='" + user.Email + "'", null, null, null, null, out totalCount).Count() > 0)
            {
                //BusinessException.Instance.ThrowHttpResponseException(new BusinessError()
                //{
                //    BusinessModule = BusinessModule.Module.Security,
                //    ErrorCodeDefinition = NCC.Security.Service.Services.ErrorCode.EmailAlreadyInUse,
                //    ReasonPhrase = "User email already in use"
                //});
                var existingUser = new User()
                {
                    UserId = -100,
                    FirstName = "User email already in use"
                };
                return existingUser;
            }

            if (user.UserId == 0)
            {
                user.Password = user.Password;
            }


            user.Password = user.Password;
            user.Password = Hash.ComputeHash(user.Password, HashAlgorithmEnum.SHA512, null);

            SecurityDbContext.UserRepository.Add(user);

            if (user.UserId > 0)
            {
                String content = SecurityUtilities.GetNewUserEmailContent(user);

                if (content.Length > 0)
                {
                    Email.SendMail(user.Email, "Welcome to Korrect online platform", content, MediaTypeNames.Text.Html, false); //Resources.NewUserEmailSubject
                }

            }
            else
            {
                BusinessException.Instance.ThrowHttpResponseException(new BusinessError()
                {
                    BusinessModule = BusinessModule.Module.Security,
                    ErrorCodeDefinition = NCC.Security.Service.Services.ErrorCode.UserNotCreated,
                    ReasonPhrase = "User not created"
                });
            }

            return user;
        }

        public User RegisterFirstUser()
        {

            int? totalCount = null;

            if (SecurityDbContext.UserRepository.GetFiltered("User", "", null, null, null, null, out totalCount).Count() > 0)
            {
                BusinessException.Instance.ThrowHttpResponseException(new BusinessError()
                {
                    BusinessModule = BusinessModule.Module.Security,
                    ErrorCodeDefinition = NCC.Security.Service.Services.ErrorCode.UserNotCreated,
                    ReasonPhrase = "User not created"
                });
            }

            User usr = new User();
            usr.Email = ApplicationConfig.UserName;
            usr.Password = Hash.ComputeHash(ApplicationConfig.UserPwd, HashAlgorithmEnum.SHA512, null);
            usr.FirstName = ApplicationConfig.UserFirstName;
            usr.LastName = ApplicationConfig.UserLastName;
            usr.Status = 2;

            return SecurityDbContext.UserRepository.Add(usr);

        }


        public User UpdateUser(User user)
        {
            if (user == null)
            {
                BusinessException.Instance.ThrowHttpResponseException(new BusinessError()
                {
                    BusinessModule = BusinessModule.Module.Security,
                    ErrorCodeDefinition = NCC.Security.Service.Services.ErrorCode.InvalidUserDetails,
                    ReasonPhrase = "Invalid user details"
                });
            }
            // Invalid user id
            else if (user != null && user.UserId <= 0)
            {
                BusinessException.Instance.ThrowHttpResponseException(new BusinessError()
                {
                    BusinessModule = BusinessModule.Module.Security,
                    ErrorCodeDefinition = NCC.Security.Service.Services.ErrorCode.UserIdMissing,
                    ReasonPhrase = "User id missing"
                });
            }
            string password;
            string newencyptpassword;
            if (!string.IsNullOrEmpty(user.Password))
            {
                password = user.Password;
                newencyptpassword = Hash.ComputeHash(password, HashAlgorithmEnum.SHA512, null);
                int? totalCount = null;
                user.Password = SecurityDbContext.UserRepository.GetFiltered("User", "UserID='" + user.UserId + "'", null, null, null, null, out totalCount).ToList().FirstOrDefault().Password;

            }
            else
            {
                int? totalCount = null;
                user.Password = SecurityDbContext.UserRepository.GetFiltered("User", "UserID='" + user.UserId + "'", null, null, null, null, out totalCount).ToList().FirstOrDefault().Password;
                newencyptpassword = user.Password;
            }
            if (!user.Password.Equals(newencyptpassword))
            {
                user.Password = newencyptpassword;
            }
            SecurityDbContext.UserRepository.Update(user);

            return user;
        }


        public bool DeleteUser(long id)
        {

            if (id == null || (id != null && id < 0))
            {
                BusinessException.Instance.ThrowHttpResponseException(new BusinessError()
                {
                    BusinessModule = BusinessModule.Module.Security,
                    ErrorCodeDefinition = NCC.Security.Service.Services.ErrorCode.UserIdMissing,
                    ReasonPhrase = "Invalid user id"
                });
            }

            return SecurityDbContext.UserRepository.Delete(id);
        }

        public object ChangePassword(ChangePasswordRequest changePasswordRequest)
        {
            if (!(changePasswordRequest != null && !string.IsNullOrEmpty(changePasswordRequest.NewPassword) && changePasswordRequest.OldPassword == changePasswordRequest.NewPassword))
            {
                BusinessException.Instance.ThrowHttpResponseException(new BusinessError()
                {
                    BusinessModule = BusinessModule.Module.Security,
                    ErrorCodeDefinition = Security.Service.Services.ErrorCode.InvalidUserEmailOrPassword,
                    ReasonPhrase = "invalid User password"
                });
            }
            var emailRequest = new LoginRequest
            {
                Email = changePasswordRequest.Email,
                Password = changePasswordRequest.NewPassword
            };


            return ResetPassword(emailRequest, false);
        }


        public object ResetPassword(LoginRequest emailRequest, bool isForgetPassword)
        {

            if (string.IsNullOrEmpty(emailRequest.Email))
            {
                BusinessException.Instance.ThrowHttpResponseException(new BusinessError()
                {
                    BusinessModule = BusinessModule.Module.Security,
                    ErrorCodeDefinition = NCC.Security.Service.Services.ErrorCode.InvalidUserEmailOrPassword,
                    ReasonPhrase = "invalid User email"
                });
            }

            int? totalCount = null;
            User user = SecurityDbContext.UserRepository.GetFiltered("User", "email='" + emailRequest.Email + "'", null, null, null, null, out totalCount).FirstOrDefault();//.GetUserByEmail(emailRequest.email);

            if (user == null)
            {
                //throw Business Error
                BusinessException.Instance.ThrowHttpResponseException(new BusinessError()
                {
                    BusinessModule = BusinessModule.Module.Security,
                    ErrorCodeDefinition = NCC.Security.Service.Services.ErrorCode.InvalidUserEmailOrPassword,
                    ReasonPhrase = "user does not exist"
                });
            }

            if (isForgetPassword)
            {

                string pwd = PasswordGenerator.CreateRandomPassword(12, true);
                user.Password = pwd;
                user.Password = Hash.ComputeHash(pwd, HashAlgorithmEnum.SHA512, null);
            }
            else
            {
                user.Password = emailRequest.Password;
                user.Password = Hash.ComputeHash(emailRequest.Password, HashAlgorithmEnum.SHA512, null);
            }

            SecurityDbContext.UserRepository.Update(user);

            if (isForgetPassword)
            {
                String content = SecurityUtilities.GetNewUserEmailContent(user);

                if (content.Length > 0)
                {
                    Email.SendMail(user.Email, "Your new password for Korrect online platform", content, MediaTypeNames.Text.Html, false);
                }
            }

            return new { result = true };

        }


        public TokenPrincipal GetUserAuthToken(Guid token, string email)
        {

            return SecurityDbContext.UserAuthTokenRepository.GetUserFromTokenOrEmail(token, email);

        }


        public List<AccessRight> GetUserEntityAccessRights(long? userId)
        {
            return SecurityDbContext.UserRepository.GetUserEntityAccessRights(userId);
        }

        private string GetUserType(List<AccessRight> accessRights)
        {
            if (accessRights.Any())
            {
                switch (accessRights.First().SecurityPrincipal)
                {
                    case Constants.SecurityPrincipalType.SuperAdmin: return "admin";

                    case Constants.SecurityPrincipalType.ClientAdmin:
                    case Constants.SecurityPrincipalType.ClientUser: return "client";

                    case Constants.SecurityPrincipalType.Affiliate: return "affiliate";
                }

            }
            return "admin";
        }


        public List<UserBO> UserList(Dictionary<string, string> searchFields)
        {
            return SecurityDbContext.UserRepository.UserList(searchFields);
        }

        public List<UserBO> UserSearchList(Dictionary<string, string> searchFields)
        {
            return SecurityDbContext.UserRepository.UserSearchList(searchFields);
        }

        public List<UserHomeBO> UsersHomeCount(Dictionary<string, string> searchFields)
        {
            return SecurityDbContext.UserRepository.UsersHomeCount(searchFields);
        }

        public UserBO GetById(long id)
        {
            return SecurityDbContext.UserRepository.GetUserById(id);
        }
    }
}
