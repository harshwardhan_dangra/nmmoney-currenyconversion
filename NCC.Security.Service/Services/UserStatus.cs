﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Security.Service.Services
{
    public enum  UserStatus
    {
        None,
        Active,
        Created,
        Forget,
        Pending,
        Blocked,
        SecurityBlocked // Temporary block on failed attempts
    }
}
