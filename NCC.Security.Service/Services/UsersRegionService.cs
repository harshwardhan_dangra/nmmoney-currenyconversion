﻿using NCC.Security.Service.Data;
using NCC.Security.Service.Entities;

namespace NCC.Security.Service.Services
{
    public class UsersRegionService : IUsersRegionService
    {

        private ISecurityDbContext SecurityDbContext { get; set; }


        public UsersRegionService(ISecurityDbContext securityDbContext)
        {
            this.SecurityDbContext = securityDbContext;
        }       

        public UsersRegion InsertUserRegion(UsersRegion user)
        {
           return SecurityDbContext.UsersRegionRepository.Add(user);
        }
    }
}
