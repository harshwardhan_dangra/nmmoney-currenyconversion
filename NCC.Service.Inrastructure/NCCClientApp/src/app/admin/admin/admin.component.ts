import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/service/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  loginService:LoginService;
  router:Router;
    constructor(_loginService:LoginService,_router: Router) { 
      this.loginService = _loginService;
      this.router = _router;
    }

  ngOnInit() {
    if(!this.loginService.IsUserAuthenticated()){
      this.router.navigate(['/']);
    }
  }

}
