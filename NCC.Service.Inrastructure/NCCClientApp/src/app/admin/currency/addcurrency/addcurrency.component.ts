import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { UploadService } from 'src/service/upload/upload.service';
import { ImageGalleryService } from 'src/service/imageGallery/image-gallery.service';
import { NgxGalleryOptions, NgxGalleryImage } from 'ngx-gallery';
import { Globalconfig } from 'src/global/globalconfig';
import { ActivatedRoute, Router } from '@angular/router';
import { DOCUMENT } from '@angular/platform-browser';
import { NgForm, FormGroup, FormControl } from '@angular/forms';
import { Currency } from 'src/model/Currency/Currency';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { CurrencyService } from 'src/service/currency/currency.service';
import { LoginService } from 'src/service/login.service';

@Component({
  selector: 'app-addcurrency',
  templateUrl: './addcurrency.component.html',
  styleUrls: ['./addcurrency.component.css']
})
export class AddcurrencyComponent implements OnInit {

  fileList: Array<File>;
  _uploadService: UploadService;
  errorMessage: String;
  errorMessagedisplay: boolean;
  _imageGalleryService: ImageGalleryService;
  private baseImageUrl: string = "";
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];
  @ViewChild('custForm') custForm: NgForm;
  title = 'Add Currency';
  _loginService: LoginService;
  router: Router;
  myGroup: FormGroup = null;
  result: any = {
    Id: 0,
    Result: 0,
    Status: false,
    Message: ""
  };
  CurrencyId: number = 0;
  public modelref: BsModalRef;
  SelectedImageId: number;
  SelectedImageSrc: string;
  SelectedImageSrcLocal: string;
  SelectedImageView: string;
  currency: Currency = null;
  Id: number;
  CurrencyName: string;
  CurrencyDescription: string;
  CurrencyCode: string;
  CurrencySymbol: string;
  ImageId: number;
  uploadImageId:number;



  constructor(@Inject(DOCUMENT) private document: any, uploadService: UploadService,
    imageGalleryService: ImageGalleryService, private modalService: BsModalService,
    loginService: LoginService, private _currencyService: CurrencyService, _router: Router, private route: ActivatedRoute) {
    this._imageGalleryService = imageGalleryService;
    this._loginService = loginService;
    this.router = _router;
    this._uploadService = uploadService;

  }

  ngOnInit() {
    if (document.location.hostname == "localhost") {
      this.baseImageUrl = Globalconfig.LocalGalleryImageFolderUrl + '/';
    } else {
      this.baseImageUrl = Globalconfig.GalleryImageFolderUrl + '/';
    }
    var paramId = this.route.snapshot.params["id"];
    if (paramId != undefined) {
      this.title = "Edit Currency"
      this.GetCurrencyRecordById(paramId);
    } else {
      this.title = "Add Currency"
    }
    this.myGroup = new FormGroup({
      ScreenName: new FormControl("CurrencyName")
    });
  }

  GetCurrencyRecordById(id: number) {
    this._currencyService.GetCurrencyRecordById(id).subscribe(res => {
      if (res != null) {
        this.Id = res.Id;
        this.currency = res;
        // this.filterChanged(this.Screen.ScreenType)
        this.CurrencyName = this.currency.CurrencyName;
        this.CurrencyCode = this.currency.CurrencyCode;
        this.CurrencyDescription = this.currency.CurrencyDescription;
        this.CurrencyId = this.currency.Id;
        this.ImageId = this.currency.ImageId;
        this.CurrencySymbol = this.currency.CurrencySymbol;
        this.SelectedImageSrc = this.baseImageUrl + this.currency.ImageUrl;
        console.log(res)
      }
    })
  }


  fileChange(event) {    
    this.fileList = <Array<File>>event.target.files;
    if (this.fileList.length === 0)
    return;

  var mimeType = this.fileList[0].type;
  if (mimeType.match(/image\/*/) == null) {
    this.errorMessage = "Only images are supported.";
    return;
  }

  var reader = new FileReader();  
  reader.readAsDataURL(this.fileList[0]); 
  reader.onload = (_event) => { 
    this.SelectedImageSrc = reader.result.toString(); 
  }
      }

  getGalleryImages(): any {
    this._imageGalleryService.GetAllImages().subscribe(res => {
      var galleryImages = [];
      res.forEach(element => {
        if(element.Id == this.uploadImageId)
        {
          this.ImageId = this.uploadImageId;
          this.SelectedImageSrc = this.baseImageUrl + element.FileName;
        }
        element.ImageUrl = this.baseImageUrl + element.FileName;
      });
      this.galleryImages = res;
    })
  }
  
  CloseModelWithLastValue() {
    this.modelref.hide();
    this.SelectedImageSrc = this.SelectedImageSrcLocal;
  }
  SelectedImage(event) {
    this.SelectedImageId = event.target.id;
    this.SelectedImageSrcLocal = event.target.currentSrc;
    this.SelectedImageView = 'imageselected';
  }
  open(content) {
    this.modelref = this.modalService.show(content, Object.assign({}, { class: 'model-lg' }))
    this.getGalleryImages();
  }


  AddCurrency() {       
    if (this.fileList != undefined && this.fileList.length != 0) {
      let formData: FormData = new FormData();
      formData.append("uploadedFiles", this.fileList[0], this.fileList[0].name);

      this._uploadService.uploadImage(formData).subscribe(
        (res) => {
          this.uploadImageId = res.Id;
          var newCurrency = new Currency(this.CurrencyId, this.custForm.value.CurrencyName, this.custForm.value.CurrencyDescription, this.custForm.value.CurrencyCode,
            this.custForm.value.CurrencySymbol, res.Id, null);
          this._currencyService.AddCurrency(newCurrency).subscribe(data => {
            this.result = data;
            if (this.result.Status) {
              this.errorMessage = this.result.Message;
              this.errorMessagedisplay = true;              
              setTimeout(function () {
                this.errorMessagedisplay = false;
                this.router.navigate(['/admin/currencyList']);
              }.bind(this), 5000)

            }

          });
        })
    } else {      
      var newCurrency = new Currency(this.CurrencyId, this.custForm.value.CurrencyName, this.custForm.value.CurrencyDescription, this.custForm.value.CurrencyCode,
        this.custForm.value.CurrencySymbol, this.ImageId, null);

      this._currencyService.AddCurrency(newCurrency).subscribe(data => {
        this.result = data;
        if (this.result.Status) {
          this.errorMessage = this.result.Message;
          this.errorMessagedisplay = true;          
          setTimeout(function () {
            this.errorMessagedisplay = false;
            this.router.navigate(['/admin/currencyList']);
          }.bind(this), 5000)

        }

      });

    }
  }
}
