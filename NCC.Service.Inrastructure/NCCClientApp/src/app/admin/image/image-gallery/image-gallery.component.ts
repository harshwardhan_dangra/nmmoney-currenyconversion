import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { ImageGalleryService } from 'src/service/imageGallery/image-gallery.service';
import { Globalconfig } from 'src/global/globalconfig';
import { DOCUMENT } from '@angular/platform-browser';
import { UploadService } from 'src/service/upload/upload.service';
import { PageChangedEvent } from 'ngx-bootstrap';

@Component({
  selector: 'app-image-gallery',
  templateUrl: './image-gallery.component.html',
  styleUrls: ['./image-gallery.component.css']
})
export class ImageGalleryComponent implements OnInit {
  galleryImages: any[];
  totalItems:number=0;
  imageGalleryService: ImageGalleryService;
  private baseImageUrl: string = "";
  fileList: File[];
  uploadService: UploadService;
  errorMessagedisplay: boolean;
  errorMessage: string;
  bgImage: any;
  deleteImage: string = null;
  galleryImagesListPaged: any[];

  constructor(_imageGalleryService: ImageGalleryService, @Inject(DOCUMENT) private document: any, _uploadService: UploadService) {
    this.imageGalleryService = _imageGalleryService
    this.uploadService = _uploadService;
    if (document.location.hostname == "localhost") {
      this.baseImageUrl = Globalconfig.LocalGalleryImageFolderUrl + '/';
    } else {
      this.baseImageUrl = Globalconfig.GalleryImageFolderUrl + '/';
    }
  }

  ngOnInit() {
    this.getGalleryImages()
  }

  setValueInArray(item: any) {
    this.deleteImage += "_" + item.id;
  }
  DeleteImages() {
    this.imageGalleryService.DeleteImages(this.deleteImage).subscribe(res => {
      this.getGalleryImages();
    });
  }

  getGalleryImages(): any {
    this.imageGalleryService.GetAllImages().subscribe(res => {
      var galleryImages = [];
      res.forEach(element => {
        var obj = {
          small: this.baseImageUrl + element.FileName,
          medium: this.baseImageUrl + element.FileName,
          big: this.baseImageUrl + element.FileName,
          id: element.Id
        }
        galleryImages.push(obj);
      });
      this.totalItems = galleryImages.length;
      this.galleryImages = galleryImages;
      this.galleryImagesListPaged = this.galleryImages.slice(0, 16);
    })
  }

  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.galleryImagesListPaged = this.galleryImages.slice(startItem, endItem);
  }


  fileChange(event) {
    this.fileList = <Array<File>>event.target.files;
    if (this.fileList != undefined && this.fileList.length != 0) {
      let formData: FormData = new FormData();
      formData.append("uploadedFiles", this.fileList[0], this.fileList[0].name);

      this.uploadService.uploadImage(formData).subscribe(
        (res) => {
          if (res.Status) {
            this.getGalleryImages()
            this.errorMessage = "File Uploaded Successfully";
            this.errorMessagedisplay = true;
            this.bgImage = null;
            setTimeout(function () {
              this.errorMessagedisplay = false;
            }, 5000)
          } else {
            this.errorMessage = "File Uploaded Failed";
            this.errorMessagedisplay = true;
            setTimeout(function () {
              this.errorMessagedisplay = false;
            }, 5000)
          }
        })

    }

  }

}




