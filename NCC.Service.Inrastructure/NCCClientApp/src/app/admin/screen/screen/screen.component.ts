import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ScreenService } from 'src/service/Screen/screen.service';
import { Screens } from 'src/model/Screen/screens';
import { PageChangedEvent } from 'ngx-bootstrap';


@Component({
  selector: 'app-screen',
  templateUrl: './screen.component.html',
  styleUrls: ['./screen.component.css']
})
export class ScreenComponent implements OnInit {

  isDeleteSuccess : boolean;
  title = 'Screens';
  ScreenList:Screens[]=null;
  returnedScreens: Screens[]= null;
  IsLoggedIn = false;
  errorMessagedisplay: boolean;
  errorMessage: String;
  router : Router;
  totalItems: number;


  ngOnInit() : void {  
    this.GetScreens();  
  }

  constructor(private _screenService: ScreenService,  _router: Router) {
    this.router = _router;
  }
  DeleteScreen(id:number)
  {
    
    this._screenService.DeleteScreen(id).subscribe(res => {      
      this.isDeleteSuccess = res;      
        this.GetScreens();
        alert("Deleted Successfully");        
      })
  }


  GetScreens(){    
      this._screenService.GetScreens().subscribe(res => {
        this.ScreenList = res;
        this.totalItems = res.length;
        this.returnedScreens = this.ScreenList.slice(0, 10);        
        if (this.returnedScreens != null) {          
         
        } else {
          this.errorMessagedisplay = true;
          this.errorMessage="Some Error Occured";
          this.IsLoggedIn = true;
        }
      });
    
  }

  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.returnedScreens = this.ScreenList.slice(startItem, endItem);
  }

}
