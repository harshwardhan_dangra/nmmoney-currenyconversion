import { Component, OnInit, ViewChild } from '@angular/core';
import { AveragePipe } from 'src/app/pipe/Average/average.pipe';
import { ExtractArrayByPropertyPipe } from 'src/app/pipe/ExtractArrayByProperty/extract-array-by-property.pipe';
import { Router } from '@angular/router';
import { CurrencyconvertService } from 'src/service/currencyconvert/currencyconvert.service';
import { saveAs as importedSaveAs } from "file-saver";


@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css'],
  providers: [AveragePipe, ExtractArrayByPropertyPipe]
})
export class StatsComponent implements OnInit {
  @ViewChild('cchartPie') cchartPie;
  @ViewChild('cchartColumn') cchartColumn;
  router: Router;
  averageArray = [];
  dataArray = [['Slots', 'Count']];
  columnDataArray = [['Slots', 'Count']];
  columnChartData: { chartType: string; dataTable: string[][]; options: { 'legend': string; chartArea: { left: number; top: number; width: string; height: string; }; }; };
  pieChartData: { chartType: string; dataTable: string[][]; options: { 'legend': string; pieSliceText: string, chartArea: { left: number; top: number; width: string; height: string; }; }; };
  displayPieChart: boolean = false;
  displayColumnChart: boolean = false;

  constructor(_router: Router, private _slotService: CurrencyconvertService, private averagePipe: AveragePipe, private extractarraybyproperty: ExtractArrayByPropertyPipe) {
    this.router = _router;
    //this.FetchSlots();

  }

  ngOnInit() {
    this.FetchSlots();

  }
  GenerateExcelPie() {
    this._slotService.GetExcelNamePie().subscribe(res => {

      console.log(res);
      this._slotService.GenerateExcel(res).subscribe(resData => {
        console.log(resData);
        importedSaveAs(new Blob([resData]), res);
      });
    });
  }

  GenerateExcel() {
    this._slotService.GetExcelName().subscribe(res => {

      console.log(res);
      this._slotService.GenerateExcel(res).subscribe(resData => {
        console.log(resData);
        importedSaveAs(new Blob([resData]), res);
      });
    });
  }

  FetchSlots() {
    this._slotService.GetSlots().subscribe(res => {

      var valAverage = this.averagePipe.transform(this.extractarraybyproperty.transform(res, "Count"))

      var data = res.filter(x => x.Count >= valAverage);

      var columnChartDataArray = [];
      data.forEach(element => {
        var PieChartDataArray = [];
        if (element.Slots == 0) {
          PieChartDataArray = ['12-1 am', element.Count];
        }
        if (element.Slots == 1) {
          PieChartDataArray = ['1-2 am', element.Count];
        }
        if (element.Slots == 2) {
          PieChartDataArray = ['2-3 am', element.Count];
        }
        if (element.Slots == 3) {
          PieChartDataArray = ['3-4 am', element.Count];
        }
        if (element.Slots == 4) {
          PieChartDataArray = ['4-5 am', element.Count];
        }
        if (element.Slots == 5) {
          PieChartDataArray = ['5-6 am', element.Count];
        }
        if (element.Slots == 6) {
          PieChartDataArray = ['6-7 am', element.Count];
        }
        if (element.Slots == 7) {
          PieChartDataArray = ['7-8 am', element.Count];
        }
        if (element.Slots == 8) {
          PieChartDataArray = ['8-9 am', element.Count];
        }
        if (element.Slots == 9) {
          PieChartDataArray = ['9-10 am', element.Count];
        }
        if (element.Slots == 10) {
          PieChartDataArray = ['10-11 am', element.Count];
        }
        if (element.Slots == 11) {
          PieChartDataArray = ['11-12 pm', element.Count];
        }
        if (element.Slots == 12) {
          PieChartDataArray = ['12-1 pm', element.Count];
        }
        if (element.Slots == 13) {
          PieChartDataArray = ['1-2 pm', element.Count];
        }
        if (element.Slots == 14) {
          PieChartDataArray = ['2-3 pm', element.Count];
        }
        if (element.Slots == 15) {
          PieChartDataArray = ['3-4 pm', element.Count];
        }
        if (element.Slots == 16) {
          PieChartDataArray = ['4-5 pm', element.Count];
        }
        if (element.Slots == 17) {
          PieChartDataArray = ['5-6 pm', element.Count];
        }
        if (element.Slots == 18) {
          PieChartDataArray = ['6-7 pm', element.Count];
        }
        if (element.Slots == 19) {
          PieChartDataArray = ['7-8 pm', element.Count];
        }
        if (element.Slots == 20) {
          PieChartDataArray = ['8-9 pm', element.Count];
        }
        if (element.Slots == 21) {
          PieChartDataArray = ['9-10 pm', element.Count];
        }
        if (element.Slots == 22) {
          PieChartDataArray = ['10-11 pm', element.Count];
        }
        if (element.Slots == 23) {
          PieChartDataArray = ['11-13 pm', element.Count];
        }

        this.dataArray.push(PieChartDataArray);

      });
      console.log(this.dataArray);

      res.forEach(element => {
        this.columnDataArray.push(['12-1 am', element.Slots == 0 ? element.Count : 0]);
        this.columnDataArray.push(['1-2 am', element.Slots == 1 ? element.Count : 0]);
        this.columnDataArray.push(['2-3 am', element.Slots == 2 ? element.Count : 0]);
        this.columnDataArray.push(['3-4 am', element.Slots == 3 ? element.Count : 0]);
        this.columnDataArray.push(['4-5 am', element.Slots == 4 ? element.Count : 0]);
        this.columnDataArray.push(['5-6 am', element.Slots == 5 ? element.Count : 0]);
        this.columnDataArray.push(['6-7 am', element.Slots == 6 ? element.Count : 0]);
        this.columnDataArray.push(['7-8 am', element.Slots == 7 ? element.Count : 0]);
        this.columnDataArray.push(['8-9 am', element.Slots == 8 ? element.Count : 0]);
        this.columnDataArray.push(['9-10 am', element.Slots == 9 ? element.Count : 0]);
        this.columnDataArray.push(['10-11 am', element.Slots == 10 ? element.Count : 0]);
        this.columnDataArray.push(['11-12 pm', element.Slots == 11 ? element.Count : 0]);
        this.columnDataArray.push(['12-1 pm', element.Slots == 12 ? element.Count : 0]);
        this.columnDataArray.push(['1-2 pm', element.Slots == 13 ? element.Count : 0]);
        this.columnDataArray.push(['2-3 pm', element.Slots == 14 ? element.Count : 0]);
        this.columnDataArray.push(['3-4 pm', element.Slots == 15 ? element.Count : 0]);
        this.columnDataArray.push(['4-5 pm', element.Slots == 16 ? element.Count : 0]);
        this.columnDataArray.push(['5-6 pm', element.Slots == 17 ? element.Count : 0]);
        this.columnDataArray.push(['6-7 pm', element.Slots == 18 ? element.Count : 0]);
        this.columnDataArray.push(['7-8 pm', element.Slots == 19 ? element.Count : 0]);
        this.columnDataArray.push(['8-9 pm', element.Slots == 20 ? element.Count : 0]);
        this.columnDataArray.push(['9-10 pm', element.Slots == 21 ? element.Count : 0]);
        this.columnDataArray.push(['10-11 pm', element.Slots == 22 ? element.Count : 0]);
        this.columnDataArray.push(['11-12 am', element.Slots == 23 ? element.Count : 0]);

        // this.columnDataArray.push(columnChartDataArray); 
        console.log(this.columnDataArray);
      });
      this.pieChartData = {
        chartType: 'PieChart',
        dataTable: this.dataArray,
        options: {
          'legend': 'bottom',
          'pieSliceText': 'value',
          chartArea: { left: 20, top: 0, width: '75%', height: '75%' }
        }
      };

      this.columnChartData = {
        chartType: 'ColumnChart',
        dataTable: this.columnDataArray,
        options: {
          'legend': 'none',
          chartArea: { left: 20, top: 0, width: '75%', height: '75%' }
        }
      };

      this.displayPieChart = true;
      this.displayColumnChart = true;

      //this.columnChartData.dataTable = this.columnDataArray;
      //this.pieChartData.dataTable =this.dataArray;
      //  this.cchartColumn.redraw();
      //  this.cchartPie.redraw();
    });

  }

}



