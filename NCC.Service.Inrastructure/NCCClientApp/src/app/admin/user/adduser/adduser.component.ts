import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from 'src/service/user/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from 'src/service/login.service';
import { Users } from 'src/model/Users/Users';
import { UserRole } from 'src/model/UserRole/UserRole';
import { UserRoleService } from 'src/service/UserRole/user-role.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {

  @ViewChild('custForm') custForm: NgForm;
  actionName: string = '';
  showPasswordSetting:boolean=false;
  updateparamId:number=0;

  pUser : Users;


  UserId: number;
  RoleId: number;
  firstName: string;
  lastName: string;
  email: string;
  Password: string;
  userRoleList:UserRole;
  ContactNumber: string;
  UserRole: number = 0;
  redirectType:number;
  ConfirmPassword:string;
  userrole: string;
  errorMessagedisplay: boolean;
  errorMessage: string;
  constructor( private _userService:UserService, private route: ActivatedRoute,private router: Router,
    private _userRoleService:UserRoleService, private _loginservice:LoginService) { 

    var paramId = this.route.snapshot.params["id"];
    var type = this.route.snapshot.params["type"];
    this.redirectType = type;
    if (paramId > 0) {
      this.updateparamId = paramId;
      this.actionName = 'Update';
      this.FillCustomerForm(paramId);
    } else {
      this.showPasswordSetting=true;
      this.actionName = 'Save';
    }

  }

  ngOnInit() {
    this.getUserRoleList();
    this.getUserRole();
  }
  getUserRole(): any {
    this.userrole =this._loginservice.getuserRole();
  }

  getUserRoleList(){
    var userRoleSearch = {}
    this._userRoleService.getUserRoles(userRoleSearch).subscribe(res=>{
      this.userRoleList = res;
    });
  }
  FillCustomerForm(custId: number) {
    this._userService.getUserByID(custId).subscribe(res => {
      this.pUser = res;
      if (this.pUser != null && this.pUser.UserId > 0) {        
        this.firstName = this.pUser.firstName;
        this.lastName = this.pUser.lastName;
        this.email = this.pUser.email;      
        this.ContactNumber = this.pUser.ContactNumber;   
        this.UserRole = this.pUser.RoleId;      
      }
    });

  }


  AddUsers (){
    if (this.custForm.valid) {
      if(this.custForm.value.Password == this.custForm.value.ConfirmPassword)
      {
        this.CreateUser();
      }else{
        this.custForm.value.ConfirmPassword.error=false;
      }
    }
  }



  CreateUser() {
    var bid: number = 0;
    if (this.actionName == "Update") {
      bid = this.pUser.UserId;
    }    
    var newUser = new Users(bid, 
      this.custForm.value.UserRole!=null && this.custForm.value.UserRole!=undefined ?this.custForm.value.UserRole:this.UserRole,
      this.custForm.value.firstName,
      this.custForm.value.lastName,
      this.custForm.value.email,
      this.custForm.value.Password,
      this.custForm.value.ContactNumber);
    this._userService.createUser(newUser).subscribe(res => {
      this.pUser = res;
      if (this.pUser != null && this.pUser.UserId >= 0) {
        this.errorMessage = "Data Save Successfully";
        this.errorMessagedisplay = true;
        setTimeout(function () {
          this.errorMessagedisplay = false;
        }, 5000)
        if(this.redirectType!=0){
          this.router.navigate(['/admin/userlist']);
          this.pUser = null;
        }
      } 
      if (this.pUser != null && this.pUser.UserId == -100) {
        alert(this.pUser.firstName);
      } 
    });
  }

}
