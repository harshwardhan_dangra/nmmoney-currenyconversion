import { Component, OnInit } from '@angular/core';
import { Users } from 'src/model/Users/Users';
import { UserService } from 'src/service/user/user.service';
import { PageChangedEvent } from 'ngx-bootstrap';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  userlist: Users[];
  returneduserlist:Users[];
  totalItems: number;

  constructor(private _userService:UserService) {

   }

  ngOnInit() {
    this.loadUsers();
  }

  
  loadUsers(){    
    this._userService.getUsersList().subscribe(res => {
      this.userlist = res;     
      this.totalItems = res.length;
      this.returneduserlist = this.userlist.slice(0, 10);
    });
  }

  isDeleteSuccess:boolean;
  DeleteUsers(id: number) {
    if (confirm("Are you sure to delete this record?")) {
      this._userService.deleteUser(id)
        .subscribe(res => {
          this.isDeleteSuccess = res;
          if (this.isDeleteSuccess) {
            this.loadUsers();
            alert("Deleted Successfully");
            
          }
        })
    }
  }

  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.returneduserlist = this.userlist.slice(startItem, endItem);
  }

}
