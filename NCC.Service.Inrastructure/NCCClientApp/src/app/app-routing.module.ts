import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from 'src/guard/auth.guard';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { ScreenComponent } from './admin/screen/screen/screen.component';
import { AdminComponent } from './admin/admin/admin.component';
import { AddscreenComponent } from './admin/screen/addscreen/addscreen.component';
import { CurrencyListComponent } from './admin/currency/currency-list/currency-list.component';
import { ImageGalleryComponent } from './admin/image/image-gallery/image-gallery.component';
import { StatsComponent } from './admin/stats/stats/stats.component';
import { UserListComponent } from './admin/user/user-list/user-list.component';
import { AdduserComponent } from './admin/user/adduser/adduser.component';
import { UserdetailComponent } from './admin/user/userdetail/userdetail.component';
import { HomeComponent } from './home/home.component';
import { AddcurrencyComponent } from './admin/currency/addcurrency/addcurrency.component';
import { CurrencyconvertComponent } from './currencyconvert/currencyconvert.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'currencyconvert', component: CurrencyconvertComponent },
  {
    path: 'admin', canActivate: [AuthGuard], component: AdminComponent
    , children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'screenList', canActivate: [AuthGuard], component: ScreenComponent },
      { path: 'addscreen', canActivate: [AuthGuard], component: AddscreenComponent },
      { path: 'addscreen/:id', canActivate: [AuthGuard], component: AddscreenComponent },
      { path: 'currencyList', canActivate: [AuthGuard], component: CurrencyListComponent },      
      { path: 'addcurrency', canActivate: [AuthGuard], component: AddcurrencyComponent },
      { path: 'addcurrency/:id', canActivate: [AuthGuard], component: AddcurrencyComponent },
      { path: 'imageGallery', canActivate: [AuthGuard], component: ImageGalleryComponent },
      { path: 'stats', canActivate: [AuthGuard], component: StatsComponent },
      { path: 'userlist', canActivate: [AuthGuard], component: UserListComponent },
      { path: 'adduser', canActivate: [AuthGuard], component: AdduserComponent },
      { path: 'adduser/:id', canActivate: [AuthGuard], component: AdduserComponent },
      { path: 'userdetail/:id', canActivate: [AuthGuard], component: UserdetailComponent },
      { path: 'profile/:type/:id', canActivate: [AuthGuard], component: AdduserComponent },  
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
