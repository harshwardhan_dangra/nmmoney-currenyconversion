import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ScreenComponent } from './admin/screen/screen/screen.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { HttpModule } from '@angular/http';
import { AdminComponent } from './admin/admin/admin.component';
import { AddscreenComponent } from './admin/screen/addscreen/addscreen.component';
import { CurrencyListComponent } from './admin/currency/currency-list/currency-list.component';
import { ImageGalleryComponent } from './admin/image/image-gallery/image-gallery.component';
import { StatsComponent } from './admin/stats/stats/stats.component';
import { UserListComponent } from './admin/user/user-list/user-list.component';
import { NgxGalleryModule } from 'ngx-gallery';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { UserdetailComponent } from './admin/user/userdetail/userdetail.component';
import { AdduserComponent } from './admin/user/adduser/adduser.component';
import { HomeComponent } from './home/home.component';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { ModalModule } from 'ngx-bootstrap';
import { AddcurrencyComponent } from './admin/currency/addcurrency/addcurrency.component';
import { CurrencyconvertComponent } from './currencyconvert/currencyconvert.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AveragePipe } from './pipe/Average/average.pipe';
import { ExtractArrayByPropertyPipe } from './pipe/ExtractArrayByProperty/extract-array-by-property.pipe';
import { NgVirtualKeyboardModule } from '@protacon/ng-virtual-keyboard';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { SwiperModule } from 'angular2-useful-swiper';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxImageGalleryModule } from 'ngx-image-gallery';
import {ImageZoomModule} from 'angular2-image-zoom';
import { SafePipe } from './safe.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ScreenComponent,
    DashboardComponent,
    LoginComponent,
    AdminComponent,
    AddscreenComponent,
    CurrencyListComponent,
    ImageGalleryComponent,
    StatsComponent,
    UserListComponent,
    UserdetailComponent,
    AdduserComponent,
    HomeComponent,
    AddcurrencyComponent,
    CurrencyconvertComponent,
    //SlotsComponent,
    AveragePipe,
    ExtractArrayByPropertyPipe,
    SafePipe
  ],
  imports: [
    Ng2GoogleChartsModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ImageZoomModule,
    ReactiveFormsModule,
    HttpModule,
    NgxGalleryModule,
    PaginationModule.forRoot(),
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    NgbModule,
    NgVirtualKeyboardModule,
    BrowserAnimationsModule,
    Ng2SearchPipeModule,
    SwiperModule,
    NgxImageGalleryModule
  ],
  providers: [
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }


