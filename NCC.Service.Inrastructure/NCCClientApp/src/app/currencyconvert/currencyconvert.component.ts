import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { ScreenService } from 'src/service/Screen/screen.service';
import { CurrencyType } from 'src/model/Currency/currency-type';
import { getCurrencySymbol, DOCUMENT, CurrencyPipe, DecimalPipe } from '@angular/common';
import { CurrencyconvertService } from 'src/service/currencyconvert/currencyconvert.service';
import { NgForm } from '@angular/forms';
import { Currencyconvert } from 'src/model/currencyconvert/currencyconvert';
// import { NgxVirtualKeyboardModule }  from 'ngx-virtual-keyboard';

@Component({
  selector: 'app-currencyconvert',
  templateUrl: './currencyconvert.component.html',
  styleUrls: ['./currencyconvert.component.css'],
  providers: [CurrencyPipe, DecimalPipe]
})
export class CurrencyconvertComponent implements OnInit {
  //title:string;
  @ViewChild('custForm') custForm: NgForm;
  private layout: any = 'numeric';
  title = 'Add Screen';
  errorMessagedisplay: string;
  CurrencyCode: string;
  router: Router;
  rate: number;
  amount: string;
  currencyType: CurrencyType[] = null;
  Id: number;
  CurrencyAmount: string;
  CurrencyRate: string;
  CurrencyConvertAmount: string;
  addamountlabel: string = 'Add amount £';

  constructor(@Inject(DOCUMENT) private document: any, private _screenService: ScreenService, _router: Router, private _currencyConvertService: CurrencyconvertService,
    private currencyPipe: CurrencyPipe, private decimalPipe: DecimalPipe) {
    this.router = _router;
  }

  ngOnInit() {
    this.FetchCurrency();
  }

  FetchCurrency() {
    this._screenService.GetCurrency().subscribe(res => {
      this.currencyType = res;
      if (this.currencyType.length != 0) {
        this.CurrencyCode = "USD";
      }
    });
  }

  currencyChange(event:string): any {
    var value = event.split(',').join('')
    this.CurrencyAmount = this.decimalPipe.transform(Number(value),"1.0-0","en-US");
    //this.addamountlabel = "add " + this.currencyType.filter(x => x.CurrencyCode == this.CurrencyCode)[0].CurrencySymbol + " amount";
  }

  ConvertCurrency() {
    var currencyconvert = new Currencyconvert(this.Id, this.custForm.value.CurrencyCode, this.custForm.value.CurrencyAmount,
      this.custForm.value.CurrencyRate, this.custForm.value.CurrencyConvertAmount)
    this._currencyConvertService.FetchCurrency(currencyconvert).subscribe(res => {
      this.rate = res.CurrecnyRate.toFixed(4);
      this.amount = this.currencyPipe.transform(res.ExchangeRate.toFixed(4), this.custForm.value.CurrencyCode);
    });
  }

}
