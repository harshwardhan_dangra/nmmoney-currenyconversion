import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from '../../service/login.service';
import { AppUser } from '../../model/app-user';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  UserId: String;
  userrole: string;
  route: ActivatedRoute;
  router: Router;
  loginService: LoginService;

  constructor(_route: ActivatedRoute, _router: Router, _loginservice: LoginService) {
    this.route = _route
    this.router = _router
    this.loginService = _loginservice

  }

  ngOnInit() {
    var item = localStorage.getItem('loggedInUser');
    var jsonObject: AppUser = JSON.parse(item);
    if (jsonObject != null && jsonObject.Token != null) {
      this.UserId = jsonObject.UserId;
    }
    this.getuserRole();
  }

  getuserRole() {
    this.userrole = this.loginService.getuserRole();
  }
  
  Logout() {
    localStorage.removeItem('loggedInUser');
    this.router.navigate(['/']);
  }

}
