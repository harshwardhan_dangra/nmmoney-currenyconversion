import { Component, OnInit, ViewChild } from '@angular/core';
import { LoginService } from 'src/service/login.service';
import { Router } from '@angular/router';
import { AppUser } from 'src/model/app-user';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginform: FormGroup;
  title = 'Login';
  username:FormControl;
  password:FormControl;
  appUser: AppUser = null;
  Email:string;
  Password:string;
  IsLoggedIn = false;
  errorMessagedisplay: boolean;
  errorMessage: String;
  ngOnInit() : void {    
    this.loginform = new FormGroup({
      username:new FormControl('',Validators.required),
      password:new FormControl('',Validators.required)
    });    
  }


  constructor(private _accountService: LoginService, private router: Router) { }

  Login(){
    if (this.loginform.valid) {   
      this._accountService.userLoginPost(new AppUser('',this.loginform.value.username, this.loginform.value.password,'','','')).subscribe(res => {
        this.appUser = res;    
        if (this.appUser != null && this.appUser.UserId >= '0') {          
          this._accountService.SaveUserToSession(this.appUser);
         this.IsLoggedIn = false;  
         this.router.navigate(['/admin/dashboard']);
        } else {
          this.errorMessagedisplay = true;
          this.errorMessage=this.appUser.message;
         this.IsLoggedIn = true;
        }
      });
    }
  }
}
