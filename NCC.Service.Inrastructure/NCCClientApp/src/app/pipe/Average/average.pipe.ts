import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'average'
})
export class AveragePipe implements PipeTransform {

  transform(arr: any): any {
    if (arr.length == 0) {
      return 0;
    } else {
      arr = arr.filter(x => x != null)
      arr = arr.filter(x => x != 0)
      if (arr.length == 0) {
        return 0;
      }
      return arr.reduce((a, b) => a + b, 0) / arr.length;
    }
  }

}
