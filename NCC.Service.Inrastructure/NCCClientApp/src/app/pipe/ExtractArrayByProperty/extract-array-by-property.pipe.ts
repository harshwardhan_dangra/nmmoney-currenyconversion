import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'extractArrayByProperty'
})

export class ExtractArrayByPropertyPipe implements PipeTransform {

  transform(data: any, property: any): any {
    var newarr = data.map(function (a) {
      return a[property];
    });
    return newarr;
  }

}
