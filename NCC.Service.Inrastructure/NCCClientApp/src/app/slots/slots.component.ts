// import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';
// import { CurrencyconvertService } from 'src/service/currencyconvert/currencyconvert.service';
// import { ExtractArrayByPropertyPipe } from '../pipe/ExtractArrayByProperty/extract-array-by-property.pipe';
// import { AveragePipe } from '../pipe/Average/average.pipe';

// @Component({
//   selector: 'app-slots',
//   templateUrl: './slots.component.html',
//   styleUrls: ['./slots.component.css'],
//   providers:[AveragePipe,ExtractArrayByPropertyPipe]
// })
// export class SlotsComponent implements OnInit {

//   router:Router;
//   averageArray = [];
//   dataArray = [['Slots', 'Count']];
//   columnDataArray = [['Slots', 'Count']];

//   constructor(_router: Router,private _slotService: CurrencyconvertService,private averagePipe:AveragePipe,private extractarraybyproperty:ExtractArrayByPropertyPipe) { 
//     this.router = _router;

//   }

//   ngOnInit() {
//     this.FetchSlots();
//   }

//   FetchSlots()
//   {          
//     this._slotService.GetSlots().subscribe(res => {
      
//       var valAverage =  this.averagePipe.transform(this.extractarraybyproperty.transform(res, "Count"))
      
//       var data = res.filter(x=>x.Count>=valAverage);       
//       var PieChartDataArray= [];
//       var columnChartDataArray = [];
//       data.forEach(element => { 
//           if(element.Slots == 0)
//           {
//             PieChartDataArray= ['12-1 am',element.Count];               
//           }       
//           if(element.Slots == 1)
//           {
//             PieChartDataArray= ['1-2 am',element.Count];               
//           }  
//           if(element.Slots == 2)
//           {
//             PieChartDataArray= ['2-3 am',element.Count];               
//           }  
//           if(element.Slots == 3)
//           {
//             PieChartDataArray= ['3-4 am',element.Count];               
//           }  
//           if(element.Slots == 4)
//           {
//             PieChartDataArray= ['4-5 am',element.Count];               
//           }  
//           if(element.Slots == 5)
//           {
//             PieChartDataArray= ['5-6 am',element.Count];               
//           }  
//           if(element.Slots == 6)
//           {
//             PieChartDataArray= ['6-7 am',element.Count];               
//           }  
//           if(element.Slots == 7)
//           {
//             PieChartDataArray= ['7-8 am',element.Count];               
//           }  
//           if(element.Slots == 8)
//           {
//             PieChartDataArray= ['8-9 am',element.Count];               
//           }  
//           if(element.Slots == 9)
//           {
//             PieChartDataArray= ['9-10 am',element.Count];               
//           }  
//           if(element.Slots == 10)
//           {
//             PieChartDataArray= ['10-11 am',element.Count];               
//           }  
//           if(element.Slots == 11)
//           {
//             PieChartDataArray= ['11-12 pm',element.Count];               
//           }  
//           if(element.Slots == 12)
//           {
//             PieChartDataArray= ['12-1 pm',element.Count];               
//           }  
//           if(element.Slots == 13)
//           {
//             PieChartDataArray= ['1-2 pm',element.Count];               
//           }  
//           if(element.Slots == 14)
//           {
//             PieChartDataArray= ['2-3 pm',element.Count];               
//           }  
//           if(element.Slots == 15)
//           {
//             PieChartDataArray= ['3-4 pm',element.Count];               
//           }  
//           if(element.Slots == 16)
//           {
//             PieChartDataArray= ['4-5 pm',element.Count];               
//           }  
//           if(element.Slots == 17)
//           {
//             PieChartDataArray= ['5-6 pm',element.Count];               
//           }  
//           if(element.Slots == 18)
//           {
//             PieChartDataArray= ['6-7 pm',element.Count];               
//           }  
//           if(element.Slots == 19)
//           {
//             PieChartDataArray= ['7-8 pm',element.Count];               
//           }  
//           if(element.Slots == 20)
//           {
//             PieChartDataArray= ['8-9 pm',element.Count];               
//           }  
//           if(element.Slots == 21)
//           {
//             PieChartDataArray= ['9-10 pm',element.Count];               
//           }  
//           if(element.Slots == 22)
//           {
//             PieChartDataArray= ['10-11 pm',element.Count];               
//           }  
//           if(element.Slots == 23)
//           {
//             PieChartDataArray= ['11-13 pm',element.Count];               
//           }           

//           this.dataArray.push(PieChartDataArray); 
//       });
//        this.pieChartData.dataTable =this.dataArray;
       
//        res.forEach(element => { 
//         if(element.Slots == 0)
//         {
//             columnChartDataArray= ['12-1 am',element.Count];               
//         }       
//         if(element.Slots == 1)
//         {
//             columnChartDataArray= ['1-2 am',element.Count];               
//         }  
//         if(element.Slots == 2)
//         {
//             columnChartDataArray= ['2-3 am',element.Count];               
//         }  
//         if(element.Slots == 3)
//         {
//             columnChartDataArray= ['3-4 am',element.Count];               
//         }  
//         if(element.Slots == 4)
//         {
//             columnChartDataArray= ['4-5 am',element.Count];               
//         }  
//         if(element.Slots == 5)
//         {
//             columnChartDataArray= ['5-6 am',element.Count];               
//         }  
//         if(element.Slots == 6)
//         {
//             columnChartDataArray= ['6-7 am',element.Count];               
//         }  
//         if(element.Slots == 7)
//         {
//             columnChartDataArray= ['7-8 am',element.Count];               
//         }  
//         if(element.Slots == 8)
//         {
//             columnChartDataArray= ['8-9 am',element.Count];               
//         }  
//         if(element.Slots == 9)
//         {
//             columnChartDataArray= ['9-10 am',element.Count];               
//         }  
//         if(element.Slots == 10)
//         {
//             columnChartDataArray= ['10-11 am',element.Count];               
//         }  
//         if(element.Slots == 11)
//         {
//             columnChartDataArray= ['11-12 pm',element.Count];               
//         }  
//         if(element.Slots == 12)
//         {
//             columnChartDataArray= ['12-1 pm',element.Count];               
//         }  
//         if(element.Slots == 13)
//         {
//             columnChartDataArray= ['1-2 pm',element.Count];               
//         }  
//         if(element.Slots == 14)
//         {
//             columnChartDataArray= ['2-3 pm',element.Count];               
//         }  
//         if(element.Slots == 15)
//         {
//             columnChartDataArray= ['3-4 pm',element.Count];               
//         }  
//         if(element.Slots == 16)
//         {
//             columnChartDataArray= ['4-5 pm',element.Count];               
//         }  
//         if(element.Slots == 17)
//         {
//             columnChartDataArray= ['5-6 pm',element.Count];               
//         }  
//         if(element.Slots == 18)
//         {
//             columnChartDataArray= ['6-7 pm',element.Count];               
//         }  
//         if(element.Slots == 19)
//         {
//             columnChartDataArray= ['7-8 pm',element.Count];               
//         }  
//         if(element.Slots == 20)
//         {
//             columnChartDataArray= ['8-9 pm',element.Count];               
//         }  
//         if(element.Slots == 21)
//         {
//             columnChartDataArray= ['9-10 pm',element.Count];               
//         }  
//         if(element.Slots == 22)
//         {
//             columnChartDataArray= ['10-11 pm',element.Count];               
//         }  
//         if(element.Slots == 23)
//         {
//             columnChartDataArray= ['11-13 pm',element.Count];               
//         }           

//         this.columnDataArray.push(columnChartDataArray); 
//     });
       
//        this.columnChartData.dataTable = this.columnDataArray;
    
//     });
   
//   }

  
//   pieChartData =  {
//     chartType: 'PieChart',
//     dataTable: [],
//     options: {'legend': 'bottom',
//             chartArea:{left:20,top:0,width:'75%',height:'75%'}
//              }
//   };

//   columnChartData =  {
//     chartType: 'ColumnChart',
//     dataTable: [],
//     options: {'legend': 'none',
//             chartArea:{left:20,top:0,width:'75%',height:'75%'}
//              }
//   };
  
// }






