import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from 'src/service/login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService: LoginService, private route: Router) {
  }
  /**
   *
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
      : Observable<boolean> | Promise<boolean> | boolean {
      return this.authService.IsAuthorized().then(
          (auth: boolean) => {
              if (auth) {
                  return true;
              } else {
                  this.route.navigate(['']);
              }
          }
      );
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
      : Observable<boolean> | Promise<boolean> | boolean {
      return this.canActivate(route, state);
  }
}
