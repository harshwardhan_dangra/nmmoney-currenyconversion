export class Currency {
    Id:number;
    CurrencyName:string;
    CurrencyDescription:string;
    CurrencyCode:string;
    CurrencySymbol:string;
    ImageId:number;
    ImageUrl:string;
    constructor(Id:number,CurrencyName:string,CurrencyDescription:string,
        CurrencyCode:string,CurrencySymbol:string,ImageId:number,ImageUrl:string=null)
    {
        this.Id = Id;
        this.CurrencyCode = CurrencyCode;
        this.CurrencyDescription = CurrencyDescription;
        this.CurrencyName = CurrencyName;
        this.CurrencySymbol = CurrencySymbol;
        this.ImageId = ImageId;
        this.ImageUrl = ImageUrl;
    }
}