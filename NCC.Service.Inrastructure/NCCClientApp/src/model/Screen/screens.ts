

export class Screens {
    Id:Number;
    ScreenName:string;
    ScreenType:string;
    FeatureColour:string;
    Title:string;
    SubTitle:string;
    CurrencyCode:number;
    ScreenOrder:number;
    BackgroundImage:string;
    HtmlCode:string;
    CurrencyFirst:number;
    CurrencyTwo:number;
    CurrencyThree:number;
    CurrencyFour:number;
    CurrencyFive:number;
    Image:string;
    ScreenFalg:number;
    IsActive:boolean;
    CurrencySymbol:string;
    CurrencyFirstSymbol:string;	
    CurrencyTwoSymbol:string;	
    CurrencyThreeSymbol:string;	
    CurrencyFourSymbol:string;	
    CurrencyFiveSymbol:string; 
    BackgroundImageUrl:string=null;
    ImageUrl:string = null;
    StartDate: string;
    EndDate: string;  


    constructor(Id:Number,ScreenName:string,ScreenType:string,FeatureColour:string,Title:string,SubTitle:string,
        CurrencyCode:number,ScreenOrder:number,BackgroundImage:string,HtmlCode:string,CurrencyFirst:number,
        CurrencyTwo:number,CurrencyThree:number,CurrencyFour:number,CurrencyFive:number,Image:string,ScreenFlag:number
        ,BackgroundImageUrl:string,ImageUrl:string,StartDate:string,EndDate:string
        )
    {
        this.BackgroundImage=BackgroundImage,
        this.CurrencyCode = CurrencyCode,
        this.CurrencyFirst = CurrencyFirst,
        this.CurrencyFive = CurrencyFive,
        this.CurrencyFour = CurrencyFour,
        this.CurrencyThree = CurrencyThree,
        this.CurrencyTwo = CurrencyTwo,
        this.FeatureColour = FeatureColour,
        this.HtmlCode = HtmlCode,
        this.Id = Id,
        this.Image = Image,
        this.ScreenName = ScreenName,
        this.ScreenOrder = ScreenOrder,
        this.ScreenType= ScreenType,
        this.SubTitle = SubTitle,
        this.Title = Title   ,
        this.BackgroundImageUrl = BackgroundImageUrl,
        this.ImageUrl = ImageUrl,
        this.StartDate = StartDate,
        this.EndDate = EndDate     
    }
}
