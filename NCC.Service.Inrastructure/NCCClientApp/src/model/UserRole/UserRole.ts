export class UserRole {
    UserRoleId: number;
    Role: string

    constructor(UserRoleId: number, Role: string) {
        this.UserRoleId = UserRoleId;
        this.Role = Role;
    }
}
