export class Users {
    UserId: number;

    RoleId: number;
    firstName: string;
    lastName: string;
    email: string;
    Password: string;

    fullName: string;
    Role: string;
    ContactNumber: string;


    constructor(uId:number,roleid:number,fname:string,lname:string,email:string,password:string,ContactNumber:string){

        this.UserId=uId;
        this.firstName=fname;
        this.lastName=lname;
        this.email=email;
        this.RoleId=roleid;
        this.Password=password;   
        this.ContactNumber = ContactNumber;     
    }
}