export class AppUser {
    UserId: String;
    userType: String;
    Token: String;
    FirstName: String;
    LastName: String;
    Password: String;
    Email: String;
  message: String;
    constructor(userid: string, email: string, password: string, FirstName: string, LastName: string, userType: string) {
        this.UserId = userid;
        this.Email = email;
        this.Password = password;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.userType = userType;
    }
}
