export class Currencyconvert {
    Id:number;
    CurrencyCode:string;
    CurrencyAmount:string;
    CurrencyRate:string;
    CurrencyConvertAmount:string;    
    constructor(Id:number,CurrencyCode:string,CurrencyAmount:string,
        CurrencyRate:string,CurrencyConvertAmount:string)
    {
        this.Id = Id;
        this.CurrencyCode = CurrencyCode;
        this.CurrencyAmount = CurrencyAmount;
        this.CurrencyRate = CurrencyRate;
        this.CurrencyConvertAmount = CurrencyConvertAmount;        
    }
}
