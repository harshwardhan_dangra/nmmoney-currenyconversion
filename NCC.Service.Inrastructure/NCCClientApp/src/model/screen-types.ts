export class ScreenTypes {
    Id:number;
    ScreenType:string;
    ScreenDescription:string;
    IsActive:boolean;
}
