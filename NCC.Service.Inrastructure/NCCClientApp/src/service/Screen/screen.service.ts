import { Injectable } from '@angular/core';
import { CommonService } from '../common/common.service';
import { Observable } from 'rxjs';
import { AppUser } from 'src/model/app-user';
import { Http } from '@angular/http';
import { map, catchError } from 'rxjs/operators';
import { Screens } from 'src/model/Screen/screens';
import { ScreenTypes } from 'src/model/screen-types';
import { CurrencyType } from 'src/model/Currency/currency-type';


@Injectable({
  providedIn: 'root'
})
export class ScreenService {
  constructor(private _http: Http,private _commonService:CommonService) { }

  GetActiveScreens(): any {
    return this._commonService.get('GetActiveScreen');   
  }

  CheckLoggedIn(): any {
    return this._commonService.getToken();
  } 

  public _appUser: AppUser = null;
  IsAuthenticated = false;  
  
 
  GetScreens():Observable<Screens[]>
  {
    return this._commonService.get('GetScreen');      
  }
  DeleteScreen(Id:number):Observable<any>{
   
    return this._commonService.delete('DeleteScreen/'+ Id)
  }

  GetScreenTypes():Observable<ScreenTypes[]>
  {
    return this._commonService.get('GetScreenType');
  }
  GetCurrency():Observable<CurrencyType[]>
  {
    return this._commonService.get('GetCurrency');
  }

  AddScreen(screenBO:Screens):Observable<number>{  
  return this._commonService.post('SaveScreen',screenBO);
  }

  GetScreenRecordById(id:number):Observable<any>
  {
    return this._commonService.get('GetSingleScreen/'+id);
  }
}