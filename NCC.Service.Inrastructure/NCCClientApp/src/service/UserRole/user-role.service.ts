import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { CommonService } from '../common/common.service';
import { UserRole } from 'src/model/UserRole/UserRole';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserRoleService {

  constructor(private _http: Http,private _commonService:CommonService) {
  }


  
  getUserRoles(userRoleSearch:any): Observable<UserRole> {
    return this._commonService.get('userrole/list?'+this._commonService.buildQueryString(userRoleSearch));
  }
}
