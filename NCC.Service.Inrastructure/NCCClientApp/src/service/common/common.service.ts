import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AppUser } from 'src/model/app-user';
import { Http, Response, Headers, RequestOptions, ResponseContentType } from '@angular/http';
import { map, catchError } from 'rxjs/operators';
import { DOCUMENT } from '@angular/platform-browser';
import { Globalconfig } from 'src/global/globalconfig';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  public baseUrl: string = "";
  public accessToken: string = '';

  constructor(private _http: Http,private router: Router,@Inject(DOCUMENT) private document: any) {
    if(document.location.hostname=="localhost"){
      this.baseUrl = Globalconfig.LocalApiUrl +'/';
    }else{
      this.baseUrl = Globalconfig.ApiUrl + '/';
    }
  }

  getHeader(): RequestOptions {
    var item = localStorage.getItem('loggedInUser');
    var jsonObject: AppUser = JSON.parse(item);
    if (jsonObject != null && jsonObject.Token != null) {
      this.accessToken = jsonObject.Token as string;
    }
    let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
    cpHeaders.append('X-AuthToken', this.accessToken);
    let options = new RequestOptions({ headers: cpHeaders });
    return options;
  }

  getPostFileHeader(): RequestOptions {
    var item = localStorage.getItem('loggedInUser');
    var jsonObject: AppUser = JSON.parse(item);
    if (jsonObject != null && jsonObject.Token != null) {
      this.accessToken = jsonObject.Token as string;
    }
    let cpHeaders = new Headers({ "Mime-Type": "multipart/form-data" });
    cpHeaders.append('X-AuthToken', this.accessToken);
    let options = new RequestOptions({ headers: cpHeaders });
    return options;
  }

  getToken(): string {
    var item = localStorage.getItem('loggedInUser');
    var jsonObject: AppUser = JSON.parse(item);
    if (jsonObject != null && jsonObject.Token != null) {
      return jsonObject.Token as string;
    }
    return '';
  }

  
  getuserRole(): string {
    var item = localStorage.getItem('loggedInUser');
    var jsonObject: AppUser = JSON.parse(item);
    if (jsonObject != null && jsonObject.Token != null) {
      return jsonObject.userType as string;
    }
    return '';
  }

  
  postFile(url: string, formData: FormData): any {
    var response = this._http.post(this.baseUrl + url, formData, this.getPostFileHeader())
    .pipe(map((response: any) => response.json()),
    catchError(this.handleError('getData')));
    return response;
  }

  post(url: string, model: any): Observable<any> {
    var response = this._http.post(this.baseUrl + url, model, this.getHeader())
    .pipe(map((response: any) => response.json()),
    catchError(this.handleError('getData')));
    return response;
  }


  get(url: string): Observable<any> {    
    var response = this._http.get(this.baseUrl + url, this.getHeader()).pipe(map(data => <any>data.json()),
    catchError(this.handleError('getData')));  
    return response;
  }

  

  getwithParams(url: string,model:any): Observable<any> {
    var response = this._http.get(this.baseUrl + url, this.getHeader()).pipe(map(data => <any>data.json()),
    catchError(this.handleError('getData')));
    return response;
  }

  delete(url: string): Observable<boolean> {
    var response = this._http.delete(this.baseUrl + url, this.getHeader()).pipe(map((response: Response) => response.json()),
    catchError(this.handleError('getData'))    
    );
    return response;
  }
  downloadFile(url:string): Observable<Blob> {
    let options = this.getHeader();
    options.responseType= ResponseContentType.Blob;
    var response = this._http.get(this.baseUrl + url, options).pipe(map(res => res.blob()),
    catchError(this.handleError('getData')));  
    return response;
  }
  buildQueryString = function ($params) {


    var queryParams = [];
  
    for (var k in $params) {
        if ($params.hasOwnProperty(k)) {
            queryParams.push(encodeURIComponent(k) + "=" + encodeURIComponent($params[k]));
            //queryParams.push(k + "=" + $params[k]);
        }
    }
  
    return queryParams.join("&");
  }


  private handleError(operation: String) {
    return (err: any) => {
        let errMsg = `error in`;       
        if(err.status==401){
            this.router.navigate(['/admin']);
        }
        if(err.status==400){
          alert('User name or password is incorrect, please enter valid credentials!');
      }
        return Observable.throw(errMsg);
    }
  }
}
