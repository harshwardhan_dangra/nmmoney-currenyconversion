import { Injectable } from '@angular/core';
import { CommonService } from '../common/common.service';
import { Http } from '@angular/http';
import { AppUser } from 'src/model/app-user';
import { Observable } from 'rxjs';
import { Currency } from 'src/model/Currency/Currency';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {

  CheckLoggedIn(): any {
    return this._commonService.getToken();
  } 
  public _appUser: AppUser = null;
  IsAuthenticated = false;  
  constructor(private _http: Http,private _commonService:CommonService) { }

  GetCurrencies():Observable<Currency[]>
  {
    return this._commonService.get('GetCurrency');      
  }
  DeleteCurrency(Id:number):Observable<any>{
   
    return this._commonService.delete('DeleteCurrency/'+ Id)
  }
  AddCurrency(currency:Currency):any{
    return this._commonService.post('SaveCurrency',currency);
  }
  GetCurrencyRecordById(id:number):Observable<Currency>{
    return this._commonService.get('GetSingleCurrency/'+ id)
  }
}
