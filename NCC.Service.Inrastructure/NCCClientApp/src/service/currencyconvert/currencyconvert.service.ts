import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { CommonService } from '../common/common.service';
import { Currencyconvert } from 'src/model/currencyconvert/currencyconvert';

@Injectable({
  providedIn: 'root'
})
export class CurrencyconvertService {

  constructor(private _http: Http,private _commonService:CommonService) { }

  FetchCurrency(currencyconvert:Currencyconvert):any
  {
    return this._commonService.post('ConvertCurrency',currencyconvert);      
  }

  GetSlots():any{
    return this._commonService.get('GetSlots');      
  }
  GetExcelNamePie():any{
    return this._commonService.get('DownloadExcelColumnPie');
   }
  GetExcelName():any{
    return this._commonService.get('DownloadExcelColumn');
   }
   GenerateExcel(fileName:string):any
   {
     return this._commonService.downloadFile('downloadexcel?fileName='+fileName);
   }
}
