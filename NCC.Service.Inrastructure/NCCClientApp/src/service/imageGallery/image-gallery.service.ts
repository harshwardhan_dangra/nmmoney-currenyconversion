import { Injectable } from '@angular/core';
import { CommonService } from '../common/common.service';

@Injectable({
  providedIn: 'root'
})
export class ImageGalleryService {
 

  constructor(private _commonService:CommonService) {
    
   }

  GetAllImages(): any {
    return this._commonService.get('GetImages');
  }
  DeleteImages(DeleteImage:string):any{
    return this._commonService.delete('DeleteImage/'+DeleteImage);
  }
}
