import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { CommonService } from '../common/common.service';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(private _http: Http,private _commonService:CommonService) { }
  
  uploadImage(formData:FormData): any {
    return this._commonService.postFile('UploadImage', formData);
  }
}
