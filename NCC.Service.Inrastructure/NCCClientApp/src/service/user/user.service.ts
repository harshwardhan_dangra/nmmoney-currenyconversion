import { Injectable } from '@angular/core';
import { CommonService } from '../common/common.service';
import { Observable } from 'rxjs';
import { Users } from 'src/model/Users/Users';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private _commonService:CommonService) {
  }

  getUsersList():Observable<Users[]>{
    return this._commonService.get('users/list');
  }

  createUser(user: Users): Observable<Users> {    
    return this._commonService.post('users/register', user);      
  }

  getUserByID(id:number):Observable<Users>{
    return this._commonService.get('user/'+id);
  }

  deleteUser(id: number): Observable<boolean> {    
    return this._commonService.delete('users/'+ id);                  
  }
}
