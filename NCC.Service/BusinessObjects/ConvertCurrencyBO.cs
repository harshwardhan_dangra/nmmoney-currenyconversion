﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.BusinessObjects
{
    [DataContract]
    public class ConvertCurrencyBO
    {
        [DataMember]
        public decimal CurrecnyRate { get; set; }

        [DataMember]
        public decimal ExchangeRate { get; set; }

        [DataMember]
        public string CurrencyAmount { get; set; }

        [DataMember]
        public string CurrencyCode { get; set; }

        [DataMember]
        public string CurrencyRate { get; set; }

        [DataMember]
        public string CurrencyConvertAmount { get; set; }

        [DataMember]
        public string Date { get; set; }

        [DataMember]
        public string Time { get; set; }

        [DataMember]
        public int Slots { get; set; }

        [DataMember]
        public int Count { get; set; }
    }
}
