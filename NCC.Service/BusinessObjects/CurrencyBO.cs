﻿using NCC.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.BusinessObjects
{
    [DataContract]
    public class CurrencyBO:Currency
    {
        [DataMember(Name = "ImageUrl")]
        public string ImageUrl { get; set; }
    }
}
