﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.Constants
{
    public enum Slots
    {
        first = 1,          //Start 00:00 To 01:00 am 1
        second = 2,         //Start 01:00 To 02:00 am 2
        third = 3,          //Start 02:00 To 03:00 am 3
        four = 4,           //Start 03:00 To 04:00 am 4
        five = 5,           //Start 04:00 To 05:00 am 5
        six  = 6,           //Start 05:00 To 06:00 am 6
        seven = 7,          //Start 06:00 To 07:00 am 7
        eight = 8,          //Start 07:00 To 08:00 am 8
        nine  = 9,          //Start 08:00 To 09:00 am 9
        ten   = 10,         //Start 09:00 To 10:00 am 10
        eleven = 11,        //Start 10:00 To 11:00 am 11
        twelve = 12,        //Start 11:00 To 12:00 pm 12
        thirteen= 13,       //Start 12:00 To 01:00 pm 13
        fourteen = 14,      //Start 01:00 To 02:00 pm 14
        fifteen = 15,       //Start 02:00 To 03:00 pm 15
        sixteen = 16,       //Start 03:00 To 04:00 pm 16
        seventeen = 17,     //Start 04:00 To 05:00 pm 17 
        eighteen = 18,      //Start 05:00 To 06:00 pm 18
        ninteen  = 19,      //Start 06:00 To 07:00 pm 19
        twenty   = 20,      //Start 07:00 To 08:00 pm 20
        twentyone = 21,     //Start 08:00 To 09:00 pm 21 
        twentytwo = 22,     //Start 09:00 To 10:00 pm 22
        twentythree = 23,   //Start 10:00 To 11:00 pm 23
        twentyfour = 24,    //Start 11:00 To 00:00 am 24

    }
}
