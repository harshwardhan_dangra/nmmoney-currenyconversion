﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NCC.Core.Data;
using NCC.Core.Utilities;
using NCC.Core.Utilities.Extensions;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;

namespace NCC.Service.Data
{
    public class BranchRepository : BaseRepository<Branch>,IBranchRepository
    {
        public BranchRepository(IDbContext dbContext) : base(dbContext)
        {

        }
        private const string GetByIdStoredProcName = "USP_GetBranch_ById";
        private const string GetFilteredStoredProcName = "USP_GetAllBranchData";
        private const string UpSertStoredProcName = "USP_Branch_Upsert";
        private const string DeleteStoredProcName = "USP_Branch_DeleteById";
        protected override string GetByIdStoredProcedureName { get { return BranchRepository.GetByIdStoredProcName; } }

        protected override string GetFilteredStoredProcedureName { get { return BranchRepository.GetFilteredStoredProcName; } }

        protected override string UpSertStoredProcedureName { get { return BranchRepository.UpSertStoredProcName; } }

        protected override string DeleteStoredProcedureName { get { return BranchRepository.DeleteStoredProcName; } }

        public IList<Branch> GetAllBranch()
        {
            List<Branch> BranchBOList = new List<Branch>();
            Branch branch;

            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_GetAll_Branch", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            branch = new Branch();
                            DataReaderExtensions.FillEntity<Branch>(reader, branch);
                            BranchBOList.Add(branch);
                        }
                    }
                }
            }
            return BranchBOList;
        }

        public Branch GetSingleBranchRecord(int id)
        {
            Branch branch = new Branch();
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_GetBranch_ById", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@Id", id); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Int;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            branch = new Branch();
                            DataReaderExtensions.FillEntity<Branch>(reader, branch);

                        }
                    }
                }
            }
            return branch;
        }

        public Branch Insert(Branch branch)
        {
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_Branch_Upsert", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.AddWithValue("@Id", branch.Id);
                    sqlCmd.Parameters.AddWithValue("@BranchName", branch.BranchName);
                    sqlCmd.Parameters.AddWithValue("@BranchCode", branch.BranchCode);
                    sqlCmd.Parameters.AddWithValue("@ScreenNumber", branch.ScreenNumber);
                    sqlCmd.Parameters.AddWithValue("@Location", branch.Location);
                    branch.Id = database.ExecuteNonQuery(sqlCmd);
                }
            }
            return branch;
        }
    }
}
