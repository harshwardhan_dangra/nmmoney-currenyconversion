﻿using NCC.Core.Data;
using NCC.Core.Utilities;
using NCC.Core.Utilities.Extensions;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.Data
{
    public class CurrencyRepository : BaseRepository<Currency>, ICurrencyRepository
    {
        private const string GetByIdStoredProcName = "USP_GetCurrency_ById";
        private const string GetFilteredStoredProcName = "USP_GetAllCurrencyData";
        private const string UpSertStoredProcName = "USP_Currency_Upsert";
        private const string DeleteStoredProcName = "USP_Currency_DeleteById";
        protected override string GetByIdStoredProcedureName { get { return CurrencyRepository.GetByIdStoredProcName; } }

        protected override string GetFilteredStoredProcedureName { get { return CurrencyRepository.GetFilteredStoredProcName; } }

        protected override string UpSertStoredProcedureName { get { return CurrencyRepository.UpSertStoredProcName; } }

        protected override string DeleteStoredProcedureName { get { return CurrencyRepository.DeleteStoredProcName; } }

        public CurrencyRepository(IDbContext dbContext) : base(dbContext)
        {

        }

        public IList<CurrencyBO> GetAllCurrencies()
        {
            List<CurrencyBO> currencyBOList = new List<CurrencyBO>();
            CurrencyBO screen;

            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_GetAll_Currencies", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            screen = new CurrencyBO();
                            DataReaderExtensions.FillEntity<CurrencyBO>(reader, screen);
                            currencyBOList.Add(screen);
                        }
                    }
                }
            }
            return currencyBOList;
        }

        public CurrencyBO GetSingleCurrencyRecord(int id)
        {
            CurrencyBO screen = new CurrencyBO();
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_GetCurrency_ById", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter tvpParam = sqlCmd.Parameters.AddWithValue("@Id", id); //Needed TVP
                    tvpParam.SqlDbType = SqlDbType.Int;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                            screen = new CurrencyBO();
                            DataReaderExtensions.FillEntity<CurrencyBO>(reader, screen);

                        }
                    }
                }
            }
            return screen;
        }
        public void UpsertSlotRecord(string currentDate, int currentTimeSlot, string currentTime)
        {
            //CurrencyBO screen = new CurrencyBO();
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_CurrencyRateUpsert", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter tvpDateParam = sqlCmd.Parameters.AddWithValue("@Date", currentDate); //Needed TVP
                    SqlParameter tvpSlotParam = sqlCmd.Parameters.AddWithValue("@Slot", currentTimeSlot); //Needed TVP
                    SqlParameter tvpTimeParam = sqlCmd.Parameters.AddWithValue("@Time", currentTime); //Needed TVP
                    tvpDateParam.SqlDbType = SqlDbType.NVarChar;
                    tvpSlotParam.SqlDbType = SqlDbType.Int;
                    tvpTimeParam.SqlDbType = SqlDbType.NVarChar;

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        //while (reader.Read())
                        //{
                        //    screen = new CurrencyBO();
                        //    DataReaderExtensions.FillEntity<CurrencyBO>(reader, screen);

                        //}
                    }
                }
            }
        }

        public List<ConvertCurrencyBO> GetAllSlots(string currentDate)
        {
            List<ConvertCurrencyBO> screen = new List<ConvertCurrencyBO>();
            using (SqlConnection sqlCon = new SqlConnection(database.ConnectionString))
            {
                sqlCon.Open();
                using (SqlCommand sqlCmd = new SqlCommand("USP_GetAll_Slots", sqlCon))
                {
                    sqlCmd.CommandTimeout = ApplicationConfig.CommandTimeout;
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter tvpDateParam = sqlCmd.Parameters.AddWithValue("@Date", currentDate); //Needed TVP                   
                    tvpDateParam.SqlDbType = SqlDbType.NVarChar;                    

                    using (var reader = database.ExecuteReader(sqlCmd))
                    {
                        while (reader.Read())
                        {
                           var screens = new ConvertCurrencyBO();
                            DataReaderExtensions.FillEntity<ConvertCurrencyBO>(reader, screens);
                            screen.Add(screens);
                        }
                    }
                }
            }
            return screen;
        }
    }
}
