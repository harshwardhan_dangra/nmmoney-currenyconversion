﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NCC.Core.Data;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;

namespace NCC.Service.Data
{
    public interface IBranchRepository:IRepository<Branch>
    {
        IList<Branch> GetAllBranch();
        BranchBO GetSingleBranchRecord(int id);
        Branch Insert(Branch branch);
    }
}
