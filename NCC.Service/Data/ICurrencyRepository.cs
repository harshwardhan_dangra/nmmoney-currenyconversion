﻿using NCC.Core.Data;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.Data
{
    public interface ICurrencyRepository : IRepository<Currency>
    {
        IList<CurrencyBO> GetAllCurrencies();
        CurrencyBO GetSingleCurrencyRecord(int id);
        void UpsertSlotRecord(string currentDate, int currentTimeSlot, string currentTime);
        List<ConvertCurrencyBO> GetAllSlots(string currentDate);
    }
}
