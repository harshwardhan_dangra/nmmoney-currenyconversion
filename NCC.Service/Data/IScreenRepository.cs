﻿using NCC.Core.Data;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.Data
{
    public interface IScreenRepository: IRepository<Screens>
    {
        IList<ScreenBO> GetAllScreensData();
        IList<ScreenTypesBO> GetAllScreenTypeData();
        ScreenBO GetSingleScreenRecord(int id);
        IList<ScreenBO> GetActiveScreenData();
    }
}
