﻿using NCC.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.Entities
{
  
        [DataContract]
        public class Branch : BaseEntity
        {
            public Branch() : base("Id")
            {

            }

            [DataMember(Name = "Id")]
            public int Id { get; set; }

            [DataMember(Name = "BranchName")]
            public string BranchName { get; set; }

            [DataMember(Name = "BranchCode")]
            public string BranchCode { get; set; }

            [DataMember(Name = "ScreenNumber")]
            public long ScreenNumber { get; set; }

            [DataMember(Name = "Location")]
            public string Location { get; set; }

        }
    
}
