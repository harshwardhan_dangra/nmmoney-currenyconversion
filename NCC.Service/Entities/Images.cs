﻿using NCC.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.Entities
{
    [DataContract]
    public class Images : BaseEntity
    {
        [DataMember(Name = "Id")]
        public long Id { get; set; }

        [DataMember(Name = "OrignalName")]
        public string OrignalName { get; set; }

        [DataMember(Name = "FileName")]
        public string FileName { get; set; }

        public Images() : base("Id")
        {
        }
    }
}
