﻿using NCC.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.Entities
{
    [DataContract]
    public class Screens : BaseEntity
    {
        public Screens() : base("Id")
        {

        }
        [DataMember(Name = "Id")]
        public int Id { get; set; }
        [DataMember(Name = "ScreenName")]
        public string ScreenName { get; set; }
        [DataMember(Name = "ScreenType")]
        public string ScreenType { get; set; }
        [DataMember(Name = "FeatureColour")]
        public string FeatureColour { get; set; }
        [DataMember(Name = "Title")]
        public string Title { get; set; }
        [DataMember(Name = "SubTitle")]
        public string SubTitle { get; set; }
        [DataMember(Name = "CurrencyCode")]
        public int CurrencyCode { get; set; }
        [DataMember(Name = "CurrencySymbol")]
        public string CurrencySymbol { get; set; }
        [DataMember(Name = "ScreenOrder")]
        public string ScreenOrder { get; set; }
        [DataMember(Name = "BackgroundImage")]
        public string BackgroundImage { get; set; }
        [DataMember(Name = "HtmlCode")]
        public string HtmlCode { get; set; }
        [DataMember(Name = "CurrencyFirst")]
        public int CurrencyFirst { get; set; }
        [DataMember(Name = "CurrencyTwo")]
        public int CurrencyTwo { get; set; }
        [DataMember(Name = "CurrencyThree")]
        public int CurrencyThree { get; set; }
        [DataMember(Name = "CurrencyFour")]
        public int CurrencyFour { get; set; }
        [DataMember(Name = "CurrencyFive")]
        public int CurrencyFive { get; set; }
        [DataMember(Name = "CurrencyFirstSymbol")]
        public string CurrencyFirstSymbol { get; set; }
        [DataMember(Name = "CurrencyTwoSymbol")]
        public string CurrencyTwoSymbol { get; set; }
        [DataMember(Name = "CurrencyThreeSymbol")]
        public string CurrencyThreeSymbol { get; set; }
        [DataMember(Name = "CurrencyFourSymbol")]
        public string CurrencyFourSymbol { get; set; }
        [DataMember(Name = "CurrencyFiveSymbol")]
        public string CurrencyFiveSymbol { get; set; }
        [DataMember(Name = "Image")]
        public string Image { get; set; }
        [DataMember(Name = "ScreenFlag")]
        public int ScreenFlag { get; set; }
        //[DataMember(Name = "UpsertFlag")]
        //public int UpsertFlag { get; set; }
        //[IgnoreDataMember]
        //public string StartDateTime { get; set; }
        //[IgnoreDataMember]
        //public string EndDateTime { get; set; }
        [DataMember(Name = "StartDate")]
        public string StartDate { get; set; }
        [DataMember(Name = "EndDate")]
        public string EndDate { get; set; }
    }
}
