﻿using NCC.CMDB.Service.Data;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;
using System;
using System.Collections.Generic;

namespace NCC.Service.Services
{
   public class BranchService:IBranchService
    {
        private ICMDBContext _cmdbContext;
        private string currentDate = DateTime.Now.Date.Day.ToString() + "/" + DateTime.Now.Date.Month.ToString() + "/" + DateTime.Now.Date.Year.ToString();
        public BranchService(ICMDBContext cmdbContext)
        {
            _cmdbContext = cmdbContext;
        }

        public IList<Branch> GetAllBranch()
        {
            return _cmdbContext.BranchRepository.GetAllBranch();
        }
        public bool DeleteBranch(int id)
        {
            return _cmdbContext.BranchRepository.Delete(id);
        }

        public Branch GetSingleBranchRecord(int id)
        {
            return _cmdbContext.BranchRepository.GetSingleBranchRecord(id);
        }

        public Branch SaveBranchData(Branch branch)
        {
            return _cmdbContext.BranchRepository.Insert(branch);
        }
    }
}
