﻿using NCC.CMDB.Service.Data;
using NCC.Service.BusinessObjects;
using System;
using System.Collections.Generic;
using NCC.Service.Entities;
using NCC.Service.Constants;

namespace NCC.Service.Services
{
    public class CurrencyService : ICurrencyService
    {
        private ICMDBContext _cmdbContext;
        private string currentDate = DateTime.Now.Date.Day.ToString() + "/" + DateTime.Now.Date.Month.ToString() + "/" + DateTime.Now.Date.Year.ToString();
        public CurrencyService(ICMDBContext cmdbContext)
        {
            _cmdbContext = cmdbContext;
        }

        public IList<CurrencyBO> GetAllCurrencies()
        {
            return _cmdbContext.CurrencyRepository.GetAllCurrencies();
        }

        public bool DeleteCurrency(int id)
        {
            return _cmdbContext.CurrencyRepository.Delete(id);
        }
        
        public CurrencyBO GetSingleCurrencyRecord(int id)
        {
            return _cmdbContext.CurrencyRepository.GetSingleCurrencyRecord(id);
        }

        public Currency SaveCurrencyData(CurrencyBO currencyBO)
        {
            return _cmdbContext.CurrencyRepository.Add(currencyBO);
        }

        public void UpsertSlotRecord()
        {
            
            int currentTimeSlot = DateTime.Now.TimeOfDay.Hours;
            string currentTime = DateTime.Now.TimeOfDay.ToString();
            _cmdbContext.CurrencyRepository.UpsertSlotRecord(currentDate,currentTimeSlot,currentTime);
        }

        public List<ConvertCurrencyBO> GetAllSlots()
        {
            return _cmdbContext.CurrencyRepository.GetAllSlots(currentDate);
        }
    }
}
