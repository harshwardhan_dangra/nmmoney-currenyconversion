﻿using NCC.Service.BusinessObjects;
using NCC.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.Services
{
    public interface IScreenService
    {
        IList<ScreenBO> GetAllScreensData();
        IList<ScreenTypesBO> GetAllScreenTypeData();
        Screens SaveScreenData(Screens screenBO);
        bool DeleteScreen(int id);
        ScreenBO GetSingleScreenRecord(int id);
        IList<ScreenBO> GetActiveScreenData();
    }
}
