﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NCC.Service.Entities;

namespace NCC.Service.Services
{
    public interface IUploadService
    {
        Images SaveImageData(Images ImageBO);
        IList<Images> GetAllImageData();       
        bool DeleteImage(int id);
        Images GetSingleImageRecord(int id);
    }
}
