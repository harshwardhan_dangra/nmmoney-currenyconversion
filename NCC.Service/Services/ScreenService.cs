﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NCC.CMDB.Service.Data;
using NCC.Service.BusinessObjects;
using NCC.Service.Entities;

namespace NCC.Service.Services
{
    public class ScreenService : IScreenService
    {
        private ICMDBContext _cmdbContext;
        public ScreenService(ICMDBContext cmdbContext)
        {
            _cmdbContext = cmdbContext;
        }
        public IList<ScreenBO> GetAllScreensData()
        {
            return _cmdbContext.ScreenRepository.GetAllScreensData();
        }

        public IList<ScreenTypesBO> GetAllScreenTypeData()
        {
            return _cmdbContext.ScreenRepository.GetAllScreenTypeData();
        }

        public Screens SaveScreenData(Screens screenBO)
        {
            return _cmdbContext.ScreenRepository.Add(screenBO);           
        }

        public bool DeleteScreen(int id)
        {
            return _cmdbContext.ScreenRepository.Delete(id);
        }

        public ScreenBO GetSingleScreenRecord(int id)
        {
            return _cmdbContext.ScreenRepository.GetSingleScreenRecord(id);
        }

        public IList<ScreenBO> GetActiveScreenData()
        {
            return _cmdbContext.ScreenRepository.GetActiveScreenData();
        }
    }
}
