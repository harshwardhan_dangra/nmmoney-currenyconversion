﻿using NCC.CMDB.Service.Data;
using NCC.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NCC.Service.Services
{
    public class UploadService : IUploadService
    {
        private ICMDBContext _cmdbContext;
        public UploadService(ICMDBContext cmdbContext)
        {
            _cmdbContext = cmdbContext;
        }

        public bool DeleteImage(int id)
        {
            return _cmdbContext.UploadRepository.Delete(id);
        }

        public IList<Images> GetAllImageData()
        {
            return _cmdbContext.UploadRepository.GetAll().ToList();
        }

        public Images GetSingleImageRecord(int id)
        {
            return _cmdbContext.UploadRepository.GetById(id);
        }

      
        public Images SaveImageData(Images image)
        {
            return _cmdbContext.UploadRepository.Add(image);
        }
    }
}
